var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'app/assets/javascripts');
var APP_DIR = path.resolve(__dirname, 'app/assets/jsx');

var isProduction = process.env.NODE_ENV === "production" ? true : false;

var prodPlugins = [
  new webpack.optimize.UglifyJsPlugin({
    compress: {
        warnings: false
    },
    output: {
      comments: false
    }
  }),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  })
]

var config = {
  devtool: isProduction ? 'source-map' : 'eval',
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: isProduction ? 'react.min.js' : 'react.js'
  },
  module: {
  	loaders: [
  		{
  			test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel',
        query: {
          presets: ['es2015', 'react', 'stage-0']
        }
  		}
  	]
  },
  plugins: isProduction ? prodPlugins : []
};

module.exports = config;
require 'yaml'

DICTIONARY_FILE = 'dict.yml'

ENGLISH_DICTIONARY_FILE = './config/locales/en.yml'
JAPANESE_DICTIONARY_FILE = './config/locales/ja.yml'
SPANISH_DICTIONARY_FILE = './config/locales/es.yml'

SAVE_PATH = './'

def reorder_hash (hash, sorted_hash, keys = [], parent = nil)
	
	hash.each do |key, value|
		keys << parent
		keys = trim_keys keys
		if value.is_a? (Hash)
			reorder_hash value, sorted_hash, keys, key
		else
			#puts keys.to_s + " value: #{value}, key: #{key}"
			ordered_keys = keys.dup
			ordered_keys[0] = key
			#puts ordered_keys.to_s
			add_value_to_sorted_hash sorted_hash, ordered_keys, value
		end	
	end
	
end

def trim_keys (keys)
	#iterates through array until it finds a match
	#when it does so it removes 
	#every element in the range [index, penultimate]
	index = 0
	match = false
	trimmed_keys = keys.dup
	
	keys.each do |element|
		if (index < keys.length - 1)
			if  element == keys.last
				remove_count = keys.length - index - 1
				match = true
				remove_count.times do
					trimmed_keys.delete_at(index)
				end
			end
		end
		index += 1
		break if match == true
	end

	return trimmed_keys
end

def add_value_to_sorted_hash (sorted_hash, keys, value)

	#puts "keys: " + keys.to_s
	#puts "hash: " + sorted_hash.to_s
	#puts "keys length : #{keys.length}"

	
	if sorted_hash[keys[0]].nil?
		sorted_hash[keys[0]] = {}
	end
	
	if keys.length > 1
		inner_hash = sorted_hash[keys[0]]
		keys.shift
		add_value_to_sorted_hash inner_hash, keys, value
	else
		sorted_hash[keys[0]] = value
	end
	
end

def write_sorted_hash_file (sorted_hash)

	language_hash = {}
	sorted_hash.each do |key, hash|
		language_hash[key] = hash

		case key
		when "en"
			file = File.open(ENGLISH_DICTIONARY_FILE, "w")	
			puts "writing english file..."
		when "es"
			file = File.open(SPANISH_DICTIONARY_FILE, "w")
			puts "writing spanish file..."
		when "ja"
			file = File.open(JAPANESE_DICTIONARY_FILE, "w")
			puts "writing japanese file..."
		end
		file.write(language_hash.to_yaml(line_width: -1).to_s)
		file.close
		language_hash = {}
	end


end

sorted_hash = {}

hash = YAML.load_file(DICTIONARY_FILE)

reorder_hash(hash, sorted_hash)

write_sorted_hash_file(sorted_hash)

puts "Success."
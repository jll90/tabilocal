module PlacesHelper

	def place_formatter(place, filter=nil)
		if place
			place_name = case filter
				when "with_province" then place.name_with_province
				when "with_country" then place.name_with_country
				else place.fullname
			end
			return place_name
		else
			return I18n.t('general.not_set')
		end
	end

	def build_place_data(place)
		return place.to_json
	end
end

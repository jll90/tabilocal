module ThreadsHelper

	def build_query_params(params)
		query_params = {}

		if params[:languages]
			languages = params[:languages].split(" ")
			languages.map! { |language| language.gsub('_', ' ') }
			languages_ids = Language.where(name: languages)
			query_params["languages.id"] = languages_ids
		end

		if params[:tags]
			tags = params[:tags].split(" ")
			tags.map! { |tag| tag.gsub('_', ' ') }
			tags_ids = Tag.where(name: tags)
			query_params["tags.id"] = tags_ids
		end

		if params[:places]
			places = params[:places].split(" ")
			places.map! { |place| place.gsub('_', ' ') }
			countries_names = Place.where(country: places).pluck(:country)
			query_params["places.country"] = countries_names		
		end

		return query_params

	end

	def build_filtering_object(travel_thread)
		thread_data = {}
		thread_data["place"] = []
		thread_data["language"] = []
		thread_data["tags"] = []

		thread_data["place"].push(travel_thread.place.country)
		thread_data["language"].push(travel_thread.language.name)

		travel_thread.tags.each do |tag|
			thread_data["tags"] << tag.name
		end

		return thread_data.to_json
	end

end

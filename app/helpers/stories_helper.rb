module StoriesHelper

	def story_exists?(user, recipient, stories)
		if stories.length > 0
			stories.each do |story|
				if story.user_id == user.id && story.recipient_id == recipient.id
					return true
				end
			end
		end
		return false
	end

end

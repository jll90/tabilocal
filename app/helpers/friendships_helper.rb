module FriendshipsHelper
	def on_friendships_page?(user_id)
		request.referer.include? "/users/#{user_id}/friendships"
	end
end
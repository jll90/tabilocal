module LanguagesHelper
=begin
	def current_languages
		if logged_in?
			if session[:languages_ids].nil?
				set_languages
			end
			@current_languages = Language.where(id: session[:languages_ids])
		else
			return nil
		end
	end

	def set_languages
		session[:languages_ids] = []
		current_user_languages.each do |ul|
			session[:languages_ids] << ul.language.id
		end
	end

	def add_language(language)
		session[:languages_ids] << language.id
	end

	def remove_language(language)
		session[:languages_ids] = session[:languages_ids] - [language.id]
	end
=end	
	def unset_languages
		@unset_languages = Language.where.not(id: current_user.languages.ids)
	end

	def languages_formatter(languages, length=nil)
		if languages.count > 0
			lang = []
			langstr = ""

			languages.each do |language|
				lang << language.name
			end

			langstr = lang.join(', ')
		else
			langstr = I18n.t('general.not_set')
		end
	end	

end
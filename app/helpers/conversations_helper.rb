module ConversationsHelper

	def conversation_exists?(user1, user2)
		user_conversations = user1.user_conversations.includes(:user, :conversation)

		user_conversations.each do |user_conversation|
			users_ids = user_conversation.conversation.users.ids

			if (users_ids.sort == [user1.id, user2.id].sort) && (users_ids.length == 2)
				return user_conversation.conversation.id
			end
		end

		return false
	end

	def find_thumbnail_users(messages)
		users_ids = Conversation.find(messages.first.conversation.id).users.ids
		users_ids = users_ids - [current_user.id] #removes current user_id
		users = User.where(id: users_ids)
		return users
	end

	#excludes current user
	def participants_names(users)
		firstnames = users.pluck(:firstname)
		return firstnames.join(", ")
	end

	def build_recipients_data(users)
		recipients = []
		users.each do |user|
			recipient = {}
			recipient[:fullname] = user.fullname
			recipient[:id] = user.id
			recipients << recipient
		end
		return recipients.to_json
	end

	#function used to remove columns from query, i.e. tokens
	#patch solution due to 
	#select syntax not working for activeRecord
	def filter_user_data_as_json(users, params=[])
		filtered_users = []
		if params.length == 0
			users.each do |u|
				filtered_user = {}
				filtered_user[:fullname] = u.fullname
				filtered_user[:id] = u.id
				filtered_users << filtered_user
			end
		end
		return filtered_users.to_json
	end

	def unread_messages?(messages, unread_messages)
		if unread_messages.length > 0
			messages_ids = messages.ids
			unread_messages.each do |unread_message|
				if messages_ids.include? unread_message.message_id
					return true
				end
			end
		end
		return false
	end

	#removes recipient in case it exists
	def build_recipients_list (friends, recipient)
		if friends.include? recipient
			filtered = friends.each.reject { |friend| friend.id == recipient.id }
			return filtered
		end
		return friends
	end
end
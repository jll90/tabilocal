module ProfilePicturesHelper
	def show_profile_illustration (user, profile_picture, classes = [], db_enabled = true)
		if db_enabled == true
			profile_illustration = ""
			
			if profile_picture.nil?
				profile_illustration << "<div class=\"#{classes.join(' ')}\"> #{user.firstname[0]} </div>"
			else
				image_name = format_paperclip_image_name (profile_picture.image.original_filename)
				image_src = profile_picture.image.url(:thumbnail)
				profile_illustration <<  "<img class=\"#{classes.join(' ')}\" alt=\"#{image_name}\" src=\"#{image_src}\">"
			end
			return profile_illustration.html_safe
		else
			profile_illustration = ""
			pic_id = rand(1..30)
			url = "https://s3-us-west-2.amazonaws.com/basic-tabi/profile_pictures/user_#{pic_id}.jpg"
			style = "background-image: url(#{url})"
			
			profile_illustration << "<div class=\"#{classes.join(' ')}\" alt=\"profile picture\" "
			profile_illustration << " style=\"#{style}\">"
			profile_illustration << "</div>"
			return profile_illustration.html_safe
		end
	end

	def format_paperclip_image_name (filename)
		format_str = filename.slice(0..(filename.index('.'))).capitalize
		return format_str
	end
end

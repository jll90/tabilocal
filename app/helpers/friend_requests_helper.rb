module FriendRequestsHelper
	def fetch_relationship(user_id, friend_id)
		friend_request = FriendRequest.find_by(user_id: user_id, friend_id: friend_id)
		if friend_request
			return friend_request
		else 
			inverse_friend_request = FriendRequest.find_by(user_id: friend_id, friend_id: user_id)
			if inverse_friend_request
				return inverse_friend_request
			end	
		end
		friendship = Friendship.find_by(user_id: user_id, friend_id: friend_id)
		if friendship
			return friendship
		end
		return nil
	end

	#need senders_id so friend_request user_id
	def on_friend_profile?(user_id)
		user_path = "/users/#{user_id}"
		friendships_path = "/friendships?id=#{user_id}"
		profile_pictures_path = "/profile_pictures?id=#{user_id}"
		replies_path = "/replies?id=#{user_id}"
		references_path = "/references?id=#{user_id}"
		
		on_user_page = request.referer.include? user_path
		on_friendships_page = request.referer.include? friendships_path
		on_profile_pictures_page = request.referer.include? profile_pictures_path
		on_replies_page = request.referer.include? replies_path
		on_references_page = request.referer.include? references_path

		return on_user_page || on_friendships_page || on_profile_pictures_page || on_replies_page || on_references_page
	end

	def relationship_status(relationship)
		if relationship
			if relationship.class.name == "Friendship"
				return "friendship"
			end
			if relationship.class.name == "FriendRequest"
				if relationship.user_id == current_user.id
					return "sent"
				else
					return "incoming"
				end
			end
		end
		return "none"
	end
end

module UsersHelper

	def dob_formatter(dob)
		return I18n.localize dob, format: :long
	end

	def gender_formatter(gender)
		gender_type = case gender
			when "M" then t('users.edit.gender_male')
			when  "F" then t('users.edit.gender_female')
			when  "O" then t('users.edit.gender_other')
		end
		return gender_type
	end

	def gender_privacy_formatter(gender_privacy)
		gender_privacy_type = case gender_privacy
            when  "T" then t('users.edit.show_gender')
            when  "F" then t('users.edit.not_show_gender')
        end
        return gender_privacy_type
	end

	def birthday_privacy_formatter(birthday_privacy)
		birthday_privacy_type = case birthday_privacy
			when  "T" then t('users.edit.show_birthday')
            when  "F" then t('users.edit.not_show_birthday')
        end
        return birthday_privacy_type
	end

	def status_formatter(status_value)
		status_message = case status_value
			when "T" then t('users.edit.receive_message')
			when "F" then t('users.edit.not_receive_message')
		end
		return status_message
	end

	def coverphoto_styles(cover_picture_url)
		coverphoto_style = "
		    background: #DFD3B6 url(\"#{cover_picture_url}\") no-repeat center center;
		    -webkit-background-size: cover;
		    -moz-background-size: cover;
		    -ms-background-size: cover;
		    background-size: cover; 
    	"
    	return coverphoto_style
	end

	def banner_background_style(image_url)
		banner_background_style = "
		    background: url(\"#{image_url}\") no-repeat center center;
		    -webkit-background-size: cover;
		    -moz-background-size: cover;
		    -ms-background-size: cover;
		    background-size: cover; 
    	"
    	return banner_background_style
	end

	def user_params_formatter (user_params)
		user_params.each do |key, value|
			key_name = case key
				when "dob" then user_params[key] = dob_formatter(value)
				when "birthday_privacy" then user_params[key] = birthday_privacy_formatter(value)
				when "gender_privacy" then user_params[key] = gender_privacy_formatter(value)
				when "gender" then user_params[key] = gender_formatter(value)
				when "status" then user_params[key] = status_formatter(value)
			end
			return user_params
		end
	end

	def name_formatter(names)
		if names.is_a?(Array)
			capitalized_names_array = []
			names.each do |name|
				capitalized_name = name.split(" ")
				capitalized_name.map! {|name| name.capitalize}
				capitalized_name = capitalized_name.join(" ")
				capitalized_names_array << capitalized_name
			end
			return capitalized_names_array
		else 
			#Can add string support
			#Raises exception because it should not recover. 
			raise "This function only accepts arrays."
		end
	end

end

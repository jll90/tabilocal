module NotificationsHelper

	#This method contains logic at the view level
	def show_delete_button?(notification)
		if controller.controller_name == "notifications" &&
			controller.action_name == "index" &&
			notification.user_id == current_user.id
			return true
		else
			return false
		end
	end
end

module ApplicationHelper
	def print_locale_name
		locale_name = case I18n.locale.to_s
			when "en" then "English"
			when "es" then "Español"
			when "ja" then "日本語"
			else "English"
		end
		return locale_name
	end

	def attr_to_symbol (attribute)
		label = attribute.gsub("-", "_").to_sym
	end

	def layout_is?
		if (controller.controller_name == "static_pages" && controller.action_name != "dashboard") ||
		   (controller.controller_name == "users" && controller.action_name == "new") ||
		   (controller.controller_name == "users" && controller.action_name == "edit") ||
		   (controller.controller_name == "redirects" && controller.action_name == "show" )
			return "no_banner"
		elsif controller.controller_name == "conversations"
			return "banner_right_single"
		else
			return "banner_right_double"
		end
	end

	def navbar_present?
		#only redirects controller - action: show has no navbar
		#every other route on the website (no partial) has a navbar
		if 	(controller.controller_name == "redirects" && controller.action_name == "show" )
			return false
		else
			return true
		end
	end

	def save_language_preference(language_preference)
		cookies.permanent[:language_preference] = language_preference
	end

	def js_redirect(url, message=nil, class_name=nil, anchor_el=nil)
		#needs both opening and closing single quotation marks
		full_url = "'"
		full_url << "#{url}"
		if !anchor_el.nil?
			full_url << generate_anchor(anchor_el)  
		end
		if !message.nil? && !class_name.nil?
			full_url <<  "?alert=#{message}&class=#{class_name}"
		end
		full_url << "'"
		render js: "window.location=#{full_url}"
	end

	def pass_to_error_handler(errors)
		if errors.instance_of? String
			@errors = {error: [errors]}
		else
			@errors = errors
		end
		render 'layouts/errorHandler.js.erb'
	end

	def three_dot_formatter(string)
		three_dot = [string, "..."].join("")
	end

	def generate_anchor(object)
		anchor = [object.class.name.downcase, "-", object.id].join("")
	end

	def errors_to_html(errors)
		parsed_errors = "<ul>"
		errors.each do |key, value|
			parsed_errors << "<li>#{value}</li>"
		end
		parsed_errors << "</ul>"
		return parsed_errors
	end
end
module UserLanguagesHelper
=begin	
	def current_user_languages

		if logged_in?
			if session[:user_languages_ids].nil?
				set_user_languages
			end
			@current_user_languages ||= UserLanguage.where(id: session[:user_languages_ids])	
		else

			return nil
		end
	end

	def set_user_languages
		session[:user_languages_ids] = UserLanguage.where(user_id: current_user.id).ids
	end	

	def user_languages_set?
		!current_user_languages.nil?
	end

	def current_user_languages_learning
		if user_languages_set?
			@current_user_languages_learning = current_user_languages.where(category: "L")
		else 
			return nil
		end
	end

	def current_user_languages_known
		if user_languages_set?
			@current_user_languages_known = current_user_languages.where(category: "K")
		else
			return nil
		end
	end


	def add_user_language(user_language)
		session[:user_languages_ids] << user_language.id
		add_language(user_language.language)
	end

	def remove_user_language(user_language)
		session[:user_languages_ids] = session[:user_languages_ids] - [user_language.id]
		remove_language(user_language.language)
	end
=end

end
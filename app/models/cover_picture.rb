class CoverPicture < ActiveRecord::Base
	belongs_to :user
	after_create :update_cover_picture
	after_destroy :delete_cover_picture

	has_attached_file :image, styles: {
		thumbnail: '200x200#',
		medium: '400x400>'
	}

	#prevents error from being shown twice
  	after_validation :clean_paperclip_errors
	
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/, message: :format
	validates_attachment_size :image, :in => 0..1000.kilobytes, message: :size

	private

		def update_cover_picture
	      self.user.update(cover_picture_id: self.id)
	    end

	    def delete_cover_picture
	      self.user.update(cover_picture_id: nil) if self.user.cover_picture_id == self.id
	    end

	    def clean_paperclip_errors
	    	errors.delete(:image)
	    end
end
class User < ActiveRecord::Base

    default_scope {includes(:current_profile_picture)}

    after_initialize :set_summary_to_zero, :set_language_preference, :set_language_count

    has_many :friend_requests, dependent: :destroy
    has_many :pending_friends, through: :friend_requests, source: :friend

    has_many :friendships, dependent: :destroy
    has_many :friends, through: :friendships

    has_many :language_threads
    has_many :travel_threads
    has_many :replies
    has_many :notifications
    has_many :stories
    has_many :subscriptions
    has_many :pictures
    has_many :profile_pictures
    has_many :cover_pictures

    has_many :user_languages
    has_many :languages, through: :user_languages

    has_many :events_participant, foreign_key: "participant_id"
    has_many :events, through: :events_participant

    has_many :user_places
    has_many :places, through: :user_places

    has_many :user_conversations
    has_many :conversations, through: :user_conversations
    has_many :messages

    has_many :invitations

    has_one :hometown, class_name: "Place", foreign_key: :id, primary_key: :hometown_id
    has_one :current_residence, class_name: "Place", foreign_key: :id, primary_key: :current_residence_id
    has_one :current_profile_picture, class_name: "ProfilePicture", foreign_key: :id, primary_key: :profile_picture_id
    has_one :current_cover_picture, class_name: "CoverPicture", foreign_key: :id, primary_key: :cover_picture_id

    has_many :unread_messages, class_name: "MessageFlag", foreign_key: :user_id

	attr_accessor :remember_token, :activation_token, :reset_token, :original_password
	before_save :downcase_email
	before_create :create_activation_digest

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	VALID_GENDER_REGEX = /\A[FMO]{1}\z/
    VALID_LANG_PREF_REGEX = /\A(en|es|ja)\z/
    VALID_PRIVACY_REGEX = /\A[FT]{1}\z/

	validates :firstname, presence: {message: :presence}, length: {maximum: 20, message: :length}
	validates :lastname, presence: {message: :presence}, length: {maximum: 32, message: :length}
	validates :email, presence: {message: :presence}, length: {maximum: 64, message: :length},
				format: { with: VALID_EMAIL_REGEX, message: :valid }, uniqueness: {case_sensitive: false, message: :unique}
	validates :dob, presence: true
	validates :gender, presence: {message: :presence},
				format: {with: VALID_GENDER_REGEX, message: :valid}
    validates :language_preference, format: {with: VALID_LANG_PREF_REGEX}

    #F denotes private
    validates :birthday_privacy, presence: true, format: {with: VALID_PRIVACY_REGEX}, allow_nil: true
    validates :gender_privacy, presence: true, format: {with: VALID_PRIVACY_REGEX}, allow_nil: true
    validates :status, presence: true, format: {with: VALID_PRIVACY_REGEX}, allow_nil: true

    validates :friends_count, presence: true, numericality: {greater_than_or_equal_to: 0}
    validates :stories_count, presence: true, numericality: {greater_than_or_equal_to: 0}
    validates :posts_count, presence: true, numericality: {greater_than_or_equal_to: 0}    
    validates :languages_count, presence: true, numericality: {greater_than_or_equal_to: 0} 

    validates :about_intro, presence: true, length: {minimum: 20}, allow_nil: true
    validates :website, presence: true, allow_blank: true

    validates :current_residence_id, presence: true, numericality: {greater_than: 0, message: I18n.t('.validation.place.cannot_be_blank')}, allow_nil: true
    validates :hometown_id, presence: true, numericality: {greater_than: 0, message: I18n.t('.validation.place.cannot_be_blank')}, allow_nil: true


	has_secure_password
	validates :password, presence: {message: :presence}, length: {minimum: 6, message: :length}, allow_nil: true

    before_save :update_residence_hometown_count, on: :update

    def firstname
        if self[:firstname]
            firstname = self[:firstname].split(" ")
            firstname.map! {|n| n.capitalize}
            return firstname.join(" ")
        end
    end

    def lastname
        if self[:lastname]
            lastname = self[:lastname].split(" ")
            lastname.map! {|n| n.capitalize}
            return lastname.join(" ")
        end
    end

	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
    end

    def User.new_token
    	SecureRandom.urlsafe_base64
    end

    def remember
    	self.remember_token = User.new_token
    	update_attribute(:remember_digest, User.digest(remember_token))
    end

    def forget
    	update_attribute(:remember_digest, nil)
    end

    def authenticated?(attribute, token)
    	digest = send("#{attribute}_digest")
    	return false if digest.nil?
    	BCrypt::Password.new(digest).is_password?(token)
    end

    def activate
    	update_attribute(:activated, true)
    	update_attribute(:activated_at, Time.zone.now)
    end

    def create_first_language
        #defaults to spanish
        #refactor this function
        default_language = Language.where(name: "Spanish").first
        self.user_languages.create(category: "K", language_id: default_language.id)
    end

    def send_activation_email
    	UserMailer.account_activation(self).deliver_now
    end

      # Sets the password reset attributes.
    def create_reset_digest
        self.reset_token = User.new_token
        update_attribute(:reset_digest,  User.digest(reset_token))
        update_attribute(:reset_sent_at, Time.zone.now)
    end

    # Sends password reset email.
    def send_password_reset_email
        UserMailer.password_reset(self).deliver_now
    end

    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end

    def speaks_a_language?
        languages_count > 0
    end

    #perhaps this belong in different part
    def friends?(friend)
        self.friends.include?(friend)
    end

    def pending_friend?(friend)
        self.pending_friends.include?(friend)
    end

    def incoming_request?(friend)
        friend.pending_friends.include?(self)
    end

    def remove_friend(friend)
        self.friends.destroy(friend)
    end

    def fullname
        fullname = self.firstname + " " + self.lastname
    end

    def gender_private?
        return gender_privacy == "F"
    end

    def birthday_private?
        return birthday_privacy == "F"
    end

    def profile_complete?
        speaks_a_language = (languages.count > 0)
        hometown_set = !(hometown_id.nil?)
        current_residence_set = !(current_residence_id.nil?)
        about_intro_set = !(about_intro.nil?)

        profile_complete = (speaks_a_language) && (hometown_set) && (current_residence_set) && (about_intro_set)
    end

    def update_email(updated_email)
        self.update(email: updated_email)
    end

    private
    	def downcase_email
    		self.email = email.downcase
    	end

    	def create_activation_digest
    		self.activation_token = User.new_token
    		self.activation_digest = User.digest(activation_token)
    	end

        def set_summary_to_zero
            if self.new_record?
                self.friends_count = 0
                self.stories_count = 0
                self.posts_count = 0
            end
        end

        def set_language_preference
            if self.new_record?
                self.language_preference = "es"
            end
        end

        def set_language_count 
            if self.new_record?
                self.languages_count = 0
            end
        end

        def set_status
            #even though column on db inits to "F",
            #it's good to enforce it at the model level
            if self.new_record?
                self.status = "T"
            end
        end

        def update_residence_hometown_count
            if self.changes.key?(:hometown_id)
                prev_id = self.changes[:hometown_id][0]
                cur_id = self.changes[:hometown_id][1]

                if prev_id
                    prev_hometown = Place.find(prev_id)
                    prev_hometown.update_column(:natives, (prev_hometown.natives - 1))
                end

                cur_hometown = Place.find(cur_id)
                cur_hometown.update_column(:natives, (cur_hometown.natives + 1))
            end
            if self.changes.key?(:current_residence_id)                
                prev_id = self.changes[:current_residence_id][0]
                cur_id = self.changes[:current_residence_id][1]

                if prev_id
                    prev_residence = Place.find(prev_id)
                    prev_residence.update_column(:residents, (prev_residence.residents - 1))
                end
                
                cur_residence = Place.find(cur_id)
                cur_residence.update_column(:residents, (cur_residence.residents + 1))
            end
        end
end
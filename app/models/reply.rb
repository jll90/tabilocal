class Reply < ActiveRecord::Base

	default_scope -> {order(created_at: :asc)}

	#can only be triggered after create and not save
	#after save callback is run even after updating
	after_create :notify_subscribed_users, 
				:increment_posts_count, 
				:increment_replies_count, 
				:update_thread_timestamp

	belongs_to :replyable, polymorphic: true
	belongs_to :user

	# validate :validate_type_and_id

	VALID_TYPE_REGEX = /\A(TravelThread)\z/
	validates :replyable_type, format: {with: VALID_TYPE_REGEX}, allow_nil: true

	validates :content, presence: {message: :presence}, length: {minimum: 10,  message: :length}
	validates :user_id, presence: true
	validates :replyable_id, presence: true, allow_nil: true
	#this attribute needs to be frozen - not updatable
	validates_inclusion_of :first, in: [true, false], on: :create
	#must update this validator false positive for test
	#create custom validator

	def is_editable_by_user?(user_id)
		can_edit = (self.user_id == user_id)
	end
	
	private
		def notify_subscribed_users
			
			subscriptions = Subscription.where(
				subscriptionable_type: self.replyable_type,
				subscriptionable_id: self.replyable_id
				).includes(:user)

			if subscriptions.length > 0
				subscribers = []
				subscriptions.each do |subscription|
					#wont notify user about his own reply
					if self.user.id != subscription.user_id
						subscribers << subscription.user
					end
				end
				
				subscribers.each do |subscriber|
					subscriber.notifications.create(notificationable_type: self.class.name, notificationable_id: self.id)
				end
			end
		end

		def increment_posts_count
			user = self.user
			user.update(posts_count: (user.posts_count + 1))
		end

		def increment_replies_count
			replyable = self.replyable
			replyable.update(replies_count: (replyable.replies_count + 1))
		end

		def update_thread_timestamp
			replyable.touch
		end

		# def validate_type_and_id
		# 	errors.add(:reply, "id must be present if bound to a TravelThread") if self.replyable_type == "TravelThread" && self.replyable_id == nil
		# end

end
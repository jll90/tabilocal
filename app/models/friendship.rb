class Friendship < ActiveRecord::Base

  validates :user, presence: true
  validates :friend, presence: true, uniqueness: {scope: :user}
  validate :not_self

  after_create :create_inverse_relationship
  after_destroy :decrement_friends_count, :remove_corresponding_story, :remove_corresponding_notification, :destroy_inverse_relationship

  belongs_to :user
  belongs_to :friend, class_name: "User"


  def inverse
  	@inverse_friendship = Friendship.where(user_id: self.friend_id, friend_id: self.user_id).first
  end

  private

  	def create_inverse_relationship
  	  friend.friendships.create(friend: user)
  	end

  	def destroy_inverse_relationship
  	  friendship = friend.friendships.find_by(friend: user)
  	  friendship.destroy if friendship
  	end

  	def not_self
  	  errors.add(:friend, "can't be equal to user") if user == friend
  	end

    def decrement_friends_count
      #counter incremented before friend request is destroyed
      if self.inverse
        user = self.user
        friend = self.friend

        user.update(friends_count: (user.friends_count - 1))
        friend.update(friends_count: (friend.friends_count - 1))
      end
    end

    def remove_corresponding_story
      #in case the story doesn't exist
      story = Story.find_by(user_id: user.id, recipient_id: friend.id)
      if story
        story.destroy
      end
    end

    def remove_corresponding_notification
      notifications = Notification.where(notificationable_type: "Friendship", notificationable_id: self.id)
      if notifications
        notifications.destroy_all
      end
    end
end
class Tag < ActiveRecord::Base

	has_many :taggings
	has_many :language_threads, through: :taggings,
								source: :taggable,
								source_type: "LanguageThread"
	has_many :travel_threads, through: :taggings,
								source: :taggable,
								source_type: "TravelThread"

	def name
		name = self[:name].downcase.sub(" ", "_")
    	dictionary_find_string = ".tags.#{name}"
    	return I18n.t(dictionary_find_string)
	end

end
class Suggestion < ActiveRecord::Base

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

	validates :name, presence: {message: :presence}
	validates :email, presence: {message: :presence}, length: {maximum: 64},
				format: { with: VALID_EMAIL_REGEX, message: :valid }
	validates :subject, presence: {message: :presence}
	validates :message, presence: {message: :presence}, length: {minimum: 40, message: :length}

end
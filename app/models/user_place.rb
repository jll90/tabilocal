class UserPlace < ActiveRecord::Base
	VALID_CATEGORY_REGEX = /\A[LT]{1}\z/

	belongs_to :user
	belongs_to :place
	
	validates :user_id, presence: true, numericality: {only_integer: true, greater_than: 0}
	validates :place_id, presence: {message: I18n.t('validation.place.name_defined')}, numericality: {only_integer: true, greater_than: 0, message: I18n.t('validation.place.cannot_be_blank')}
	validates :category, presence: true, format: {with: VALID_CATEGORY_REGEX}

	validates_uniqueness_of :user_id, scope: [:place_id, :category], message: I18n.t('validation.place.unique_place')

	after_create :increment_place_counter
	before_destroy :decrement_place_counter

	private

		def increment_place_counter
			cat = self.category 
			place = Place.find(self.place_id)

			if cat == "L"
				count = place.visited
				place.update_column(:visited, (count + 1))
			end

			if cat == "T"
				count = place.visiting
				place.update_column(:visiting,  (count + 1))
			end
		end

		def decrement_place_counter
			cat = self.category
			place = Place.find(self.place_id)

			if cat == "L"
				count = place.visited
				place.update_column(:visited, (count - 1))
			end

			if cat == "T"
				count = place.visiting
				place.update_column(:visiting,  (count - 1))
			end
		end
	
end
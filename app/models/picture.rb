class Picture < ActiveRecord::Base
  belongs_to :user
 
  has_attached_file :image, styles: {
    thumbnail: '200x200#',
  }

  #prevents error from being shown twice
  after_validation :clean_paperclip_errors

  # Validate the attached image is image/jpg, image/png, etc
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/, message: :format
  validates_attachment_size :image, :in => 0..1000.kilobytes, message: :size

  private 

  	def clean_paperclip_errors
	    errors.delete(:image)
	end

end
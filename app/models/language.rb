class Language < ActiveRecord::Base
	has_many :user_languages
	has_many :language_threads
	has_many :language_threads_about, class_name: "LanguageThread", foreign_key: :about_language_id

    def name
    	name = self[:name].downcase.sub(" ", "_")
    	dictionary_find_string = ".languages.#{name}"
    	return I18n.t(dictionary_find_string)
    end
end

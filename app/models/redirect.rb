class Redirect < ActiveRecord::Base
	validates :link, presence: true
end

class Story < ActiveRecord::Base

	default_scope -> {order(created_at: :desc)}
	validates :user, presence: true
	validates :recipient, presence: true, uniqueness: {scope: :user}
	validates :content, presence: {message: :presence}, length: {minimum: 1000, message: :length}
	validate :not_self
	validate :friendship_exists?, if: 'friendship_pre_validated != true'
	before_destroy :check_valid_time

	after_create :increment_stories_count, :notify_story_recipient
  	after_destroy :decrement_stories_count
  	before_destroy :remove_corresponding_notification
  	

  	attr_accessor :friendship_pre_validated

	belongs_to :user
	belongs_to :recipient, class_name: "User"

	def within_deletable_timeframe?
		created_at > 1.days.ago
	end

	def author?(user)
		user_id == user.id
	end

	private

		def not_self
			errors.add(:story, "can't be equal to user") if user == recipient
		end

		def increment_stories_count
			recipient = self.recipient
			recipient.update(stories_count: (recipient.stories_count + 1))
		end

		def decrement_stories_count
			recipient = self.recipient
			recipient.update(stories_count: (recipient.stories_count - 1))
		end

		def notify_story_recipient
			recipient = self.recipient
			recipient.notifications.create(notificationable_type: self.class.name, notificationable_id: self.id)
		end

		def friendship_exists?
			friendship = Friendship.find_by(user_id: self.user_id, friend_id: recipient_id)
			errors.add(:story, "must be friends to write a story") unless friendship
		end

		def check_valid_time
			if !within_deletable_timeframe?
				errors.add(:story, "You cannot destroy a story after 24 hrs it was created")
				return false
			end
		end

		def remove_corresponding_notification
			notifications = Notification.where(notificationable_type: "Story", notificationable_id: self.id)

			if notifications
				notifications.destroy_all
			end
		end

end
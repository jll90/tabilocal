class Message < ActiveRecord::Base
	belongs_to :user
	belongs_to :conversation, inverse_of: :messages

	validates :content, presence: {message: :presence}
	validates_presence_of :conversation
	validates :user_id, presence: true

	validate :correct_conversation

	after_create :mark_unread_conversation, :update_conversation_timestamp, on: :create

	private
		def mark_unread_conversation
			user_cvs = UserConversation.where(conversation_id: self.conversation_id)
			sender_id = self.user_id
			user_cvs.each do |u_c|
				if sender_id != u_c.user_id
					u_c.mark_as_unread
				end
			end
		end

		def update_conversation_timestamp
			self.conversation.touch
		end

		def correct_conversation
			user_id = self.user_id
			conversation_id  = self.conversation_id
			if conversation_id
				conversation = Conversation.find(conversation_id)
				if !conversation.new_record?
					correct_conversation = UserConversation.find_by(user_id: user_id, conversation_id: conversation_id)
					errors.add(:message, "user does not belong to this conversation") unless correct_conversation
				end
			end
		end


end
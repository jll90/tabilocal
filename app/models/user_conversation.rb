class UserConversation < ActiveRecord::Base
	belongs_to :user
	belongs_to :conversation, inverse_of: :user_conversations

	validates_presence_of :conversation
	validates :user_id, presence: true

	validates_uniqueness_of :conversation_id, scope: :user_id

	def mark_as_read
		self.update(read: true)
	end

	def mark_as_unread
		self.update(read: false)
	end

	def unread?
		unread = !self.read
	end
end
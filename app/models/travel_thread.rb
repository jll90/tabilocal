class TravelThread < ActiveRecord::Base

	after_initialize :set_replies_to_zero, on: :create

	belongs_to :user
	belongs_to :language
	belongs_to :place

	has_many :replies, as: :replyable
	has_many :taggings, as: :taggable
	has_many :tags, through: :taggings
	has_many :subscriptions, as: :subscriptionable

	accepts_nested_attributes_for :replies
	accepts_nested_attributes_for :taggings
	accepts_nested_attributes_for :subscriptions
	
	validates :title, presence: {message: :presence}, length: {minimum: 6, message: :length} 
	validates :user_id, presence: true
	validates :language_id, presence: true
	validates :place_id, presence: {message: :blank}

	validates :replies, presence: true, length: {is: 1}, on: :create
	validates :taggings, presence: {message: :blank}, length: {minimum: 1, maximum: 3, message: :maximum}, on: :create
	
    validate :correct_user_language, on: :create
    validate :match_reply_user, on: :create
    validate :match_subscription_user, on: :create

    after_create :update_place_count
  
	def user_language_exists?(user_id, language_id)
		ul = UserLanguage.where(user_id: user_id, language_id: language_id)
		return (ul.length == 1)
	end

	private

		def correct_user_language
			errors.add(:travel_thread, "must choose correct language") unless user_language_exists?(self.user_id, self.language_id) 
		end

		def match_reply_user
			if self.replies.length > 0
				errors.add(:travel_thread, "travel thread's and reply's ids must match") unless self.user_id == self.replies.first.user_id
			end
		end

		def match_subscription_user
			if self.subscriptions.length > 0
				errors.add(:travel_thread, "travel thread's and subscription's ids must match") unless self.user_id == self.subscriptions.first.user_id
			end
		end

		def set_replies_to_zero
			if self.new_record?
				self.replies_count = 0
			end
		end

		def update_place_count
			place = Place.find(self.place_id)
			count = place.threads
			place.update_column(:threads, (count + 1))
		end
end

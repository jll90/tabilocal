class FriendRequest < ActiveRecord::Base
  
  validates :user, presence: true
  validates :friend, presence: true, uniqueness: {scope: :user}

  validate :not_self
  validate :not_friends
  validate :not_pending

  belongs_to :user
  belongs_to :friend, class_name: "User"

  def accept
  	user.friends << friend
    notify_friend_request_sender
    increment_friends_count
  	self.destroy
  end

  def inverse
    @inverse_friendship = FriendRequest.find_by_user_id_and_friend_id(user_id: self.friend_id, friend_id: self.user_id)
  end

  def reject
    self.destroy
  end

  private
  	def not_self
  		errors.add(:friend, "can't be equal to user") if user == friend
  	end

  	def not_friends
  		errors.add(:friend, 'is already added') if user.friends.include?(friend)
  	end

  	def not_pending
    	errors.add(:friend, 'already requested friendship') if friend.pending_friends.include?(user)
  	end

    def notify_friend_request_sender
      user = self.user
      friendship = Friendship.where(user_id: self.user_id, friend_id: self.friend_id).first
      user.notifications.create(notificationable_type: friendship.class.name, notificationable_id: friendship.id)
    end

    def increment_friends_count
      #counter decremented when friendship is destroyed
      user = self.user
      friend = self.friend

      user.update(friends_count: (user.friends_count + 1))
      friend.update(friends_count: (friend.friends_count + 1))
    end
end

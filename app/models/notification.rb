class Notification < ActiveRecord::Base

	# default_scope { includes(:notificationable) }

	default_scope -> {order(created_at: :desc)}
	

	after_initialize :set_unread_status

	validate :check_unread_status, on: :create

	belongs_to :notificationable, polymorphic: true
	belongs_to :user

	VALID_TYPE_REGEX = /\A(Reply|Friendship|Story)\z/

	validates :notificationable_type, format: {with: VALID_TYPE_REGEX}
	validates :notificationable_id, presence: true
	validates :user_id, presence: true

	def mark_as_read
		self.update(read: true)		
	end

	private 
		def check_unread_status
			errors.add(:notification, "status cannot be unread upon creation") unless self.read == false
		end

		def set_unread_status
			if self.new_record?
				self.read = false
			end
		end
end

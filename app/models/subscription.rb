class Subscription < ActiveRecord::Base
	belongs_to :subscriptionable, polymorphic: true
	belongs_to :user
	
	validates :user_id, presence: true

	VALID_TYPE_REGEX = /\A(TravelThread|LanguageThread)\z/
	validates :subscriptionable_type, format: {with: VALID_TYPE_REGEX}, allow_nil: true

	validates_uniqueness_of :user_id, scope: [:subscriptionable_type, :subscriptionable_id]

end
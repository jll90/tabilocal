class UserLanguage < ActiveRecord::Base
	VALID_CATEGORY_REGEX = /\A[LK]{1}\z/

	belongs_to :user
	belongs_to :language
	
	validates :user_id, presence: true, numericality: {only_integer: true, greater_than: 0}
	validates :language_id, presence: true, numericality: {only_integer: true, greater_than: 0}
	validates :category, presence: true, format: {with: VALID_CATEGORY_REGEX}

	validates_uniqueness_of :language_id, scope: :user_id

	after_create :increment_languages_count, on: :create
	after_destroy :decrement_languages_count, on: :destroy

	private

		def increment_languages_count
			user_id = self.user_id
			user = User.find(user_id)
			languages_count = user.languages_count + 1
			user.update(languages_count: languages_count)
		end

		def decrement_languages_count
		 	user_id = self.user_id
			user = User.find(user_id)
			languages_count = user.languages_count - 1
			user.update(languages_count: languages_count)
		end

end
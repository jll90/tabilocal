class Place < ActiveRecord::Base
	include PgSearch
	
	has_many :user_places
	has_many :travel_threads
	has_many :events

	has_one :country_assoc, class_name: "Country", foreign_key: :id, primary_key: :country_id
	has_one :admin1_assoc, class_name: "AdminDivOne", foreign_key: :id, primary_key: :admin_id

	validates :residents, presence: true, numericality: {greater_than_or_equal_to: 0}
    validates :natives, presence: true, numericality: {greater_than_or_equal_to: 0}
    validates :visiting, presence: true, numericality: {greater_than_or_equal_to: 0}    
    validates :visited, presence: true, numericality: {greater_than_or_equal_to: 0} 
    validates :events, presence: true, numericality: {greater_than_or_equal_to: 0} 
    validates :threads, presence: true, numericality: {greater_than_or_equal_to: 0} 


	pg_search_scope :search_by_name, against: :name, using: {
		tsearch: {
			dictionary: 'simple'
		}
	}

	pg_search_scope :trigram_search,
                  :against => :name,
                  :using => :trigram

	def name_with_province
		name_with_province = self[:name] + ", " + self.admin1_assoc[:name]
	end

	def name_with_country
		name_with_country = self[:name] + ", " + self.country_assoc.name
	end

	def fullname
		fullname = self[:name] + ", " + self.admin1_assoc[:name] + ", " + self.country_assoc.name
	end

	def trigram_alter(query)
		places = Place.where('name ILIKE ?', "%#{query}%")
	end

	def copy_index
		self.update_column(:concat_key, (self[:country] + self[:admin1]))
	end

	protected

		def nil_if_blank
			NULL_ATTRS.each do |column|
				self[column] = nil if self[column].blank?
			end
		end
	
end
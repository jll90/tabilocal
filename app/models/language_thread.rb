class LanguageThread < ActiveRecord::Base

	default_scope -> {order(created_at: :desc)}

	belongs_to :user
	belongs_to :language
	belongs_to :about_language, class_name: "Language",
				foreign_key: :about_language_id

	has_many :replies, as: :replyable
	has_many :taggings, as: :taggable
	has_many :tags, through: :taggings
	has_many :subscriptions, as: :subscriptionable

	accepts_nested_attributes_for :replies
	accepts_nested_attributes_for :taggings
	accepts_nested_attributes_for :subscriptions

	validates :title, presence: true, length: {minimum: 6, maximum: 128} 
	validates :user_id, presence: true
	validates :language_id, presence: true
	validates :about_language_id, presence: true
	
	validates :replies, presence: true, length: {is: 1}	
	validates :taggings, presence: true, length: {minimum: 1, maximum: 4}

	validate :correct_user_language

	def user_language_exists?(user_id, language_id)
		ul = UserLanguage.where(user_id: user_id, language_id: language_id)
		return (ul.length == 1)
	end

	private

		def correct_user_language
			errors.add(:language_thread, "must choose correct language") if !user_language_exists?(self.user_id, self.language_id) 
			errors.add(:language_thread, "must choose correct language") if !user_language_exists?(self.user_id, self.about_language_id)
		end

	private 

		def create_subscription
			user_id = self.user_id
			thread_id = self.id
			Subscription.create(user_id: user_id,
							subscriptionable_id: thread_id,
							subscriptionable_type: "LanguageThread")
		end
end

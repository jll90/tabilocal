class Conversation < ActiveRecord::Base

	default_scope -> {order(updated_at: :desc)}

	has_many :user_conversations, inverse_of: :conversation
	has_many :users, through: :user_conversations
	has_many :messages, inverse_of: :conversation

	accepts_nested_attributes_for :user_conversations
	accepts_nested_attributes_for :messages

	before_validation :set_privacy, on: :create
	before_validation :set_nested_message_conversation

	#can message up to a maximum of 20 users
	validates :user_conversations, length: {minimum: 2, maximum: 21, message: :recipients}
	validates :messages, length: {is: 1}
	validates_inclusion_of :private, in: [true, false]
	
	validate :no_repeating_ids, on: :create
	validate :private_conversation_exists?, on: :create
	#must validate on creation so that it doesn't reach the DB

	before_create :set_user_conversations_read_status, on: :create

	def mark_conversation_as_read(user)
		@user_conversation = UserConversation.where(user_id: user.id, conversation_id: self.id).first
		@user_conversation.mark_as_read
	end


	#verifies if a private conversation exists
	def find_private_conversation
		if self.user_conversations.length == 2
			user1 = self.user_conversations.first.user
			user2 = self.user_conversations.last.user

			user_conversations = user1.user_conversations.includes(:user, :conversation)

			user_conversations.each do |user_conversation|
				users_ids = user_conversation.conversation.users.ids

				if (users_ids.sort == [user1.id, user2.id].sort) && (users_ids.length == 2)
					return user_conversation.conversation
				end
			end
			return nil
		end
	end

	def find_user_conversation(user)
		user_conversation = UserConversation.where(conversation_id: self.id, user: user.id).first
	end

	private

		def no_repeating_ids
			user_ids = Array.new
			self.user_conversations.each do |uc|
				user_ids.push(uc.user_id)
			end
			errors.add(:conversation, "cannot add repeated users") unless user_ids.length == user_ids.uniq.length
		end

		def private_conversation_exists?
			if self.find_private_conversation
				errors.add(:conversation, "cannot create a private conversation again")
			end
		end

		def set_privacy
			if self.user_conversations.length == 2
				self.private = true
			else
				self.private = false
			end
			#must return to otherwise it will return false
			#when the the number of ucs is less than 2 and 
			#will stop all the callbacks from running.
			#in other words it won't save
			return true
		end

		def set_nested_message_conversation
			self.messages.first.conversation = self
		end

		def set_user_conversations_read_status
			user_conversations = self.user_conversations
			#first user is the current_user

			user_conversations.each_with_index do |u_c, index|
				if index == 0
					u_c.read=(true)
				else
					u_c.read=(false)
				end
			end
		end
end
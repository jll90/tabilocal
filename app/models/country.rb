class Country < ActiveRecord::Base

	default_scope -> {order("country ASC")}

	def name
		return self[:country]
	end
end
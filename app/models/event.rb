class Event < ActiveRecord::Base
	belongs_to :place

	has_many :events_participant
	has_many :participants, through: :events_participant, source: :participant
end

class ProfilePicture < ActiveRecord::Base

	belongs_to :user
	after_create :update_profile_picture
	after_destroy :delete_profile_picture

	#prevents error from being shown twice
	after_validation :clean_paperclip_errors

	has_attached_file :image, styles: {
	  thumbnail: '200x200#',
	}

	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/, message: :format
	validates_attachment_size :image, :in => 0..500.kilobytes, message: :size

	private

	    def update_profile_picture
	      #update triggers a validation error
	      #callback is executed if user.update
	      #check if the problem is fixed after updating to rails 5
	      self.user.update_column(:profile_picture_id, self.id)
	    end

	    def delete_profile_picture
	      self.user.update(profile_picture_id: nil) if self.user.profile_picture_id == self.id
	    end

	    def clean_paperclip_errors
	    	errors.delete(:image)
	    end

end

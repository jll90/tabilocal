class ApplicationMailer < ActionMailer::Base
  default from: "noreply@tabilocal.com"
  layout 'mailer'
end

class InvitationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invitation_mailer.invite_friend.subject
  #
  #def invite_friend(email)
  def invite_friend(email, recipient_name, sender_name, message)
    @email = email
    @recipient_name = recipient_name
    @sender_name = sender_name
    @message = message
    mail to: email, subject: t('invitation_mailer.invite_friend.subject')
  end
end

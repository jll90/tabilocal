class StoriesController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: [:edit, :update, :destroy]
	#must be friends has a param dependency
	before_action :must_be_friends, only: [:create, :update]
	before_action :redirect_if_story_exists, only: :new

	def index
		@user = User.find(params[:user_id])
		if @user
			@stories_written_for_user = Story.where(recipient_id: params[:user_id])
			@stories_written_for_others = Story.where(user_id: params[:user_id])
			@story = current_user.stories.build

			if current_user?(@user)
				photo_upload_dependencies
			else
				@relationship = fetch_relationship current_user, @user.id
			end
		else
			flash[:error] = t('alerts.error.user.not_exist')
			redirect_to dashboard_path
		end
	end

	def create
		@story = current_user.stories.build(story_params)
		@story.friendship_pre_validated = true
		if @story.save
			flash[:success] = t('alerts.success.story.created')
			js_redirect "#{user_stories_path(user_id: @story.recipient.id)}"
		else
			pass_to_error_handler @story.errors
		end
	end

	def edit
		@story = Story.find(params[:id])
		@user = @story.recipient
		if current_user?(@user)
			photo_upload_dependencies
		end
	end

	def update
		@story = Story.find(params[:id])
		@story.friendship_pre_validated = true
		if @story.update(story_params)
			flash[:success] = t('alerts.success.story.updated')
			js_redirect "#{user_stories_path(user_id: @story.recipient.id)}"
		else
			pass_to_error_handler @story.errors
		end
	end

	def new
		@story = Story.new
		@user = User.find(params[:recipient_id])

		#must check who is the user to upload photo dependencies
		photo_upload_dependencies		
	end

	def destroy
		@story = Story.find(params[:id])
		@story_id = @story.id
		if @story.destroy
			render 'stories/destroy.js.erb'
		else
			pass_to_error_handler @story.errors
		end
	end

	private

		def story_params
			params.require(:story).permit(:content, :recipient_id, :user_id)
		end	

		def must_be_friends
			if !current_user.friends.include? User.find_by(id: params[:story][:recipient_id])
				pass_to_error_handler t('validation.story.must_be_friends')
			end
		end

		def redirect_if_story_exists
			if params[:check_story_exists]
				if story = Story.find_by(user_id: current_user.id, recipient_id: params[:recipient_id])
					redirect_to edit_story_path(id: story.id)
				end
			end
		end
end
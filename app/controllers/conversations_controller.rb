class ConversationsController < ApplicationController
	before_action :logged_in_user, :completed_profile
	before_action :correct_user, only: [:show]

	def index
		@conversation = nil #to show conversation in case it exists
		@control_flag = "index_conversations"
		if messaging_other_user?
			@recipient = User.find_by_id(params[:message_to])
			if @recipient 
				if (conversation_id = conversation_exists?(current_user, @recipient))
					@conversation = Conversation.where(id: conversation_id).includes(:messages).first
					@messages = @conversation.messages
					@control_flag = "conversation_found"
				else
					@control_flag = "new_conversation"
				end
			else
				@control_flag = "invalid recipient"
				flash.now[:notice] = "User being messaged doesn't exist"
			end
		end
		@conversations = current_user.conversations.all.includes(:users, :user_conversations, :messages)
		@message = Message.new
		@new_conversation = Conversation.new
		@new_conversation_message = @new_conversation.messages.build

		if @control_flag == "new_conversation"
			#removes the user from the friends list if it exists so it's not added as a tag
			#and the autocomplete at the same time
			@recipients_list = build_recipients_list current_user.friends, @recipient
		else
			@recipients_list = current_user.friends
		end	
	end

	def create
		@conversation = Conversation.new(conversation_params)
		@existing_conversation = @conversation.find_private_conversation
		if @conversation.save || @existing_conversation
			@control_variable = "create_conversation"
			if @existing_conversation
				@message = @conversation.messages.first
				@conversation = @existing_conversation
				#must add message to the conversation
				@conversation.messages << @message
				@control_variable = "conversation_exists"
			end
			@messages = @conversation.messages.all.includes(:user)
			@message_count = 0
			@recipients_names = @conversation.users.pluck(:firstname)
			
			respond_to do |format|
				#redirected to this callback to prevent duplicate logic
				format.js {render 'conversations/show.js.erb'}
			end
		else
			pass_to_error_handler @conversation.errors
		end
	end

	def show
		@conversation = Conversation.find(params[:id])
		@conversation.mark_conversation_as_read(current_user)
		@messages = @conversation.messages.all.includes(:user)
		@firstnames = @conversation.users.pluck(:firstname)
		#so the current_user.firstname doesn't show on the recipients list
		@recipients_names = ( (name_formatter @firstnames) - [current_user.firstname])
		@control_variable = "show_conversation"
		respond_to do |format|
			format.js {render 'conversations/show.js.erb'}
		end
	end

	private 
		def conversation_params
			params.require(:conversation).permit(messages_attributes:[:content, :user_id], 
				user_conversations_attributes:[:user_id])
		end

		def messaging_other_user?
			return (current_user.id != params[:message_to] && !params[:message_to].nil?)
		end

		def completed_profile
			profile_complete = current_user.profile_complete?
			if !profile_complete
				flash[:notice] =  t('alerts.notice.conversation.complete_profile')
				redirect_to current_user
			end
		end

end
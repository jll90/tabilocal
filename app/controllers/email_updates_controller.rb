class EmailUpdatesController < ApplicationController

	before_action :logged_in_user

	def create
		@user = current_user.authenticate(params[:user][:password])
		if @user
			if @user.update(user_params)
				render 'email_updates/update_success.js.erb'
			else
				pass_to_error_handler @user.errors
			end
		else
			pass_to_error_handler t('activerecord.errors.models.user.attributes.password.correct')
		end
	end



	private

		#Password is allowed through params[:user][:password]
	    def user_params
	      params.require(:user).permit(:email)
	    end
end
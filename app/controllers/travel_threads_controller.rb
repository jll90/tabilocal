class TravelThreadsController < ApplicationController

	before_action :logged_in_user
	before_action :verify_language_count
	before_action :current_languages, only: [:index, :show]
	before_action :verify_correct_language, only: [:show]

	JSON_INCLUDE_PARAMS = [:place, :language, :tags, :user]
	#MUST MATCH VALUE ON DASHBOARD CONTROLLER
	THREADS_PER_PAGE = 10
	#TravelThread.includes(:tags).where(:tags => {id: [1,2,3]}).all

	def index
		@tags = Tag.all
		respond_to do |format|
			format.json do
				 #add fltering for languages
				 #where places.country "Japan"
				 @travel_threads = TravelThread.all.includes(:place, :language, :tags).where("languages.id" => @current_languages.ids)
				 render json: @travel_threads.as_json(:include=>JSON_INCLUDE_PARAMS), status: 200
			end
			format.html do
				@filter = params[:sort]
				case @filter
				when "unanswered"
					@travel_threads = TravelThread.all.includes(:place, :language, :tags, :user) #no replies is one reply (opening post)
								.where("languages.id" => @current_languages.ids, "replies_count" => 1)
								.paginate(page: params[:page], per_page: THREADS_PER_PAGE)
				when "most_popular"
					@travel_threads = TravelThread.all.includes(:place, :language, :tags, :user)
								.where("languages.id" => @current_languages.ids)
								.paginate(page: params[:page], per_page: THREADS_PER_PAGE).order(replies_count: :desc)
				else
					@travel_threads = TravelThread.all.includes(:place, :language, :tags, :user)
								.where("languages.id" => @current_languages.ids)
								.paginate(page: params[:page], per_page: THREADS_PER_PAGE).order(updated_at: :desc)
				end

				render @travel_threads
									
		 	 	# if @travel_threads.length == 0
		 	 	# 	render @travel_threads, status: 304
		 	 	# else
		 	 	# 	render @travel_threads, status: 200
		 	 	# end
			end
		end
	end

	def show
		# so the partial comes out right
		@replies = @thread.replies
		@reply = Reply.new
		@existing_subscription = current_user.subscriptions.find_by(
			subscriptionable_type: @thread.class.name,
			subscriptionable_id: @thread.id)
		@new_subscription = current_user.subscriptions.build
	end

	def new
		@travel_thread = current_user.travel_threads.build
		@reply = @travel_thread.replies.build
		@subscription = @travel_thread.subscriptions.build
		@my_languages = current_languages
		@tags = Tag.where(category: "T")
	end

	def create
		@travel_thread = current_user.travel_threads.build(travel_thread_params)
		if @travel_thread.save	
			flash[:success] = t('alerts.success.thread.created')
			render js: "window.location='#{travel_thread_path(@travel_thread)}'"
		else
			@errors = @travel_thread.errors
			render 'layouts/errorHandler.js.erb'
		end
	end

	# def query
	# 	if params[:query] == 'true'
	# 	query_params = build_query_params(params)
    #  	@travel_threads = TravelThread.all.includes(:place, :language, :tags).where(query_params)
	# end

	private

		def travel_thread_params
			params.require(:travel_thread).permit(:title, :user_id, :language_id, :place_id, 
				replies_attributes: [:content, :user_id, :first], taggings_attributes: [:tag_id], 
				subscriptions_attributes: [:user_id])
		end

		def verify_correct_language
			@thread = TravelThread.find(params[:id])
			if !(@current_languages.ids.include? @thread.language.id)
				flash[:notice] = "You must speak the language of the thread you're trying to see" 
				redirect_to current_user
			end
		end

end
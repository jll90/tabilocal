class FriendshipsController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: :destroy


	def index
		@user = User.find(params[:user_id])
		@friendships = @user.friendships

		if current_user?(@user)
			photo_upload_dependencies
		else
			@relationship = fetch_relationship current_user.id, @user.id
		end
	end

	def destroy
		if @user
			@inverse_friendship = @friendship.inverse
			@friend_id = @friendship.friend_id
			@inverse_friendship_id = @inverse_friendship.id
			current_user.remove_friend(@friend)
			respond_to do |format|
				format.js {render "destroy_friendship.js.erb"}
			end
		else
			respond_to do |format|
				format.js {render js: "alert('could not remove friend');"}
			end
		end
		
	end
	
	private

		def friendship_params
			#not used
		end
end
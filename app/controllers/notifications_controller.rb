class NotificationsController < ApplicationController
  before_action :logged_in_user 
  before_action :correct_user, only: [:show, :destroy]

  STORY = "Story"
  FRIENDSHIP = "Friendship"
  REPLY = "Reply"
  
  def show
    @notification = Notification.find_by_id(params[:id])
    if @notification
      @notification.mark_as_read

    	notification_type = case @notification.notificationable_type

    		when STORY then 
          redirect_to user_stories_path(user_id: current_user.id)
    		when FRIENDSHIP then 
          @friendship = @notification.notificationable
          redirect_to user_path(id: @friendship.friend.id)
    		when REPLY then 
          @reply = @notification.notificationable
          @travel_thread = @reply.replyable
          redirect_to travel_thread_path(id: @travel_thread.id, anchor: (generate_anchor @reply))
    	end
    else
    	flash[:notice] = t('alerts.notice.notification.does_not_exist')
    	redirect_to current_user
    end
  end

  def index
    @notifications = current_user.notifications
    render :index
  end

  def destroy
    @notification = Notification.find(params[:id])
    @notification_id = @notification.id
    if @notification.destroy
      render 'notifications/destroy.js.erb'
    else
      pass_to_error_handler @notification.errors
    end
  end

  def update_all
    @notifications = current_user.notifications
    @notifications.update_all(read: true)
    render 'notifications/mark_all_as_read.js'
  end

  def destroy_all
    @notifications = current_user.notifications
    @notifications.destroy_all
    render 'notifications/remove_all.js'
  end
  

end
class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        js_redirect dashboard_path(:locale => user.language_preference)
      else
        js_redirect root_url, t('alerts.notice.account.not_activated'), "notice"
      end
    else
      pass_to_error_handler t('alerts.error.session.incorrect_login')
    end
  end

  def destroy
  	log_out if logged_in?
  	redirect_to login_url
  end

end
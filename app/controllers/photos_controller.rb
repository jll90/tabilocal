class PhotosController < ApplicationController

	before_action :logged_in_user

	def index
		@user = User.find(params[:user_id])
		@profile_pictures = @user.profile_pictures
		@cover_pictures = @user.cover_pictures
		@pictures = @user.pictures

		if current_user?(@user)
			photo_upload_dependencies
		else 
			@relationship = fetch_relationship current_user.id, @user.id
		end
	end
	
end
class PlacesController < ApplicationController

  before_action :logged_in_user
  METHODS = [:name_with_province, :name_with_country, :fullname]

  def show
  	@place = Place.find(fetch_id)
  	if params[:q] == 'summary'
      respond_to do |format|
        format.json {render :json => @place.to_json(:methods => METHODS)}
      end
    else
      respond_to do |format|
        @natives = User.where(hometown_id: @place.id)
        @residents = User.where(current_residence_id: @place.id)
        @user_places = UserPlace.where(place_id: @place.id).includes(:user)
        @travelers = []
        @user_places.each do |user_place|
          @travelers << user_place.user
        end
        @threads = TravelThread.where(place_id: @place.id)
        format.html {render :show}
      end
    end
  end

  def index
  	#@places = Place.trigram_search(params[:q])
  	respond_to do |format|
  		format.json  do       
        if params[:q].length > 2
          @places = Place.where('asciiname ILIKE ?', "%#{params[:q]}%").limit(30)
          @places = @places.reject{|p| p.admin1.nil?}
        else
          @places = []
        end
        render :json => @places.to_json(:methods => METHODS)
      end
      format.html do 
        @places = Place.where(fclass: "C")
        render :index
      end
  	end
  end

  def update
    @place = Place.find(params[:id])
    if @place.update(place_params)
      render js: "alert('place successfully updated')"
    else
      render js: "alert('place coult not be updated')"
    end
  end


  private
		def place_params
			params.require(:place).permit(
				:latitude, 
				:longitude)
		end

    def fetch_id 
      #comes from params

      #JSON
      if params[:place_id]
        place_id = params[:place_id]
      end

      #link to 
      if params[:id]
        place_id = params[:id]
      end

      return place_id
    end
end

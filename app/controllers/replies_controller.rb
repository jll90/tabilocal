class RepliesController < ApplicationController

	before_action :logged_in_user
	before_action :user_speaks_thread_language?, only: [:create]
	before_action :correct_user, only: [:edit, :update]

	def index
		@user = User.find(params[:user_id])
		
		@opening_replies = @user.replies.where(first: true).includes(:replyable) 
		@other_replies = @user.replies.where(first: false).includes(:replyable)

		if current_user?(@user)
			photo_upload_dependencies
		else
			@relationship = fetch_relationship current_user, @user.id
		end
	end


	def create
		@reply = current_user.replies.build(reply_params)
		if @reply.save
			respond_to do |format|
				format.js {render 'replies/create.js.erb'}
			end
		else
			pass_to_error_handler(@reply.errors)
		end
	end

	def edit
		#superflous validation since correct_user should verify for
		#correct user - done like so for consistency with other modules
		#in the application
		@reply = Reply.find(params[:id])
		#dependency to correctly display form
		@thread = @reply.replyable
		if current_user?(@user)
			photo_upload_dependencies
		end
	end

	def update
		@reply = Reply.find(params[:id])
		@replyable = @reply.replyable
		#at the moment other values than content are not updatable
		#needs to be validated at the model-layer level
		#for defense in depth
		if @reply.update(reply_params)
			js_redirect "#{travel_thread_path(id: @replyable.id, anchor: (generate_anchor @reply))}"
		else
			pass_to_error_handler @reply.errors
		end
	end

	private
		def reply_params
			#only the content will be edited
			#acts as a barrier so other fields are not updated
			if params[:action] == "update"
				params.require(:reply).permit(:content)
			else 
				params.require(:reply).permit(:content,
					:replyable_id, :replyable_type, :first)
			end
		end

		def redirect_to_thread
			if params[:replyable_type] == "TravelThread"
				redirect_to travel_thread_path(params[:reply][:replyable_id])
			else
				redirect_to language_thread_path(params[:reply][:replyable_type])
			end
		end
end

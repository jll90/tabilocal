class DataController < ApplicationController

	def event_coorganizers
		@coorganizers = current_user.friends
		render json: (filter_user_data_as_json @coorganizers), status: 200
	end

end
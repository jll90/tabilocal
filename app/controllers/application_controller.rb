class ApplicationController < ActionController::Base
  
  protect_from_forgery with: :exception
  before_action :load_friend_requests
  before_action :load_unread_conversations
  before_action :load_notifications
  before_action :set_locale
  before_action :set_invitation
  before_action :set_suggestion

  #allows to use dates
  include ActionView::Helpers::DateHelper
  include SessionsHelper
  include ConversationsHelper
  include FriendRequestsHelper
  include FriendshipsHelper
  include StoriesHelper
  include ProfilePicturesHelper
  include UserLanguagesHelper
  include LanguagesHelper
  include ThreadsHelper
  include UsersHelper
  include ApplicationHelper

	def logged_in_user
		unless logged_in?
			store_location
			flash[:error] = t('alerts.error.session.login')
			redirect_to login_url
		end
	end

	##methods when user is online
	def load_friend_requests
		if logged_in?
			@friend_requests = FriendRequest.where(friend: current_user)
			@friend_requests_count = @friend_requests.count
		end
	end

	##methods when user is online
	def load_unread_conversations
		if logged_in?
			@unread_user_conversations = UserConversation.where(user_id: current_user.id, read: false)
		end
	end

	def load_notifications
		if logged_in?
			@notifications = current_user.notifications.includes(:notificationable)
		end
	end

	def set_locale
		if logged_in?
			I18n.locale = current_user.language_preference.to_s
		else
			if ["ja", "es", "en"].include? params[:locale]
				I18n.locale = params[:locale] || cookies[:language_preference] || I18n.default_locale
			else
				I18n.locale = I18n.default_locale
			end
		end
	end


	#Overrides RAILS DEFAULT METHOD
	def default_url_options(options = {})
		{locale: I18n.locale}.merge options
	end

	def set_invitation
		if logged_in?
			@invitation = current_user.invitations.build
		end
	end

	#for contact form
	#available in all pages
	def set_suggestion
		@suggestion = Suggestion.new
	end

	 
	private 
		def correct_user
			case params[:controller]
				when "conversations"
					@user_conversation = UserConversation.find_by(user_id: current_user.id, conversation_id: params[:id])
					if @user_conversation
						@user = @user_conversation.user
					end
				when "notifications"
					@notification = Notification.find_by(id: params[:id])
					if @notification
						@user = @notification.user
					end
				when "users" 
					@user = User.find(params[:id])
				when "friend_requests"
					@user = FriendRequest.find(params[:id]).friend
				when "friendships"
					@friendship = Friendship.find(params[:id])
				 	@user = @friendship.user
		 			@friend = @friendship.friend
		 		when "messages"
		 			@user = UserConversation.find_by(user_id: current_user.id, conversation_id: params[:message][:conversation_id]).user
		 		when "pictures"
		 			@user = Picture.find(params[:id]).user
		 		when "profile_pictures"
		 			@user = ProfilePicture.find(params[:id]).user
		 		when "cover_pictures"
		 			@user = CoverPicture.find(params[:id]).user
		 		when "stories"
		 		 	@story = Story.find_by(id: params[:id])
				 	@user = @story.user
				when "subscriptions"
					@user = Subscription.find_by(id: params[:id]).user
				when "user_languages"
					@user = UserLanguage.find(params[:id]).user
				when "user_places"
					@user = UserPlace.find(params[:id]).user
				when "replies"
					@user = Reply.find(params[:id]).user
			end
			redirect_to(unauthorized_access_url) unless current_user?(@user)
		end	

		def photo_upload_dependencies
			@profile_picture = current_user.profile_pictures.build
			@picture = current_user.pictures.build
			@cover_picture = current_user.cover_pictures.build
		end

		def user_language_dependencies
			@user_language = current_user.user_languages.build
		end

		def user_place_dependencies
			@user_place = current_user.user_places.build
		end

		def verify_language_count
		 	if !current_user.speaks_a_language?
		 		flash[:notice] = t('alerts.notice.language.select_one')
		 		redirect_to current_user
		 	end
		end

		def current_languages
			@current_languages = current_user.languages
		end

		def user_speaks_thread_language?
			user_id = params[:user_id]
			#replace by correct_user id, no other user than current_user
			#can create replies or subscriptions. #see comment below for 
			#full refactoring
			
			if controller_name == "replies"
				model_name = params[:reply][:replyable_type].constantize
				model_id = params[:reply][:replyable_id]
			end
			
			if controller_name == "subscriptions"
				model_name = params[:subscription][:subscriptionable_type].constantize
				model_id = params[:subscription][:subscriptionable_id]
			end

			#this logic of validation presence of parameters must be sorted out
			if user_id && model_id && (model_name.to_s == TravelThread.to_s)
				languages_ids = User.find_by(id: user_id).user_languages.pluck(:language_id)
				language_id = model_name.find_by(id: model_id).language_id
				if ! (languages_ids.include? language_id)
					flash[:error] = t('validation.reply.incorrect_language')
					redirect_to root_url
				end
			else
				flash[:error] = "Incorrect number of parameters."
				redirect_to root_url
			end
		end
end
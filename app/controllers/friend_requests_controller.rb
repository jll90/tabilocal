class FriendRequestsController < ApplicationController
	before_action :logged_in_user
	before_action :correct_user, only: [:update, :destroy]

	def create
		@friend_request = current_user.friend_requests.build(friend_request_params)
		if @friend_request.save
			respond_to do |format|
				@relationship = @friend_request
				format.js {render action: "sent_friend_request.js.erb"}
			end
		else
			respond_to do |format|
				format.js {render action: "failed_friend_request.js.erb"}
			end
		end
	end

	def update
		@friend_request = FriendRequest.find_by(id: params[:id])
		if @friend_request
			@friend_request.accept
			@friendship = Friendship.where(user_id: current_user.id, friend_id: @friend_request.user_id).first
			@inverse_friendship = @friendship.inverse
			respond_to do |format|
				format.js {render action: "accept_friend_request.js.erb"}
			end
		else
			respond_to do |format|
				format.js {render action: "failed_friend_request.js.erb"}
			end
		end
	end

	def destroy
		@friend_request = FriendRequest.find_by(id: params[:id])
		if @friend_request
			@friend_id = @friend_request.user_id
			@friend_request.reject
			respond_to do |format|
				format.js {render action: "reject_friend_request.js.erb"}
			end
		else
			respond_to do |format|
				format.js {render action: "failed_reject_request.js.erb"}
			end
		end
	end

	private

	   def friend_request_params
	   	  params.require(:friend_request).permit(:user_id, :friend_id)
	   end

end
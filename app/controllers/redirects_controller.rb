class RedirectsController < ApplicationController

	before_action :logged_in_user

	#this needs to be moved to an api
	def create
		@redirect = Redirect.new(redirect_params)
		if @redirect.save
			render json: @redirect.as_json, status: :created
		else
			render json: {error: "Could not create record"}, status: :bad_request
		end
	end

	#Handle case where id is invalid
	def show
		@redirect = Redirect.find(params[:id])
		render :show
	end

	private 

		def redirect_params
			params.require(:redirect).permit(:link)
		end
end

class UserLanguagesController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: [:destroy]

	def create
		@user_language = current_user.user_languages.build(user_language_params)
		if @user_language.save
			respond_to do |format|
				format.js {render 'user_languages/create.js.erb'}
			end
		else
			respond_to do |format|
				format.js {render js: "alert('language could not be added');"}
			end
		end
	end

	def destroy
		@user_language = UserLanguage.find(params[:id])
		if @user_language
			@user_language.destroy
			respond_to do |format|
				format.js {render 'user_languages/destroy.js.erb'}
			end
		else
			respond_to do |format|
				format.js {render js: "alert('An error ocurred.');"}
			end
		end
	end

	private 

		def user_language_params
			params.require(:user_language).permit(
				:user_id, :language_id, :category)
		end
end

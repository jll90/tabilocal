class InvitationsController < ApplicationController
  
  before_action :logged_in_user

  def create
    @invitation = current_user.invitations.build(invitation_params)
    if @invitation.save
      InvitationMailer.invite_friend(
        @invitation.email, 
        @invitation.recipient_name, 
        @invitation.sender_name, 
        @invitation.content
      ).deliver_now
      render "invitations/email_sent.js.erb"
    else
      render "invitations/email_not_sent.js.erb"
    end
  end


  private

    def invitation_params
      params.require(:invitation).permit(:content, :email, 
        :sender_name, :recipient_name)
    end


end
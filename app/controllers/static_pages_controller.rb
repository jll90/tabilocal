class StaticPagesController < ApplicationController

  before_action :logged_in_user, :verify_language_count, :current_languages, only: [:dashboard]
  
  #MUST MATCH VALUE ON TRAVEL_THREADS CONTROLLER
  THREADS_PER_PAGE = 10

  def home_offline
  	if logged_in?
      redirect_to dashboard_path
    end
  end

  def dashboard
    @tags = Tag.all
    respond_to do |format|
      format.html do
        @filter = params[:sort]
        case @filter
        when "unanswered"
          @travel_threads = TravelThread.all.includes(:place, :language, :tags, :user) #no replies is one reply (opening post)
                .where("languages.id" => @current_languages.ids, "replies_count" => 1).paginate(page: params[:page], per_page: THREADS_PER_PAGE)
        when "most_popular"
          @travel_threads = TravelThread.all.includes(:place, :language, :tags, :user)
                .where("languages.id" => @current_languages.ids).paginate(page: params[:page], per_page: THREADS_PER_PAGE).order(replies_count: :desc)
        else
          @travel_threads = TravelThread.all.includes(:place, :language, :tags, :user)
                .where("languages.id" => @current_languages.ids).paginate(page: params[:page], per_page: THREADS_PER_PAGE).order(updated_at: :desc)
        end

        @countries = Country.select(:country).distinct
                  
        # if params[:page].nil? 
        #   @countries = Country.select(:country).distinct
        #   render :index
        # else
        #   if @travel_threads.length == 0
        #     render @travel_threads, status: 304
        #   else
        #     render @travel_threads, status: 200
        #   end
        # end

        # @recent_travel_threads = TravelThread.all.includes(:place, :language, :tags).where("languages.id" => @current_languages.ids).paginate(page: params[:page], per_page: 10).order(created_at: :desc)
        # @popular_travel_threads = TravelThread.all.includes(:place, :language, :tags).where("languages.id" => @current_languages.ids).paginate(page: params[:page], per_page: 10).order(replies_count: :desc)
        # @unanswered_travel_threads = TravelThread.all.includes(:place, :language, :tags, :user).where("languages.id" => @current_languages.ids, "replies_count" => 1).paginate(page: params[:page], per_page: 10)

        # Does not show users for the moment
        # @recent_users = User.where.not(id: current_user.id).order(created_at: :desc).limit(10)
        # @active_users = User.where.not(id: current_user.id).order(posts_count: :desc).limit(10)
      end
    end
  end

  def contact_us

  end

  def terms_of_use

  end

  def safety

  end

  def objectives

  end

  def faq

  end

  def react_test

  end

  
end

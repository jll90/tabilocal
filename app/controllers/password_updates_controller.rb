class PasswordUpdatesController < ApplicationController

	before_action :logged_in_user

	def create
		@user = current_user.authenticate(params[:user][:original_password])
		if @user
			if validates_new_password == true
				password_updated = @user.update(password: params[:user][:password])
				if password_updated
					render 'password_updates/update_success.js.erb' 
				else
					pass_to_error_handler @user.errors
				end
			else
				pass_to_error_handler t('alerts.error.password_update.not_match')
			end
		else
			pass_to_error_handler t('activerecord.errors.models.user.attributes.password.correct')
		end
	end


	private

	    def user_params
	      params.require(:user).permit(:original_password, :password, :password_confirmation)
	    end

	    def validates_new_password
	    	passwords_are_equal = (params[:user][:password] == params[:user][:password_confirmation])
	    	password_not_empty = (!params[:user][:password].empty?)

	    	validation = (passwords_are_equal && password_not_empty)
	    end

end
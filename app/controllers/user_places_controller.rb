class UserPlacesController < ApplicationController
  
  before_action :logged_in_user
  before_action :correct_user, only: [:destroy]

  
  def create
    @user_place = current_user.user_places.build(user_place_params)

    respond_to do |format|
      if @user_place.save
        format.js {render 'user_places/create_success.js.erb' }
      else
        @errors = @user_place.errors
        format.js {render 'layouts/errorHandler.js.erb'}
      end
    end
  end


  def destroy
    @user_place = UserPlace.find(params[:id])
    respond_to do |format|
      if @user_place
        @user_place.destroy
        format.js {render 'user_places/destroy.js.erb'}
      else
        @errors = @user_place.errors
        format.js {render 'layouts/errorHandler.js.erb'}
      end
    end
  end

  private
    
    def user_place_params
      params.require(:user_place).permit(:user_id, :place_id, :category)
    end
end

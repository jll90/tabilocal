class SuggestionsController < ApplicationController

  before_action :logged_in_user, only: [:index]
  before_action :authorized_user, only: [:index]

  def create
  	@suggestion = Suggestion.new(suggestion_params)
  	if @suggestion.save
  		render 'success.js.erb'
  	else
  		pass_to_error_handler @suggestion.errors
  	end
  end

  def index
    @suggestions = Suggestion.all
  end
  
  private

    def suggestion_params
      params.require(:suggestion).permit(:name, :email, :subject, :message)
    end

    def authorized_user
      valid_emails = ["jesus.luongo.lizana@gmail.com", "reiko.nakagawa.k@gmail.com"]
      if !valid_emails.include? current_user.email
        redirect_to unauthorized_access_url
      end
    end
end

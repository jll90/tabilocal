class LanguageThreadsController < ApplicationController
	before_action :logged_in_user

	def index
		@language_threads = LanguageThread.where(language_thread_query_params).paginate(page: params[:page], per_page: 20)
	end

	def show
		#so the partial comes out right
		@thread = LanguageThread.find(params[:id])
		@replies = @thread.replies
		@reply = Reply.new
		@existing_subscription = current_user.subscriptions.find_by(
			subscriptionable_type: @thread.class.name,
			subscriptionable_id: @thread.id)
		@subscription = Subscription.new
	end

	def new
		@language_thread = current_user.language_threads.build
		@reply = @language_thread.replies.build
		@my_languages = current_user.languages
		@tags = Tag.where(category: "L")
	end

	def create
		@language_thread = current_user.language_threads.build(language_thread_params)
		if @language_thread.save
			flash[:success] = t('alerts.success.thread.created')
			redirect_to @language_thread
		else
			@my_languages = current_user.languages
			@tags = Tag.where(category: "L")
			flash[:error] = t('alerts.error.thread.not_created')
			render :new
		end
	end

	private
		#REQUIRES WORK
		def language_thread_params
			params.require(:language_thread).permit(
				:title, :user_id, :language_id, :about_language_id, replies_attributes: [:content, :user_id], taggings_attributes: [:tag_id])
		end
	

		def language_thread_query_params
			query_params = {language_id: user_languages_ids, about_language_id: user_languages_ids}
		end

		def user_languages_ids
			lang_ids = Array.new
			current_user.user_languages.each {|x| lang_ids << x.language_id}
			return lang_ids
		end
end

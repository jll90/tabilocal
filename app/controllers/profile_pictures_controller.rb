class ProfilePicturesController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: [:update, :destroy]
		
	def create
		@profile_picture = current_user.profile_pictures.build(profile_picture_params)
		if @profile_picture.save
			flash[:success] = t('alerts.success.image.profile_uploaded')
		else
			flash[:error] = errors_to_html @profile_picture.errors
		end
		redirect_to user_photos_path(current_user)
	end

	def update
		@profile_picture = ProfilePicture.find(params[:id])
		current_user.update(profile_pic_id: @profile_picture.id)
		redirect_to user_photos_path(current_user)
	end

	def destroy
		@profile_picture = ProfilePicture.find(params[:id])
		if @profile_picture
			@deleted_picture = @profile_picture
			@profile_picture.destroy
			@notification_message = t('alerts.success.image.profile_deleted')
			render "photos/deleteSuccess.js.erb"
		else
			pass_to_error_handler t('alerts.error.image.profile_not_deleted')
		end		
	end

	private

		def profile_picture_params
			params.require(:profile_picture).permit(:image)
		end
end

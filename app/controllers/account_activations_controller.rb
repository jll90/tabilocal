class AccountActivationsController < ApplicationController

	def edit
		user = User.find_by(email: params[:email])
		if user && !user.activated? && user.authenticated?(:activation, params[:id])
			user.activate
			user.create_first_language
			log_in user
			# removed due to modal
			# flash[:success] = t('alerts.success.account.activated')
			redirect_to user_path(id: user.id, show_modal: true)
		else
			flash[:error] = t('alerts.error.account.invalid_link')
			redirect_to root_url
		end
	end
	
end

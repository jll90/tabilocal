class SubscriptionsController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: [:destroy]
	before_action :user_speaks_thread_language?, only: [:create]	


	def create
		@subscription = current_user.subscriptions.build(subscription_params)
		if @subscription.save
			@existing_subscription = @subscription
			respond_to do |format|
				format.js {render 'subscriptions/create_success.js.erb'}
			end
		else
			respond_to do |format|
				format.js {render 'subscriptions/create_error.js.erb'}
			end
		end
	end

	def destroy
		@subscription = Subscription.find_by(id: params[:id])
		if @subscription
			@thread = @subscription.subscriptionable #passes thread
			@subscription.destroy
			@new_subscription = current_user.subscriptions.build
			respond_to do |format|
				format.js {render 'subscriptions/destroy_success.js.erb'}
			end
		else 
			respond_to do |format|
				format.js {render 'subscriptions/destroy_error.js.erb'}
			end
		end

	end

	private

		def subscription_params
			params.require(:subscription).permit(:subscriptionable_type,
				:subscriptionable_id)
		end

end

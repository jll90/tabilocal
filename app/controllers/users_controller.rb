class UsersController < ApplicationController

	before_action :logged_in_user, only: [:edit, :update, :index, :destroy, :show]
	before_action :correct_user, only: [:edit, :update, :destroy]
	before_action :setting_language_preference?, only: [:update]

	def show
		@user = User.includes(:user_languages, :languages).where(id: params[:id]).first
		if @user
			@languages_known = @user.user_languages.where(category: "K")
			@languages_learning = @user.user_languages.where(category: "L")
			@lived_places = @user.user_places.where(category: "L")
			@traveling_places = @user.user_places.where(category: "T")

			if current_user?(@user)
				photo_upload_dependencies
				user_language_dependencies
				user_place_dependencies
			else
				@relationship = fetch_relationship(current_user.id, @user.id)
			end
		else 
			flash[:error] = t('alerts.error.user.not_exist')
			redirect_to user_path(current_user)
		end
	end

	def index
		
	end

	def new
		if !logged_in?
			@user = User.new
		else
			redirect_to dashboard_path
		end
	end

	def create
		if !logged_in?
			@user = User.new(user_params)
			if @user.save
				@user.send_activation_email
				js_redirect root_url, t('alerts.notice.account.see_activation_email'), "notice"
			else
				pass_to_error_handler @user.errors
			end
		else
			redirect_to dashboard_path
		end
	end

	def edit
		@user = current_user
	end

	def update
		@user = current_user
		custom_params = user_params
		if custom_update custom_params
			if dob = date_updated?
				custom_params["dob"] = dob
				custom_params.delete("dob(3i)")
				custom_params.delete("dob(2i)")
				custom_params.delete("dob(1i)")
			end
			@user_params = user_params_formatter custom_params
			respond_to do |format|
				format.js {render 'update_success.js.erb'}
			end
		else
			pass_to_error_handler @user.errors
		end		
	end

	private
		def user_params
			params.require(:user).permit(:firstname, :lastname,
										:email, :gender, :dob,
										:password,
										:password_confirmation,
										:about_intro,
										:website, :language_preference, :status,
										:gender_privacy, :birthday_privacy,
										:hometown_id,
										:current_residence_id,
										:profile_picture_id,
										:cover_picture_id)
		end

		def setting_language_preference?
		 	if language_preference = params[:user][:language_preference] 
	 			current_user.update(language_preference: language_preference.to_s)
	 			save_language_preference language_preference
	 			I18n.locale = current_user.language_preference
	 			flash[:success] = t('alerts.success.language.changed')
	 			redirect_to edit_user_path(id: current_user.id)
		 	end
		end

		def date_updated?
			updated_date = nil
			if @user.previous_changes.key?("dob")
				updated_date = @user.previous_changes["dob"][1]
			end
			return updated_date
		end

			#to prevent callback profile picture error
			#see profile_picture.rb
		def custom_update(custom_params)
			if custom_params.key?(:profile_picture_id)
				return @user.update_column(:profile_picture_id, custom_params[:profile_picture_id])
			else
				return @user.update(custom_params)
			end
		end
end
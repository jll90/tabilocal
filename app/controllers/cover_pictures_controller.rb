class CoverPicturesController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: [:update, :destroy]

	def create
		@cover_picture = current_user.cover_pictures.build(cover_picture_params)
		if @cover_picture.save
			flash[:success] = t('alerts.success.image.cover_uploaded')
		else
			flash[:error] = errors_to_html @cover_picture.errors
		end
		redirect_to user_photos_path(current_user)
	end

	def update
		@cover_picture = CoverPicture.find(params[:id])
		current_user.update(profile_pic_id: @cover_picture.id)
		redirect_to user_photos_path(current_user)
	end

	def destroy
		@cover_picture = CoverPicture.find(params[:id])
		if @cover_picture
			@deleted_picture = @cover_picture
			@cover_picture.destroy
			@notification_message = t('alerts.success.image.cover_deleted')
			render "photos/deleteSuccess.js.erb"
		else
			pass_to_error_handler t('alerts.error.image.cover_not_deleted')
		end
	end

	private

		def cover_picture_params
			params.require(:cover_picture).permit(:image)
		end
end

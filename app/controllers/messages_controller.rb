class MessagesController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user

	def create
		@message = current_user.messages.build(message_params)
		if @message.save
			@date_string = time_ago_in_words(@message.created_at)
			respond_to do |format|
				format.js {render 'messages/create.js.erb'}
			end
		else
			pass_to_error_handler @message.errors
		end
		
	end

	private
		def message_params
			params.require(:message).permit(:content, :conversation_id)
		end
end
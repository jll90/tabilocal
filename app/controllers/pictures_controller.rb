class PicturesController < ApplicationController

	before_action :logged_in_user
	before_action :correct_user, only: [:destroy]
	

	def create
		@picture = current_user.pictures.build(picture_params)
		if @picture.save
			flash[:success] = t('alerts.success.image.picture_uploaded')
		else
			flash[:error] = errors_to_html @picture.errors
		end
		redirect_to user_photos_path(current_user)
	end

	def destroy
		@picture = Picture.find(params[:id])
		if @picture
			@deleted_picture = @picture
			@picture.destroy
			@notification_message = t('alerts.success.image.picture_deleted')
			render "photos/deleteSuccess.js.erb"
		else
			pass_to_error_handler t('alerts.error.image.picture_not_deleted')
		end
	end

	private

		def picture_params
			params.require(:picture).permit(:name, :caption, :image)
		end
end

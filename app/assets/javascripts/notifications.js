'use strict';

$(document).ready(function(){

	Tabilocal.notifications = (function(){

		var $notifications = $("li.notification");
		var $notificationUnreadCount = $("#notification-unread-count");
		var $noNotificationNotice = $(".no-notifications-notice");
		var $dropdownButton = $(".notifications-container .dropdown-trigger");
		var $dropdownOptions = $(".notifications-container .buttons-container");

		$dropdownButton.on('click', toggleDropdown);
		$(window).on('resize', adjustMobileMenu);

		function toggleDropdown(){
			$dropdownOptions.toggle();
		}

		function showDropdown(){
			$dropdownOptions.show();
		}

		function markAllAsRead(){
			$notifications.each(function(){
				_markAsRead(this);
			});
			_removeCount();
		}

		function _removeCount(){
			//element exists
			if (getUnreadCount() > 0){
				$notificationUnreadCount.remove();
			}
		}

		function _markAsRead(notification){
			$(notification).removeClass("unread");
		}

		function remove(notificationId){
			var classWithIdSelector = [".notification-", notificationId].join("");
			$(classWithIdSelector).remove();
			if (getAllCount() === 0){
				showNoNotificationsNotice();
				_removeCount();
			}
		}

		function removeAll(){
			$notifications.remove();
			showNoNotificationsNotice();
			_removeCount();
		}

		function showNoNotificationsNotice(){
			$noNotificationNotice.show();
		}

		function hideNoNotificationsNotice(){
			$noNotificationNotice.hide();
		}

		function getUnreadCount(){
			return $notificationUnreadCount.length;
		}

		function getAllCount(){
			//dynamically updated
 			return $("#notification-dropdown li.notification").length;
		}

		function init(){
			if (getAllCount() === 0){
				showNoNotificationsNotice();
			} else {
				hideNoNotificationsNotice();
			}
		}

		function adjustMobileMenu(){
			if (window.innerWidth > 780){
				showDropdown();
			}
		}



		init();

		return {
			markAllAsRead: markAllAsRead,
			remove: remove,
			removeAll: removeAll
		};


	})();
});
$(document).ready(function(){
  'use strict';
  Tabilocal.layout = (function(){

    var SCROLL_UP;
    var TRAVEL_THREADS_PATH = "/travel_threads";
    var DASHBOARD_PATH = "/dashboard";
    var STORIES_PATH = "/stories";
    var FRIENDSHIPS_PATH = "/friendships";
    var REPLIES_PATH = "/replies";
    var HOMEPAGE_PATH = "/";
    var LOGIN_PATH = "/login"

    SCROLL_UP = "";
    SCROLL_UP += '<div class="scroll-to-top-button">';
    SCROLL_UP +=  '<div class="fixed-positioner">';
    SCROLL_UP +=   '<i class="fa fa-chevron-circle-up"></i>';
    SCROLL_UP +=  '</div>';
    SCROLL_UP += '</div>';


    var $editButtons, $positioning;
    var invitationModal, uploadPhotoModal;
    var scrollButtons;

    $editButtons = $('.item-wrapper button.edit-item');
    $editButtons.on('click', toggleIcon);

    function toggleIcon (){
      var chevronDownIcon = "<i class='fa fa-chevron-down'></i>";
      var chevronUpIcon = "<i class='fa fa-chevron-up'></i>";

      if ($(this).children().hasClass('fa-chevron-down')){
        this.innerHTML = chevronUpIcon;
      } else {
        this.innerHTML = chevronDownIcon;
      }
    }

    function injectScrollUpBtn(){
      var currentPath = window.location.pathname;

      if (currentPath.indexOf(TRAVEL_THREADS_PATH) > -1){
        //index
        $(".threads-container").append(SCROLL_UP);
        //show
        $(".thread-container").append(SCROLL_UP);
      }

      if (currentPath.indexOf(STORIES_PATH) > -1 || 
      currentPath.indexOf(REPLIES_PATH) > -1 ||
      currentPath.indexOf(FRIENDSHIPS_PATH) > -1){
        $(".profile-panel").append(SCROLL_UP);
      }

      if (currentPath === HOMEPAGE_PATH || 
      currentPath.indexOf(LOGIN_PATH) > -1){
        $(".landing-cover").append(SCROLL_UP);
      }
    }

    injectScrollUpBtn();

    //init modals


    var forgotPasswordModal = new Modal({
      htmlContainer: '.modal-forgot-password',
      openModal: '.forgot-password-button'
    });

    var invitationModal = new Modal({
      htmlContainer: '.modal-invitation',
      openModal: '.invite-friends-button'
    });

    var deleteAccountModal = new Modal({
      htmlContainer: '.modal-delete-account',
      openModal: '.delete-account-button'
    });

    var reportUserModal = new Modal({
      htmlContainer: '.modal-report-user',
      openModal: '.report-user-button'
    });
    

    scrollButtons = (function(){
      var $whatsTabilocal, $howItWorks, $features, $offlineHomeTop;

      $offlineHomeTop = $(".scroll-to-offlinehome-top");
      $whatsTabilocal = $(".scroll-to-what");
      $howItWorks = $(".scroll-to-how");
      $features = $(".scroll-to-features");

      $offlineHomeTop.on('click', {containerId: ".offline-navbar"}, scrollToContainer);
      $whatsTabilocal.on('click', {containerId: "#what-container"}, scrollToContainer);
      $howItWorks.on('click', {containerId: "#how-container"}, scrollToContainer);
      $features.on('click', {containerId: "#features-container"}, scrollToContainer);

      function scrollToContainer(event){
        Utilities.scrollToElement(event.data.containerId, 400);
      }

    })();


    //banners

    $positioning = $('.banners-container > .positioning');

    $(window).scroll(function() {
      if($(this).scrollTop() > 210) {
        $positioning.removeClass('relative', 1000);
        $positioning.addClass('fixed-to-bottom', 1000);
      } else {
        $positioning.removeClass('fixed-to-bottom', 1000);
        $positioning.addClass('relative', 1000); 
      }
    });
      
    var $editorToolbar = $(".editor-toolbar");
    var $boldBrackets = $editorToolbar.find(".bold-brackets");
    var $italicBrackets = $editorToolbar.find(".italic-brackets");
    var $quoteBrackets = $editorToolbar.find(".quote-brackets");
    var $imgBrackets = $editorToolbar.find(".img-brackets");

    $boldBrackets.on('click', function(){
      var textarea = findTextarea();
      Utilities.insertTextAtCursor(textarea, "[b][/b]", document);
    });

    $italicBrackets.on('click', function(){
      var textarea = findTextarea();
      Utilities.insertTextAtCursor(textarea, "[i][/i]", document);
    });

    $quoteBrackets.on('click', function(){
      var textarea = findTextarea();
      Utilities.insertTextAtCursor(textarea, "[q][/q]", document);
    });

    $imgBrackets.on('click', function(){
      var textarea = findTextarea();
      Utilities.insertTextAtCursor(textarea, "[img][/img]", document);
    });

    function findTextarea (){
      var textarea = null;
      var replyForm = document.getElementById("new_reply");
      if (replyForm){
        return document.querySelector("textarea#reply_content");
      }
      var newThreadForm = document.getElementById("new_travel_thread");
      if (newThreadForm){
        return document.querySelector("textarea#travel_thread_replies_attributes_0_content");
      }
      throw("Could not bind preview textarea");
    }

    var $disableWith = $("[data-disable-with]");

    function bindDisableButton(){
      $disableWith.on("click", function(){
        $(this).addClass("processing");
      });  
    }

    function bindEnableButton(){
      $(document).ajaxComplete(function() {
        $disableWith.removeClass("processing");
      });  
    }

    bindEnableButton();bindDisableButton();

    function highlightAnchor(){
      if (window.location.hash.length > 1){
        var hashId = "#" + window.location.hash.substr(1);
        $(hashId).addClass('highlight-updated-field');
        Utilities.scrollToElement(hashId, 1200);
      }
    }

    highlightAnchor();

    $('.slidedown-trigger.switch-button').click(function() {
      $(this).parents().siblings('.content.stories-me').toggle(200);
      $(this).parents().siblings('.content.stories-friends').toggle(200);
      $(this).parents().siblings('.content.questions').toggle(200);
      $(this).parents().siblings('.content.answers').toggle(200);
    });

  })();

});

$(document).ready(function(){
	'use strict';

	Tabilocal.messages = (function(){

		var $unreadCount, $log, $conversationId, $form, $message;

		$unreadCount = $("#unread-messages-count");
		$log = $(".conversation-log .messages");
		$conversationId = $("#message_conversation_id");
		$form = $("form#new_message");
		$message = $form.find("textarea");

		function clearLog(){
			$log.html("");
		}

		function setLog(messagesHtml){
			$log.html(messagesHtml);
			scrollToBottom();
		}

		function getUnreadCount(){
			return parseInt($unreadCount.html());
		}

		function setUnreadCount(updatedCount){
			if ( parseInt(updatedCount) === 0){
        		$unreadCount.html("");
      		} else { 
        		$unreadCount.html(updatedCount);
      		}
		}

		function updateUnreadCount(readMessagesCount){
			var newCount;
      		newCount = getUnreadCount() - parseInt(readMessagesCount);
      		setUnreadCount(newCount);
		}

		function appendMessage(messageHtml){
			$log.append(messageHtml);
			scrollToBottom();
		}

		function setConversationId(id){
			$conversationId.val(id)
		}

		function reset(){
			$message.val("");
		}

		function enableForm(){
			$("form#new_conversation").hide(); //hides other form
			$form.show();
			reset();
		}

		function scrollToBottom(){
			var height = $log[0].scrollHeight;
			$log.scrollTop(height);
		}

		function testEagerLoading(){
			if (window.location.search.indexOf("message_to") > -1){
				scrollToBottom();
			}
		}

		testEagerLoading();

		return {
			clearLog: clearLog,
			setLog: setLog,
			updateUnreadCount: updateUnreadCount,
			appendMessage: appendMessage,
			setConversationId: setConversationId,
			reset: reset,
			enableForm: enableForm
		};

	})();
});
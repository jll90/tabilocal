"use strict";

$(document).ready(function(){

	Tabilocal.friendships = (function(){

		var $friendshipsContainer = $(".friendships-container .content");
		var $friendshipsCountContainer = $(".number-of-friends");

		function removeFriendshipById(id, noticeHtml){
			$(".friendship#" + id).remove();
			decrementFriendshipCount();
			if (getFriendshipCount() === 0){
				addNoFriendshipNotice(noticeHtml);
			}
		}

		function addFriendshipToList(friendshipHtml){
			if (getFriendshipCount() === 0){
				removeNoFriendshipNotice();
			}
			$friendshipsContainer.append(friendshipHtml);
		}

		function decrementFriendshipCount(){
			var newCount;
			newCount = getFriendshipCount() - 1;
			setFriendshipCount(newCount);
		}

		function setFriendshipCount(updatedCount){
			$friendshipsCountContainer.find("span").eq(0).html(parseInt(updatedCount));
		}

		function getFriendshipCount(){
			return parseInt($friendshipsCountContainer.find("span").eq(0).html());
		}

		function removeNoFriendshipNotice(){
			Utilities.removeStaticAlert();
		}

		//html is passed to this function to avoid erb compilation inside this file
		function addNoFriendshipNotice(noticeHtml){
			$friendshipsContainer.append(noticeHtml);
		}

		return {
			removeFriendshipById: removeFriendshipById,
			addFriendshipToList: addFriendshipToList,
			decrementFriendshipCount: decrementFriendshipCount
		};


	})();
});
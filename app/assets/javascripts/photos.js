$(document).ready(function(){
  'use strict';
  Tabilocal.photos = (function(){

    var $albumPanel, $imgMenuBtns, PICTURES_PARTIAL_PATH, IMG_DROPDOWN_SELECTOR;
    var notificationHandler = Tabilocal.notificationHandler;

    $albumPanel = $(".album.profile-panel");
    $imgMenuBtns = $(".img-menu");
  
    // $uploadBtn = $(".upload-button");

    PICTURES_PARTIAL_PATH = "/photos";
    IMG_DROPDOWN_SELECTOR = ".img-edit-dropdown";

    $imgMenuBtns.on('click', toggleImgMenu);

    var ProfilePictureModal = new PhotoModal({
      htmlContainer: '.modal-img-upload.profile-picture',
      openModal: "[photo-model='profile-picture']",
    });

    var CoverPictureModal = new PhotoModal({
      htmlContainer: '.modal-img-upload.cover-picture',
      openModal: "[photo-model='cover-picture']",
    });

    var PictureModal = new PhotoModal({
      htmlContainer: '.modal-img-upload.picture',
      openModal: "[photo-model='picture']",
    });

    function toggleImgMenu(){
      $(this).children(IMG_DROPDOWN_SELECTOR).toggle();
    }

    function repositionImages(){
      $albumPanel.find("img").each(function(){
        if (this.naturalWidth > this.naturalHeight)
          $(this).addClass("landscape");
        else 
          $(this).addClass("portrait");
      });
    }


    if (window.location.pathname.indexOf(PICTURES_PARTIAL_PATH) > -1 ){
      $(window).on("load", function(){
        repositionImages();
      });
    }

    function remove(id, className, message){
      var idSelector = ["#", className.split(/(?=[A-Z])/).join("-"), '-', id].join("");
      idSelector = idSelector.toLowerCase();
      $(idSelector).remove();
      notificationHandler.create(message, "success");
    }

    return {
      remove: remove
    };

  })();

});

$(document).ready(function(){
	'use strict';

	Tabilocal.subscriptions = (function(){

		var $subscriptionStatusContainer;

		$subscriptionStatusContainer = $("#subscription-status");

		function renderSubscriptionBtn(button_html){
			$subscriptionStatusContainer.html(button_html);
		}

		return {
			renderSubscriptionBtn: renderSubscriptionBtn
		};
		
	})();

});
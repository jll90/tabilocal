'use strict';


$(document).ready(function(){

  var $stepProfile, $stepForum, $stepInbox, $stepCount, $profileIcon, $nextStepButton, $previousStepButton, $modal, $closeButton;


  $modal = $(".modal-welcome-tour");
  $stepProfile = $('.step.profile');
  $stepForum = $('.step.forum');
  $stepInbox = $('.step.inbox');
  $stepCount = $('.step-count');
  $nextStepButton = $('.next-step-button');
  $previousStepButton = $('.previous-step-button');
  $closeButton = $modal.find(".end-tour-button");

  $nextStepButton.on('click', goToNextStep);
  $previousStepButton.on('click', goToPreviousStep);
  $closeButton.on('click', closeModal);

  if (shouldFireModal()){
    init();  
  }
  
	function init() {
    showModal();
		hideStepButton.call($previousStepButton);
		hideAllContent();
		showStepContent.call($stepProfile);
		setStepCount(1);
	}

  function showModal(){
    $modal.show();
  }

  function closeModal(){
    $modal.hide();
  }

	function getStepCount() {
		return parseInt($stepCount.html());
	}

	function setStepCount(stepCount) {
		$stepCount.html(parseInt(stepCount));
	}

	function hideAllContent() {
		$stepProfile.hide();
		$stepForum.hide();
		$stepInbox.hide();
	}

	function showStepContent() {
		$(this).show();
	}

	function hideStepButton() {
		$(this).hide();
	}

	function showStepButton() {
		$(this).show();
	}

  function hideAllContent() {
    $stepProfile.hide();
    $stepForum.hide();
    $stepInbox.hide();
  }

	function goToNextStep() {
		hideAllContent();
  	switch(getStepCount()){
  		case 1:
  			showStepButton.call($previousStepButton);
  			setStepCount(2);
  			showStepContent.call($stepForum);
  			showStepButton.call($previousStepButton);
  			break;
  		case 2:
  		  setStepCount(3);
  		  showStepContent.call($stepInbox);
  		  hideStepButton.call($nextStepButton);
  		  break;
  		default: 
  			throw ("Invalid index for step tutorial");
  	}
	}

	function goToPreviousStep() {
		hideAllContent();
  	switch(getStepCount()){
  		case 2:
  			setStepCount(1);
  			showStepContent.call($stepProfile);
  		  hideStepButton.call($previousStepButton);
  			break;
  		case 3:
  		  setStepCount(2);
  		  showStepContent.call($stepForum);
  		  showStepButton.call($nextStepButton);
  		  break;
  		default: 
  			throw ("Invalid index for step tutorial");
  	}
	}

  function shouldFireModal(){
    var fireModal = Utilities.queryStringHas("show_modal");
    return fireModal;
  }
});

'use strict';

$(document).ready(function(){

	Tabilocal.redirects = (function(){

		var $websiteLink = $("#website .item-value");
		$websiteLink.on('click', postRedirect);
		var $bannersLink = $(".banner.clickable");
		$bannersLink.on('click', postRedirect);

		function postRedirect(){
			var link = getLink(this);
			$.ajax({
				method: "POST",
				dataType: "json",
				url: postUrl(),
				async: false,
				data: {redirect: {link: link}}, //server responds 201
				success: function(data, status, response){
					var id = parseId(data);
					redirectToFinal(id);
				},
				error: function(){
					Tabilocal.errorHandler.notify({errors: ["Sorry an error ocurred while you were being redirected. Please try again."]});
				}
			})
		}

		function postUrl(){
			return "/redirects";
		}

		//redirects to REST show, for final redirection
		function redirectToFinal(id){
			var redirectLink = ["/redirects/", id].join("");
			window.open(redirectLink, '_blank');
		}

		function getLink(obj){
			var link;
			if ($(obj).hasClass("item-value")){
				link = $(obj).html();
			} else if ( $(obj).hasClass("banner")){
				link = $(obj).attr("redirect_link");
			} else {
				throw("Cannot determine how to retrieve redirect-link from the element's class.")
			}
			return link;
		}

		//parses id from Response's data
		function parseId(data){
			return data.id;
		}

		function redirectOutside(){
			var finalUrl = $("#final-redirect-link").html();
			if (!Utilities.stringMatcher("lowercase", "http://", finalUrl) && 
				!Utilities.stringMatcher("lowercase", "https://", finalUrl)){
				finalUrl = ["http://", finalUrl].join("");
			}
			setTimeout(function(){
			   window.location.href = finalUrl;
			}, 2500);
		}

		function shouldRedirectOutside(){
			if (Utilities.stringMatcher("lowercase", "/redirects/", window.location.pathname)){
				redirectOutside();
			} 
		}

		shouldRedirectOutside();

	})();
});
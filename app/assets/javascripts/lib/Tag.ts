/// <reference path="../typescript/jquery.d.ts" />
'use strict';

class Tag {
	state: boolean;
	disableCallback: Function;
	enableCallback: Function;
	$el: JQuery;

	constructor(initOptions: any){
		this.state = initOptions.defaultState || false;
		this.$el = initOptions.el;
		this.enableCallback = initOptions.enableCallback;
		this.disableCallback = initOptions.disableCallback;
		this.$el.on('click', () => this.toggleState());
	}

	public toggleState(): void{
		let toggle: boolean;
		if (this.state === false){
			toggle = this.enableCallback();
		} else {
			toggle = this.disableCallback();
		}
		if (toggle === true){
			this.state = !this.state;	
		}
	}

	public enable(): void{
		this.state = true;
	}

	public disable(): void{
		this.state = false;
	}

	public getState(): boolean{
		return this.state;
	}
}
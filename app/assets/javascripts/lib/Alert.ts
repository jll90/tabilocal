'use strict';

module StaticMessage { 
	//emulates a singleton pattern
	export function print(message: string, classes: string[]):string {
		let messageHtml = "";

	  	messageHtml +=  "<div class='alert " + classes.join(' ') +  "'>";
      	messageHtml +=  "<p>" + message + "</p>";
      	messageHtml +=  "<i class='fa fa-close close-button'></i>";
      	messageHtml += "</div>"; 

		return messageHtml;
	}
}
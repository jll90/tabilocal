/// <reference path="../typescript/jquery.d.ts" />
/// <reference path="./Utilities.ts" />

'use strict';

interface QueryBuilderOptions {
	categories: Array<string>;
}

interface QueryParam {
	category: string;
	keyword: string;
}

class QueryBuilder {

	public categories: Array<string>;
	private query: string;
	public params: any;

	constructor(initOptions: QueryBuilderOptions){
		this.categories = initOptions.categories; //defines usable categories
		
		this.params = {}; //becomes object of array, keys are allowed categories
		this.query = ""; //query initialized as empty

		this.initParams();
	}

	public UrlQuery(): string {
		let fullQuery: string = '';
		let keywords: Array<string> = [];

		for (var category of this.categories){
			keywords = this.params[category];
			if (keywords.length > 0) {
				fullQuery += "&" + category + "=" + keywords.join("+");
			}
		}

		this.query = fullQuery;
		return this.query;
	}

	public object(): any {
		return this.params;
	}

	public empty(): boolean {

		let keywords: Array<string> = [];
		
		for (var category of this.categories){
			keywords = this.params[category];
			if (keywords.length > 0) {
				return false;
			}
		}
		return true;
	}

	public addParam(param: QueryParam): void {
		let category: string = param.category;
		if (this.categoryExists(category)) {
			this.addKeywordToCategory(param);
		} else {	
			throw ("Param category does not belong in query categories");
		}
	}

	public removeParam(param: QueryParam): void {
		let category: string = param.category
		if (this.categoryExists(category)){
			this.removeKeywordFromCategory(param);
		} else {
			throw ("Cannot remove param from unexisting category");
		}
	}

	public reset(): void{
		for (var category of this.categories){
			this.params[category] = [];
		}
	}


	/*PRIVATE MEMBERS*/

	private initParams(): void {
		for (var category of this.categories) {
			this.params[category] = [];
		}
	}

	private categoryExists(category: string): boolean {
		return Utilities.inArray(this.categories, category);
	} 

	private addKeywordToCategory(param: QueryParam): void {
		let category: string = param.category;
		let keyword: string = param.keyword;
		this.params[category].push(keyword);
	}

	private removeKeywordFromCategory(param: QueryParam): void {

		let category: string = param.category;
		let length: number = this.params[category].length;
		let index: number = 0;

		for (var keyword of this.params[category] ){
			//i is the keyword
			if (keyword === param.keyword) {
				this.params[category].splice(index, 1);
				//removes the keyword from the category;
			}
			index++;
		}
	}

}
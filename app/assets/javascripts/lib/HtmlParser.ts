/// <reference path="../typescript/jquery.d.ts" />
/// <reference path="./Utilities.ts" />
'use strict';

interface IReplace {
	orig: string;
	repl: string;
}

class HtmlParser {

	$input: JQuery;
	$preview: JQuery;
	parseEl: IReplace[];
	convertStr: string;

	constructor(initOptions: any){
		this.$input = $(initOptions.inputSelector);
		this.$preview = $(initOptions.previewSelector);
		this.parseEl = initOptions.parseEl === undefined ? [] : initOptions.parseEl;
		this.$input.on('keyup', ()=> this.convert())
	}

	public convert(): void{
		this.getInput();
		this.replaceAll();
		this.renderOutput();
	}

	public getInput(): void {
		this.convertStr = this.$input.val();
	}	

	public renderOutput(): void {
		this.$preview.html(this.convertStr);
	}

	public replaceAll():void {
		this.convertStr = Utilities.stringReplaceGroup(this.convertStr, this.parseEl);
	}
}
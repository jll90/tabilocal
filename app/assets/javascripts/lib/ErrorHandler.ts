/// <reference path="./NotificationHandler.ts" />

class ErrorHandler {

	notificationHandler: NotificationHandler;

	constructor (){
		this.notificationHandler = new NotificationHandler();
	}

	public notify(errors: any): void{
		let errStr: string = this.parseErrors(errors);
		this.notificationHandler.create(errStr, "error");
	}

	public parseErrors(errors: any): string {	
		let errStr: string = "<ul>";
		for (var error in errors){
			for (var errMsg of errors[error]){
				errStr += "<li>" + errMsg + "</li>";
			}
		}
		errStr += "</ul>";
		return errStr;
	}
}
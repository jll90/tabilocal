/// <reference path="./NotificationHandler.ts" />
/// <reference path="../typescript/jquery.d.ts" />

'use strict';

class ItemList{

	notificationHandler: NotificationHandler;
	$container: JQuery;
	$itemList: JQuery;
	selector: Function;
	emptyMessage: string;
	successRemoveMessage: string;
	successAddMessage: string;
	$delIcons: JQuery;


	constructor (initOptions: any){
		this.$container = $(initOptions.containerSelector);
		this.$itemList = this.$container.find(".item-value");
		this.selector = initOptions.selector;
		this.emptyMessage = initOptions.emptyMessage;
		this.successAddMessage = initOptions.successAddMessage;
		this.successRemoveMessage = initOptions.successRemoveMessage;
		this.notificationHandler = new NotificationHandler();
	}

	private hasItems(): boolean{
		let count: number = this.$itemList.children(".item-count").length;
		return (count > 0);
	}

	public addItem(html: string): void{
		if (!this.hasItems()){
			this.removeEmptyMessage();	
		}
		this.$itemList.append(html);
		this.notificationHandler.create(this.successAddMessage, "success");
	}

	public removeItem(id: string): void{
		let selector: string = this.selector(id);
		$(selector).remove();
		if (!this.hasItems()){
			this.appendEmptyMessage();
		}
		this.notificationHandler.create(this.successRemoveMessage, "success");
	}

	private appendEmptyMessage(): void{
		this.$itemList.append(this.emptyMessage);
	}

	private removeEmptyMessage(): void{
		let $el = this.$itemList.find("span.none");
		$el.remove();
	}
}
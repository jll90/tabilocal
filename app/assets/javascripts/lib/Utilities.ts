/// <reference path="../typescript/jquery.d.ts" />
'use strict';

module Utilities {

	export function mergeObjects(obj1: any, obj2: any) {
		let key: any, new_obj: any;
		new_obj = void 0;
		key = void 0;
		new_obj = {};

		for (key in obj1) {
			key = key;
			if (obj1.hasOwnProperty(key)) {
		  	new_obj[key] = obj1[key];
			}
		}

		for (key in obj2) {
			key = key;
			if (obj2.hasOwnProperty(key)) {
		  	new_obj[key] = obj2[key];
			}
		}

		return new_obj;
	}


	export function inArray (array: Array<any>, value: any): boolean {
		if (array.indexOf(value) > -1){
			return true;
		} else {
			return false;
		}
	}

	export function toggleActiveClass(className: string): void{
		if ($(this).hasClass(className)){
			$(this).removeClass(className);
		} else {
			$(this).addClass(className);
		}
	}

	export function stringMatcher(criterion: string, searchStr: string, comparedStr: string): boolean {
		switch (criterion) {
			case "lowercase":
				let searchStrLower = searchStr.toLowerCase();
				let comparedStrLower = comparedStr.toLowerCase();
				if (comparedStrLower.indexOf(searchStrLower) > -1) {
					return true;
				} else {
					return false;
				}
			default:
				throw ("Please select a matching criterion");
		}
	}

	export function isEmpty(testVar: any): boolean{
		let errorMsg: string;
		errorMsg = "This function does not support this type of variable ";
		errorMsg += "or the variable doesn't have the length property";
		let type = typeof testVar
		switch(type){
			case "string":
				return testVar.length === 0;
			case "object":
				if (testVar instanceof Array){
					return testVar.length === 0;
				} else {
					for (var prop in testVar){
						return false;
					} 
					return true;
				}
			case "number": 
			default:
				throw (errorMsg);
		}
	}

	export function nonEmpty(testVar: any): boolean{
		return !isEmpty(testVar);
	}

	export function placeSuggestionsHtml(suggestions: any): string{
		let html: string = "";
		for (var suggestion of suggestions){
			html += "<li class='place-suggestion'><a href='#' tab-index='-1'>" + suggestion.fullname + "</a></li>";
		}
		return html;
	}

	export function noPlaceHtml(): string{
		let html: string;
		html = "<li class='place-suggestion'>No hay resultados</li>";
		return html;
	}

	export function stringReplace(str: string, orig: string, repl: string): string {
		return str.split(orig).join(repl);
	}

	export function stringReplaceGroup(str: string, replGroup: any){
		let parseStr = str;
		for (var el of replGroup){
			parseStr = stringReplace(parseStr, el.orig, el.repl);
		}
		return parseStr;
	}

	export function unescapeHtml(str: string): string {
		var less_than_regex = /<(?!b>|\/b>|i>|\/i>|div|\/div>|img)/;
		var less_than_repl = "&lt";

		// var greater_than_regex = />(?<!<b>|<\/b>|<i>|<\/i>|'>|<\/div>)/;
		// var greater_than_repl = "&gt";
		
		let unescapedStr = str;

		unescapedStr = unescapedStr.replace(less_than_regex, less_than_repl);
		// unescapedStr = unescapedStr.replace(greater_than_regex, greater_than_repl);

		return unescapedStr;
	}

	export function insertTextAtCursor(field: any, txt: string, document: any): void{
		var sel: any;
			//IE support
		if (document.selection) {
			field.focus();
			sel = document.selection.createRange();
			sel.text = txt;
		}
		//MOZILLA and others
		else if (field.selectionStart || field.selectionStart === '0') {
			var startPos = field.selectionStart;
			var endPos = field.selectionEnd;
			field.value = field.value.substring(0, startPos)
				+ txt
				+ field.value.substring(endPos, field.value.length);
			field.selectionStart = startPos + txt.length;
			field.selectionEnd = startPos + txt.length;
		} else {
			field.selectionEnd = startPos + txt.length;
		}
		
	}

	export function parseQueryParams(): any{
		let paramsStr: string = window.location.href.split("?")[1];
		if (paramsStr !== undefined){
			let paramsArr: string[] = paramsStr.split("&");
			let paramsHash: any = {};

			for (var param of paramsArr){
				let key: string = param.split("=")[0];
				let value: string = param.split("=")[1];
				paramsHash[key] = value;
			}
			return paramsHash;
		} else {
			return {};
		}
	}

	export function queryStringHas(key: string): string{
		let hash: any = parseQueryParams();
		if (hash.hasOwnProperty(key)){
			return stringReplace(hash[key], "%20", " ");
		} else {
			return null;
		}
	}

	export function scrollToElement(selector: string, inputSpeed: number):void {
		 let speed: number = inputSpeed === undefined ? 1000 : inputSpeed; 
		 $('html, body').animate({
            scrollTop: $(selector).offset().top
          }, speed);  
	}

	export function removeStaticAlert(): void {
		$(".alert.static").remove();
	}
}
/// <reference path="../typescript/jquery.d.ts" />
/// <reference path="./Utilities.ts" />
'use strict';

interface AutocompleteOptions {
	containerSelector: string;
	inputSelector: string;
	matchingProperty: string;
	noResultsHtml: Function;
	afterSelect: string;
	onSelect?: Function;
	onDelPress?: Function;
	html?: Function;
	remote?: boolean;
	remoteUrl?: string;
}


class Autocomplete {
	keys: any = {
		BACKSPACE: 8,
		ENTER: 13,
		UP: 38,
		DOWN: 40
		// LEFT: 37,
		// RIGHT: 39,
	};

	data: any[];
	suggestions: any[];
	suggestionsLimit: number;
	matchingProperty: string;
	$container: JQuery;
	$input: JQuery;
	highlightedIndex: number;
	prevSearch: string;
	onSelect: Function;
	onDelPress: Function;
	html: Function;
	remote: boolean;
	remoteUrl: string;
	noResultsHtml: Function;
	afterSelect: string;


	constructor(initOptions: AutocompleteOptions){
		this.data = [];
		this.suggestions = [];
		this.suggestionsLimit = 3;
		this.matchingProperty = initOptions.matchingProperty;
		this.$container = $(initOptions.containerSelector);
		this.$input = this.$container.find("input.atc");
		this.$input.on('keyup', e=> this.pressedKey(e));
		this.onSelect = initOptions.onSelect;
		this.onDelPress = initOptions.onDelPress || function(){};
		this.html = initOptions.html;
		this.noResultsHtml = initOptions.noResultsHtml;
		this.prevSearch = null;
		this.remote = initOptions.remote || false;
		this.remoteUrl = initOptions.remoteUrl || null;
		this.afterSelect = initOptions.afterSelect === 'persist' ? 'persist' : 'clear';

		$(document).on('click', this.$container.selector + ' .suggestions-dropdown li', (e) => this.selectSuggestion(e));
	}

	public setData(data: any[]): void {
		this.data = data;
	}

	public getData(): any {
		return this.data;
	}

	public getVal(): string {
		return this.$input.val();
	}

	public setSuggestions(data: any[]): void {
		this.suggestions = data;
	}

	public getSuggestions(): any {
		return this.suggestions;
	}

	private pressedKey(e: JQueryEventObject): void{
		let keyCode: number = e.which;
		let searchStr: string = $(e.target).val();

		switch(keyCode){
			case this.keys.BACKSPACE:
				if (Utilities.isEmpty(searchStr) && Utilities.isEmpty(this.prevSearch)) {
					this.onDelPress();
				}
				break;
			case this.keys.ENTER:
				if (Utilities.nonEmpty(this.suggestions)){
					this.onSelect(this.suggestions[this.highlightedIndex]);
					this.reset();				
				}
				break;
			case this.keys.UP:
				if (this.highlightedIndex > 0){ 
					this.highlightedIndex--;
					this.highlightSuggestion();
				}
				break;
			case this.keys.DOWN:
				if (this.highlightedIndex < this.suggestions.length - 1){
					this.highlightedIndex++;
					this.highlightSuggestion();
				}
				break;
			default:
				if (Utilities.nonEmpty(searchStr)){					
					if (this.remote === true){
						this.queryResource(searchStr);
					} else {
						this.filterData(searchStr);
						this.displaySuggestionsHtml();
					}
				} else {
					this.reset();
				}
				break;
		}

		this.prevSearch = searchStr;
		
	}

	private filterData(searchStr: string): void{
		this.resetSuggestions();
		for (var item of this.getData()){
			if (this.stringMatch(searchStr, item[this.matchingProperty])){
				this.addSuggestion(item);
			}
		}
	}

	private displaySuggestionsHtml(){
		let html: string = "";
		this.reset();
		html += "<ul class='suggestions-dropdown'>";
		if (Utilities.nonEmpty(this.suggestions)){
			html += this.html(this.suggestions);
		} else {
			html += this.noResultsHtml();
		}
		html += "</ul>"
		this.$container.append(html);
		this.highlightSuggestion();
	}

	private clearSuggestionsHtml(){
		this.$container.find(".suggestions-dropdown").remove();
	}


	private stringMatch(searchStr: string, comparedStr: string): boolean {
		return Utilities.stringMatcher("lowercase", searchStr, comparedStr);
	}

	private resetSuggestions(): void{
		this.suggestions = [];
	}

	private addSuggestion(item: any): void{
		this.suggestions.push(item);
	}

	private highlightSuggestion(): void {
		let index = this.highlightedIndex;
		let $suggestionsDropdown: JQuery = this.$container.find(".suggestions-dropdown");
		$suggestionsDropdown.children().removeClass("active");
		$suggestionsDropdown.children().eq(index).addClass("active");
		$suggestionsDropdown.children().eq(index).find("a").focus();
		//returns focus to the input box
		this.$input.focus();
	}

	private selectSuggestion(e: JQueryEventObject): void {
		let index = $(e.target).index();
		this.onSelect(this.suggestions[index]);
		this.resetSuggestions();
		this.clearSuggestionsHtml();
	}

	private reset(){
		this.clearSuggestionsHtml();
		this.highlightedIndex = 0;
	}

	private clearVal(): void{
		this.$input.val("");
	}

	private persistVal(value: string): void{
		this.$input.val(value);
	}

	private queryResource(searchStr: string): any {
		let self = this;
		$.ajax({
			method: "GET",
			dataType: "JSON",
			url: this.remoteUrl + "?q=" + searchStr,
			success: function(data){
				self.setData(data);
				self.setSuggestions(data);
				self.displaySuggestionsHtml();
			},
			error: function(){
				alert("Unhandled error has ocurred");
			}
		});
	}
}
/// <reference path="../typescript/jquery.d.ts" />
/// <reference path="./Utilities.ts" />

'use strict';
//can implement an interface in the future

class NotificationHandler {

	html: string;
	alertClass: string;
	message: string;
	$body: JQuery;
	alertTimeout: any;

	constructor(){
		this.html;
		this.message;
		this.alertClass;
		this.$body = $("body");
		this.alertTimeout;
		this.$body.on('click', '.alert .close-button', () => this.destroy());
		this.alertPresent();
	}

	public create(message:string, alertClass:string): void{
		if (message === undefined || alertClass === undefined){
			throw ("A flash message requires both an alert an a class");
		} else {
			this.destroy();
			this.message = message;
			this.alertClass = alertClass;	
			this.buildHtml();
			this.show();
		}
	}

	private show(): void{
		this.$body.append(this.html);
		$(".alert.fixed").fadeIn("500");
		// this.alertTimeout = setTimeout(this.destroy, 8000);
	}

	private destroy(): void {
		$(".alert.fixed").remove();
		clearTimeout(this.alertTimeout);
	}

	private buildHtml(): void {
		this.html = "";
		this.html += "<div class='alert fixed " + this.alertClass + "'>";
		this.html += "<div class='alert-content-wrapper'>"
        switch(this.alertClass){
			case "success":
				this.html += "<i class='fa fa-check alert-icon'></i>";
				break;
			case "error":
				this.html += "<i class='fa fa-exclamation-triangle alert-icon'></i>";
				break;
			case "notice":
				this.html += "<i class='fa fa-flag-o alert-icon'></i>";
				break;
			case "warning":
				this.html += "<i class='fa fa-exclamation-triangle alert-icon'></i>";
				break;
			default:
				throw ("Only the classes success, error, notice and warning are supported");
        }
        this.html += "<p>" + this.message + "</p>";
        this.html += "</div>";
        this.html += "<i class='fa fa-times close-button'></i>";
        this.html += "</div>"; 
	}

	private alertPresent(): void{
		let message:string;
		let className: string;
		if ((message = Utilities.queryStringHas("alert")) && (className = Utilities.queryStringHas("class"))){
			this.create(message, className);
		}
	}
}
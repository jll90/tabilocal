'use strict';

class EventEmitter{

    private static instance: EventEmitter;
    private events: any;
    
    constructor(){
        if (EventEmitter.instance){
            throw new Error("Error - use EventEmitter.getInstance()");
        }
        this.events = {};
    }

    public static getInstance(): EventEmitter {
        EventEmitter.instance = EventEmitter.instance || new EventEmitter();
        return EventEmitter.instance;
    }

    public on(eventName: string, fn: Function) {
        this.events[eventName] = this.events[eventName] || [];
        this.events[eventName].push(fn);
    }

    public off(eventName: string, fn: Function) {

        if (this.events[eventName]) {
            for (var i = 0; i < this.events[eventName].length; i++) {
                if (this.events[eventName][i] === fn) {
                    this.events[eventName].splice(i, 1);
                    break;
                }
            };
        }
    }

    public trigger(eventName: string) {

        if (this.events[eventName]) {
            this.events[eventName].forEach(function(fn: Function) {
                fn();
            });
        }
    }

}
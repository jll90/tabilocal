/// <reference path="../typescript/jquery.d.ts" />
"use strict";

interface ModalOptions {
	htmlContainer: string;
	openModal: string;
}

interface PhotoModalOptions {
	htmlContainer: string;
	openModal: string;
	profilePictureForm: string;
	pictureForm: string;	
	coverPictureForm: string;
	selectImgCallback: Function;
}

class Modal {

	CLOSE_BUTTON_SELECTOR: string;
	SCROLLABLE_SELECTOR: string;
	OVERFLOW_HIDDEN_SELECTOR: string;

	$body: JQuery;
	$htmlContainer: JQuery;
	$openModal: JQuery;
	$closeModal: JQuery;

	constructor(initOptions: ModalOptions){
		this.CLOSE_BUTTON_SELECTOR = '.close-button';
		this.SCROLLABLE_SELECTOR = 'scrollable-y';
		this.OVERFLOW_HIDDEN_SELECTOR = 'overflow-hidden';

		this.$body = $("body");
		this.$htmlContainer = $(initOptions.htmlContainer);
		this.$openModal = $(initOptions.openModal);
		this.$closeModal = this.$htmlContainer.find(this.CLOSE_BUTTON_SELECTOR);

		//Event added for firefox support
		this.$openModal.on('click', (event) => this.openModal(event) );
		this.$closeModal.on('click', () => this.closeModal() );
	}

	//To Do: Add definitions - Remove any.
	openModal(e: any):void{
		this.$htmlContainer.slideDown(200);
		this.$htmlContainer.addClass(this.SCROLLABLE_SELECTOR);
		this.$body.addClass(this.OVERFLOW_HIDDEN_SELECTOR);
	}

	closeModal():void{
		this.$htmlContainer.slideUp(200);
		this.$htmlContainer.removeClass(this.SCROLLABLE_SELECTOR);
		this.$body.removeClass(this.OVERFLOW_HIDDEN_SELECTOR);
	}
	
}

class PhotoModal extends Modal {
	$form: JQuery;
	$fileField: JQuery;
	$uploadBtn: JQuery;
	$circularInitBtn: JQuery;
	$chooseImgBtn: JQuery;
	$submitBtn: JQuery;
	$imgPreview: JQuery;
	selectImgCallback: Function;
	//must find correct element type
	fileField: any;

	constructor(initOptions: PhotoModalOptions){
		super(initOptions);
		this.$form = this.$htmlContainer.find("form");
		this.$imgPreview = this.$htmlContainer.find("img");
		this.fileField = document.querySelector(this.$htmlContainer.selector + " " +  "input[type='file']");
		this.$fileField = this.$form.find("input[type='file']");
		this.$circularInitBtn = this.$htmlContainer.find("div.circular-img-button");
		this.$submitBtn = this.$htmlContainer.find("input[type='submit']");
		this.$circularInitBtn.on('click', ()=> this.openFileSelect());
		this.$chooseImgBtn = this.$htmlContainer.find(".select-img-button");
		this.$chooseImgBtn.on('click', ()=> this.openFileSelect());
		this.selectImgCallback = initOptions.selectImgCallback;

		var self = this;

		//prevents throwing undefined or null
		if (this.fileField){

			this.fileField.onchange = function(event: any){
				let file: any = event.target.files[0];
				if (file){
					self.$imgPreview.attr("src", URL.createObjectURL(file));
					self.setBtnState("imgSelected");
				} else {
					self.$imgPreview.removeAttr("src");
					self.setBtnState("imgNotSelected");
				}
			}
		}
	}

	openFileSelect(): void{
		this.$fileField.trigger('click');
	}

	setBtnState(state: string): void {
		if (state === "imgSelected") {
			this.$chooseImgBtn.css("display", "inline-block");
			this.$submitBtn.css("display", "inline-block");	
			this.$circularInitBtn.hide();
		} else {
			this.$chooseImgBtn.css("display", "none");
			this.$submitBtn.css("display", "none");
			this.$circularInitBtn.show();
		}
	}

}
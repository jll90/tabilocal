$(document).ready(function(){
  'use strict';
  Tabilocal.dashboard = (function(){

    var MAP_SELECTOR = 'map-dashboard';
    var autocomplete, autocompleteOptions;
    var $slideinProfile = $(".slidein-profile");
    var $profileCloseBtn = $slideinProfile.find(".close-button");
    var $mapContainer = $(".map-container");   

    autocompleteOptions = {
      containerSelector: "#dashboard-autocomplete",
      matchingProperty: "name",
      remote: true,
      remoteUrl: "/places",
      onSelect: function(selectedPlace){
        getProfileInfo(selectedPlace.id);
        this.persistVal(selectedPlace.fullname);
        var lat = selectedPlace.latitude;
        var lng = selectedPlace.longitude;
        Tabilocal.map.center(lat, lng);
        showProfile();
      },
      html: Utilities.placeSuggestionsHtml,
      noResultsHtml: Utilities.noPlaceHtml
    };

    $profileCloseBtn.on('click', hideProfile);

    function showProfile(){
      $slideinProfile.show();
      $slideinProfile.animate({ right: '0' }, 200);
    }

    function hideProfile(){
      $slideinProfile.hide();
      $slideinProfile.animate({ right: '-250px' }, 200);
    }

    function getProfileInfo(placeId){
      $.ajax({
        method: "GET",
        url: [placeUrl(placeId), "?q=summary"].join(""),
        dataType: "JSON",
        success: function(data){ //responseHandler
          updateProfileValues(data);
        },
        error: function(){
          Tabilocal.errorHandler.notify("Data could not be loaded");
        }
      });
    }

    function updateProfileValues(data){
      var $placeSummary = $(".slidein-profile");
      var $name = $placeSummary.find(".location-name");
      var $natives = $placeSummary.find("#number-of-natives");
      var $residents = $placeSummary.find("#number-of-residents");
      var $travelers = $placeSummary.find("#number-of-travelers");
      var $threads = $placeSummary.find("#number-of-threads");
      var $profileLink = $placeSummary.find("#place-profile-link");

      $name.html(data.name_with_country);
      $natives.find(".number").html(data.natives);
      $residents.find(".number").html(data.residents);
      $travelers.find(".number").html(data.visiting + data.visited);
      $threads.find(".number").html(data.threads);

      $natives.attr("href", placeUrl(data.id) + "?select=natives");
      $residents.attr("href", placeUrl(data.id) + "?select=residents" );
      $travelers.attr("href", placeUrl(data.id) + "?select=travelers");
      $threads.attr("href", placeUrl(data.id) + "?select=threads");
    }

    function placeUrl(id){
      return "/place/" + id;
    }

    if (document.getElementById(MAP_SELECTOR)){
      Tabilocal.map.init(MAP_SELECTOR);  
      autocomplete = new Autocomplete(autocompleteOptions);
    }

  })();

  
});



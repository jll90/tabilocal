$(document).ready(function() {
  'use strict';
//shared
  $('.slidedown-trigger').click(function() {
    $(this).siblings('.slidedown-content').slideToggle(200);
  });

  Tabilocal.navbar = (function(){

    var $body, $navbar, $dropdownIcon, $selectedLanguage, $dropdowns;

    $body = $('body');
    $navbar = $body.find('ul.navbar');
    $dropdownIcon = $navbar.find('.offline-burger-icon');
    $selectedLanguage = $('.selected-language');
    $dropdowns = $('.dropdown-content');

    $selectedLanguage.on('click', toggleLanguageMenu);
    $dropdownIcon.on('click', toggleMobileNavbar);
    $(window).on('resize', adjustMobileNavbar);

    $(document).on("click", "body", function(e) {
      var $tar, $trigger, $dropdown;
      $tar = $(e.target);

      if ($tar.parents(".dropdown-content").length === 1) {
        //keeps element open
      } else if ($trigger = findTrigger($tar)){
        $dropdown = $trigger.find(".dropdown-content");

        if ($dropdown.css("display") === "block"){
          $dropdown.hide();
        } else {
          hideDropdowns();
          $dropdown.show();  
        }
      } else { 
        hideDropdowns();
      }
    });

   
    function toggleLanguageMenu(){
      $selectedLanguage.siblings('.unselected-language').slideToggle(200);
    }

    function hideDropdowns(){
      $dropdowns.hide();
    }

    function toggleMobileNavbar(){
      $dropdownIcon.siblings('li').slideToggle(200);
      $navbar.toggleClass('darker-background', 200);
    }

    function adjustMobileNavbar(){
      if (window.innerWidth > 780){
        $navbar.removeClass('darker-background', 200);
        $dropdownIcon.siblings('li').css('display', 'inline-block', 200);
      }
      else {
        if ($navbar.hasClass('darker-background'))
          $dropdownIcon.siblings('li').css('display', 'list-item');
        else
          $dropdownIcon.siblings('li').css('display', 'none');
      }
    }

    function findTrigger($el){
      var $trigger;
      if ($el.hasClass("dropdown-trigger")){
        return $el;
      }

      //optimization can find a dropdown content then a trigger to simply logic when traversing
      //the dom tree
      $trigger = $el.parents(".dropdown-trigger");
      if ($trigger.length === 1){
        return $trigger;
      }

      return null;
    }

  })();

});
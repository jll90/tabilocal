$(document).ready(function(){
  'use strict';

  var PROFILE_MAP_SELECTOR = "profile-map"

  Tabilocal.places = (function(){
    var $tabLink = $('ul.tabs li.tab-link');
    var $tabContent = $('.tab-content');

    var $nativesTab = $("[data_tab='natives-tab']");
    var $residentsTab = $("[data_tab='residents-tab']");
    var $threadsTab = $("[data_tab='threads-tab']");
    var $travelersTab = $("[data_tab='travelers-tab']");

    $tabLink.click(function() {
      var tabId = $(this).attr('data_tab');

      $tabLink.removeClass('current');
      $tabContent.removeClass('current');

      $(this).addClass('current');
      $("#" + tabId).addClass('current');
    });

    if (document.getElementById(PROFILE_MAP_SELECTOR)){
      var place = getPlaceData();
      Tabilocal.map.init(PROFILE_MAP_SELECTOR, "static", place.latitude, place.longitude);
    }

    function getPlaceData(){
      return JSON.parse($(".place-json").attr("data"));
    }

    //selects the defaulttab to be shown
    function selectDefaultTab(){
      var queryValue = Utilities.queryStringHas("select");
      switch(queryValue){
        case "natives":
          $nativesTab.trigger("click");
          break;
        case "residents":
          $residentsTab.trigger("click");
          break;
        case "travelers":
          $travelersTab.trigger("click");
          break;
        case "threads":
          $threadsTab.trigger("click");
          break;
      }
    }

    selectDefaultTab();

  })();



});
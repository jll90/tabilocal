$(document).ready(function(){
  'use strict';
  Tabilocal.replies = (function (){
    var $repliesContainer, $replyText, $previewContainer;
    var previewParser;

    

    $repliesContainer = $("#replies-list");
    $replyText = $("#reply_content");
    $previewContainer = $(".preview-container");

    function addReply(safeReply){
      //this function parses the content of the whole reply and unescapes it
      var parsedReply = parseReply(safeReply);
      $repliesContainer.append(parsedReply);
    }

    previewParser = new HtmlParser({
      inputSelector: "textarea#reply_content",
      previewSelector: ".preview-container",
      parseEl: Tabilocal.htmlParserFullOptions
    });

    function unescapeRepliesContent(){
      $(".reply-content").each(function(){
        var safeStr = $(this).html();
        var unescapedStr = unescapeReply(safeStr);
        $(this).html(unescapedStr);
      });
    } 

    function unescapeReply(safeReply){
      var replyHtml = convertToHtml(safeReply);
      var unescapedReply = unescapeHtml(replyHtml);
      return unescapedReply;
    }

    function convertToHtml(reply){
      return Utilities.stringReplaceGroup(reply, Tabilocal.htmlParserFullOptions);
    }

    function unescapeHtml(html){
      return Utilities.unescapeHtml(html);
    }

    function clearPreview(){
      $previewContainer.html("");
    }

    function clearText(){
      $replyText.val("");
    }

    function parseReply(replyHtml){
      var author = $(replyHtml).find('.author')[0].outerHTML;
      var postedDate = $(replyHtml).find(".posted-date")[0].outerHTML;
      var replyContent = $(replyHtml).find(".reply-content")[0].outerHTML;
      var unescapedReplyHtml = unescapeReply(replyContent);      
      var parsedReply = "<div class='reply'>" + postedDate + author + unescapedReplyHtml + "<hr></div>";
      return parsedReply;
    }

    unescapeRepliesContent();   


    return {
    	addReply: addReply,
    	clearText: clearText,
      clearPreview: clearPreview
    };
  })();
  
});
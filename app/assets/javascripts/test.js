'use strict';

$(document).ready(function(){

/*scroll to top button*/
  var $scrollToTopButton = $('.scroll-to-top-button');

	$(window).scroll(function() {
		if ($(window).scrollTop() > $(window).height()) {
			$scrollToTopButton.fadeIn()
			$scrollToTopButton.click(function() {
				$('html, body').stop().animate({scrollTop:0}, '1000');
			});
		}
		else {
			$scrollToTopButton.fadeOut()
		}
	});

/*menu slidein on Landing*/
  var $slideinMenu = $('.slidein-menu');
  var $body = $('.grid-container');
  var $transparentLayer = $('.transparent-layer');
  var $closeButton = $('.slidein-menu .close-button');

  function isMobileLayout(){
    return ($(window).width() < 761);
  }

  $('.offline-burger-icon').click(function() {
    if (isMobileLayout()) {
      $slideinMenu.show();
      $slideinMenu.animate({ left: '0' }, 200);
      $body.addClass('screen-lock');
      $transparentLayer.addClass('grayscale-background');
    }
  });

  $closeButton.click(function() {
    if (isMobileLayout()) {
      $slideinMenu.hide();
      $slideinMenu.animate({ left: '-250px' }, 200);
      $body.removeClass('screen-lock');
      $transparentLayer.removeClass('grayscale-background');
    }
  });

  $(window).resize(function() {
    if (isMobileLayout()) {
      $slideinMenu.hide();
      $slideinMenu.css( 'left', '-250px' );
      $body.removeClass('screen-lock');
      $transparentLayer.removeClass('grayscale-background');
    }

    else if (!isMobileLayout()) {
      $slideinMenu.show();
      $slideinMenu.css( 'left', 0 );
      $body.removeClass('screen-lock');
      $transparentLayer.removeClass('grayscale-background');
    }
  });

});
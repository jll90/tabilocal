'use strict';

$(document).ready(function(){

  Tabilocal.suggestions = (function(){
  	var $form;

  	$form = $("form#new_suggestion");

  	function clearForm(){
  		$form.children("input[type='text'], input[type='email'], textarea").val('');
  	}

  	return {
  		clearForm: clearForm
  	};


  })();

});
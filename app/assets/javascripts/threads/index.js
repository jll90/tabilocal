$(document).ready( function() {
  'use strict';

  Tabilocal.threads.index = (function(){

    var inArray = Utilities.inArray;

    var indexPaginator;
    var query
    var tabsShown;
    var $queryable, $filteringTab, $resetFilterButton;
    var $htmlContainer;
    var BASE_URL;
    var THREADS_SHOWN;
    

    var threads = {
      $: $(".thread"),

      eventEmitter: EventEmitter.getInstance(),

      refresh: function(){
        this.$ = $(".thread");
      },

      filter: function(queryObject){
        var self = this;
        if (!query.empty()){
          this.$.each(function(){
            //this is now one of the threads
            if (self.match.call(this, queryObject)){
              $(this).show();
            } else {
              $(this).hide();
            }
          });
        } else {
          this.$.each(function(){
            //this is now of the threads
            $(this).show();
          });
        }
      },

      match: function(queryObject){

        var threadData, place, language, tags, length;
        var placeMatch, languageMatch, tagMatch;

        threadData = JSON.parse($(this).attr("thread-data"));

        placeMatch = languageMatch = tagMatch = false;

        place = threadData.place[0];
        language = threadData.language[0];
        tags = threadData.tags;
      
        if (queryObject.places.length  === 0 || inArray(queryObject.places, place.country)){
          placeMatch = true
        } 

        if (queryObject.languages.length  === 0 || inArray(queryObject.languages, language)){
          languageMatch = true
        } 
        
        length = tags.length;

        for (var i=0; i < length; i++){
          if (queryObject.tags.length  === 0 || inArray(queryObject.tags, tags[i])){
            tagMatch = true;
          }
        }   

        return (placeMatch && languageMatch && tagMatch);
      }
    };

    threads.eventEmitter.on('pagination:complete', function(){
        threads.refresh();
        threads.filter(query.object());
    });


    $htmlContainer = $(".threads-container");

    BASE_URL = "/travel_threads/";
    tabsShown = 0;

    indexPaginator = new Paginator ({
      loadMoreBtn: "button.load-more-button",
      htmlContainer: $htmlContainer.selector,
      baseUrl: BASE_URL,
      currentPage: 2,
      callback: threads.refresh
    });

    $filteringTab = $(".filtering-tab");
    $resetFilterButton = $(".reset-filter-button");

    $filteringTab.on('click', function(){

      var dataAttr = $(this).attr("data-attr");
      var tabActive = $(this).is('[active]');

      if (tabActive === false){
        $(this).addClass('current');
        $(this).attr('active', 'true');
        tabsShown++;
      } else {
        $(this).removeClass('current');
        $(this).removeAttr('active');
        tabsShown--;
      }

      $(".filtering-options[data-attr='" + dataAttr + "']").toggle();

      if (tabsShown === 0){
        $resetFilterButton.hide();  
      } else {
        $resetFilterButton.show();
      }
    });

    $resetFilterButton.on('click', function(){
      $queryable.each(function(){
        var queried;

        queried = $(this).is('[queried]');
        if (queried === true){
          $(this).removeAttr('queried', toggleColor.call(this));
        }
      });
      query.reset();
      threads.filter(query.object());
    });

    $queryable = $("[queryable='true']");
    $queryable.on('click', toggleParam);

    query = new QueryBuilder ({
      categories: ['languages', 'places', 'tags']
    });

    function toggleParam(){
      var queryParam, queried;

      queryParam = {
        category: "",
        keyword: ""
      };
      queried = null;

      queryParam.category = $(this).attr('queryable-category');
      queryParam.keyword = $(this).attr('queryable-keyword');
      queried = $(this).is('[queried]');

      if (queried === false){
        query.addParam(queryParam);
        $(this).attr('queried', 'true', toggleColor.call(this));
      } else {
        query.removeParam(queryParam);
        $(this).removeAttr('queried', toggleColor.call(this));
      }

      threads.filter(query.object());
    }

    function toggleColor(){
      Utilities.toggleActiveClass.call(this, "active");
    }
     
    })();

});
'use strict';

Tabilocal.lib.QueryBuilder = function QueryBuilder(initOptions){

	var self;

	this.query = "";
	this.queryCategories = {};
	this.queryableParams = initOptions.

	this.setQueryCategories = function(){
		for (category in initOptions.categories){
			self.queryCategories[category] = [];
			//inits empty category
		}
	}

	self = this;
	
	this.buildQuery =  function(){
		var fullQuery;
		fullQuery = "";

		for (var category in self.queryCategories){
			if (category.length > 0){
				fullQuery += "&" + category + "=" + self.queryCategories[category].join("+");
			}
		}

		self.query = fullQuery;
	}


	this.addKeyword = function(queryParam){
		for (var category in queryParam){
			self.queryCategories[category].push(queryParam[category]);
		}
	}

	this.removeKeyword = function(queryParam){
		for (var category in queryParam){
		  for (var i = 0; i < self.queryCategories[category].length; i++){
		    if (self.queryCategories[category][i] === queryParam[category]){
		      self.queryCategories[category].splice(i, 1);
		    }
		  }
		}
	}

	this.checkQueryParamStatus = function(){
		var category = $(this).attr();
	}

}
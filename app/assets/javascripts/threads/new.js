$(document).ready( function() {
  'use strict';

   Tabilocal.threads.new = (function(){

    var tags, selectedTags, previewParser;
    var $tagsElem, $tagsContainer, $form;
    var MAX_TAGS;
    var autocomplete;

    autocomplete = new Autocomplete({
      containerSelector: "#travel-thread-autocomplete",
      matchingProperty: "name",
      remote: true,
      remoteUrl: "/places",
      onSelect: function(selected){
        this.persistVal(selected.fullname);
        setPlaceId(selected.id);
      },
      html: Utilities.placeSuggestionsHtml,
      noResultsHtml: Utilities.noPlaceHtml
    });

    previewParser = new HtmlParser({
      inputSelector: "textarea#travel_thread_replies_attributes_0_content",
      previewSelector: ".preview-container",
      parseEl: Tabilocal.htmlParserFullOptions
    });

    MAX_TAGS = 3;
    tags = [];
    selectedTags = [];

    $tagsContainer = $("#tags-container");
    $tagsElem = $("span.tag");
    $form = $("form#new_travel_thread");

    $tagsElem.each(function(){
      var tag = new Tag({
        el: $(this),
        enableCallback: function(){
          if (selectedTagCount() < MAX_TAGS){
            this.$el.addClass("active");
            addTagToForm(this.$el.attr("tag-id"));
            renderInForm();
            return true;
          }
          return false;
        },
        disableCallback: function(){
          if (selectedTagCount() > 0){
            this.$el.removeClass("active");
            removeTagFromForm(this.$el.attr("tag-id"));
            renderInForm();
            return true;
          }
          return false;
        }
      });
      tags.push(tag);
    });

    function selectedTagCount(){
      return selectedTags.length;
    }

    function addTagToForm(tagId){
      selectedTags.push(tagId);
    }

    function removeTagFromForm(tagId){
      var index;
      index = selectedTags.indexOf(tagId);
      if (index > -1){
          selectedTags.splice(index, 1);
      }
    }

    function renderInForm(){
      var hiddenField;
      var length = selectedTagCount();
      $tagsContainer.html("");
      for ( var i = 0; i < length; i++ ){
        hiddenField = "<input value='" + selectedTags[i] + "' id='tag-input-" + selectedTags[i] + "' type='hidden' name='travel_thread[taggings_attributes][" + i + "][tag_id]' />";
        $tagsContainer.append(hiddenField);
      }
    }

    function setPlaceId(id){
      $form.find("#travel_thread_place_id").val(id);
    }

  })();

});
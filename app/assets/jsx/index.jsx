import React from 'react';
import { render } from 'react-dom';

import EventForm from './events/events_new.jsx';
import Demo from './cropper.jsx';

import $ from 'jquery';

$.ajaxSetup({
	headers: {
		'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	}
})

if (window.location.pathname.indexOf('events') > -1){
	render (<EventForm />, document.getElementById('tabi-react'));	
}

if (window.location.pathname.indexOf('react_test') > -1){
	render (<AutocompleteTest />, document.getElementById('tabi-react'));	
}

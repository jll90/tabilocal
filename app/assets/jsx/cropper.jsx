import React from 'react';
import Cropper from 'react-cropper';

// import of css file is handled by Rails asset pipeline
// import '../../../node_modules/cropperjs/dist/cropper.css';

class Demo extends React.Component{
  _crop(){
    // image in dataUrl
    // console.log(this.refs.cropper.getCroppedCanvas().toDataURL());
  }

  render() {
    return <Cropper
        ref='cropper'
        src='http://fengyuanchen.github.io/cropper/img/picture.jpg'
        style={{height: 400, width: '100%'}}
        // Cropper.js options
        aspectRatio={16 / 9}
        guides={false}
        crop={this._crop.bind(this)} />
  }
};

export default Demo;
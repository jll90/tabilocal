function matchDataInObj(data, propertyName, query) {
  const escapedValue = escapeRegexCharacters(query.trim());
 
  if (escapedValue === '') {
    return [];
  }
  
  const regex = new RegExp(escapedValue, 'i');

  return data.filter(obj => regex.test(obj[propertyName]));
}

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
// function to escape characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export default matchDataInObj;
import React from 'react';
import { render } from 'react-dom';
import Datepicker from 'react-datepicker';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import $ from 'jquery';
import Autosuggest from 'react-autosuggest';

//imports datepicker css from rails asset pipeline
//react-datepicker.css was copied
//imports timepicker css from rails asset pipeline
//index.css inside npm corresponding folder was copied

//value of atc when suggestion is selected
function getSuggestionValue(suggestion) {
  return suggestion.fullname;
}

//render suggestions for both places and users
function renderSuggestion(suggestion) {
  return (
    <div>{suggestion.fullname}</div>
  );
}

const showSecond = false;
const str = showSecond ? 'HH:mm:ss' : 'HH:mm';
const hideInputFileStyles = {
	opacity: 1,
	width: 0,
	height: 0,
};
const defaultBkgImgUrl = 'https://s-media-cache-ak0.pinimg.com/originals/e1/e6/0f/e1e60ff3fbf889e3db19eac7f1981c39.jpg';

class EventForm extends React.Component {

	constructor(){
		super();
		this.state = {
			'title': '',
			'eventImgSrc': defaultBkgImgUrl,
			'coorganizers': [],
			'invitees': [],
			'coorganizersCandidates': [],
			'inviteesCandidates': [],
			'placesCandidates': [],
			'coorganizersAtcVal': '',
			'inviteesAtcVal': '',
			'placeAtcVal': '',
			'description': ''
		}
		this.buildBkgImgStyle = this.buildBkgImgStyle.bind(this);
		this.removeCandidate = this.removeCandidate.bind(this);
		this.addCandidate = this.addCandidate.bind(this);
	}

	componentDidMount(){
		var self = this;
		$.ajax({
  			url: '/event_coorganizers',
  			method: 'GET',
  			dataType: 'json',
  			success: function (data){
  				self.setState({
  					'coorganizers': data,
  					'invitees': data,
  					'coorganizersCandidates': data,
  					'inviteesCandidates': data
  				});
  			},
  			error: function () {
  				//Pass to errorHandler
  			}
		});
	}

	buildBkgImgStyle(imgUrl) {
		var bkgFormatStr = ['#59B2A2', 'url(', imgUrl, ')', 'no-repeat', 'center', 'center'].join(' ');
		return {
			background: bkgFormatStr
		}
	}

	handleFileSelection = (e) => {
		var file = e.target.files[0];
		//add format validation
		if (file){
			var imgAsObjUrl = URL.createObjectURL(file);
			this.setState({eventImgSrc: imgAsObjUrl});
		} else { 
			alert("Por favor, selecciona un archivo que válido");
		}
	}

	handleTypingChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	//used for the autocomplete
	//has a different signature
	//event target.name comes empty

	handleCoorganizersAtcTypingChange = (e, {newValue}) => {
		this.setState({'coorganizersAtcVal': newValue});
	}

	handleInviteesAtcTypingChange = (e, {newValue}) => {
		this.setState({'inviteesAtcVal': newValue});
	}

	handlePlaceAtcTypingChange = (e, {newValue}) => {
		this.setState({'placeAtcVal': newValue});
	}

	//used for places autocomplete
	fetchPlacesData = ({ value }) => {
		var self = this;
		if (value.length > 2){
			$.ajax({
				url: '/places?q=' + value,
	  			method: 'GET',
	  			dataType: 'json',
	  			success: function (data) {
	  				self.setState({placesCandidates: data});
	  			}
			});
		} else {
			this.setState({placesCandidates: []});
		}
	};

	onSuggestionsClearRequested = () => {
		console.log("clear")
		//this.setState({suggestions: []});
	};

	onCandidateSelected = (e, obj) => {		
		this.removeCandidate(obj.suggestion);
	}

	onPlaceSelected = (e, obj) => {
		this.setState({place_id: obj.suggestion.id});
	}

	addCandidate(candidate) {
		//implementation pending
	}

	removeCandidate(candidate) {

	}

	render () {
		const coorganizersCandidates = this.state.coorganizersCandidates;
		const inviteesCandidates = this.state.inviteesCandidates;
		const placesCandidates = this.state.placesCandidates;
		return (
			<div className="event-container-new">
		        <EventFormHeader 
		        	handleFileSelection={this.handleFileSelection} 
		        	bkgImgStyle={this.buildBkgImgStyle(this.state.eventImgSrc)}
		        	handleTypingChange={this.handleTypingChange}
		        	title={this.state.title}
		        />
		        <EventFormContent 
		        	handleTypingChange={this.handleTypingChange}
		        	description={this.state.description}

		        	coorganizersCandidates={coorganizersCandidates}
		        	handleCoorganizersAtcTypingChange={this.handleCoorganizersAtcTypingChange}
		        	coorganizersAtcVal={this.state.coorganizersAtcVal}

		        	inviteesCandidates={inviteesCandidates}
		        	handleInviteesAtcTypingChange={this.handleInviteesAtcTypingChange}
		        	inviteesAtcVal={this.state.inviteesAtcVal}

		        	placesCandidates={placesCandidates}
		        	handlePlaceAtcTypingChange={this.handlePlaceAtcTypingChange}
		        	placeAtcVal={this.state.placeAtcVal}
		        	fetchPlacesData={this.fetchPlacesData} //handles async call

		        	onCandidateSelected={this.onCandidateSelected}
		        	onPlaceSelected={this.onPlaceSelected}
		        	onSuggestionsClearRequested={this.onSuggestionsClearRequested}
		        />
	      	</div>
	    );
	}
};

class EventFormHeader extends React.Component {
	render () {
		return (
			<div className="event-header-new" style={this.props.bkgImgStyle}>
				<input className="input-title" placeholder="Título del evento" name="title" onChange={this.props.handleTypingChange} value={this.props.title}/>
				<div className="upload-eventophoto-button">
					<label htmlFor="image">
						<i className="fa fa-camera"></i>
					</label>
					<input style={hideInputFileStyles} type="file" name="image" id="image" onChange={this.props.handleFileSelection}/>
				</div>
		    </div>
		);
	}
};

class EventFormContent extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			start_date: moment(),
			start_time: '',
			end_date: moment(),
			end_time: '',
		};
		this.handleStartDateChange = this.handleStartDateChange.bind(this);
		this.handleEndDateChange = this.handleEndDateChange.bind(this);
		this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
		this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
	}

	componentDidMount() {
		var self = this;
		$.ajax({
			url: '/event_coorganizers',
			method: 'GET',
			dataType: 'json',
			success: function (data){
				self.setState({coorganizers: data});
			},
			error: function () {

			}
		});
	}

	handleStartDateChange(date) {
		this.setState({start_date: date});
	}

	handleEndDateChange(date) {
		this.setState({end_date: date});
	}

	handleStartTimeChange(time) {
		var time = time && time.format(str)
		this.setState({start_time: time});
	}

	handleEndTimeChange(time) {
		var time = time && time.format(str)
		this.setState({end_time: time});	
	}

	render() {
		const coorganizersSuggestions = this.props.coorganizersCandidates;
		const inviteesSuggestions = this.props.inviteesCandidates;
		const placesSuggestions = this.props.placesCandidates;
		const coorganizersAtcProps = {
			value: this.props.coorganizersAtcVal,
			onChange: this.props.handleCoorganizersAtcTypingChange,
			placeholder: 'Organiza este evento con tus amigos'
		};
		const inviteesAtcProps = {
			value: this.props.inviteesAtcVal,
			onChange: this.props.handleInviteesAtcTypingChange,
			placeholder: 'Invita a tus amigos'
		};
		const placeAtcProps = {
			value: this.props.placeAtcVal,
			onChange: this.props.handlePlaceAtcTypingChange,
			placeholder: 'Selecciona un lugar para el evento'
		};
		return ( 
			<div className="event-content">
	          <div className="element-wrapper">
	            <div className="event-element">Ciudad</div>
	            <Autosuggest
	            id="react-autosuggest__place" 
		          suggestions={placesSuggestions}
		          onSuggestionSelected={this.props.onPlaceSelected}
		          onSuggestionsFetchRequested={this.props.fetchPlacesData}
		          onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
		          getSuggestionValue={getSuggestionValue}
		          renderSuggestion={renderSuggestion}
		          inputProps={placeAtcProps} 
		        />
	          </div>
	          <div className="element-wrapper">
	            <div className="event-element">Dirección</div>
	            <input className="input-venue" />
	          </div>
	          <div className="element-wrapper">
	            <div className="event-element">Co-organizadores</div>
	            <Autosuggest
	            id="react-autosuggest__coorganizers" 
		          suggestions={coorganizersSuggestions}
		          onSuggestionSelected={this.props.onCandidateSelected}
		          onSuggestionsFetchRequested={()=> {}}
		          onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
		          getSuggestionValue={getSuggestionValue}
		          renderSuggestion={renderSuggestion}
		          inputProps={coorganizersAtcProps} 
		        />
	          </div>
	          <div className="element-wrapper date">
	            <div className="event-element">Fecha de comienzo</div>
	            <Datepicker selected={this.state.start_date} onChange={this.handleStartDateChange} />
	          </div>
	          <div className="element-wrapper date">
	            <div className="event-element">Hora de comienzo</div>
	            <TimePicker style={{width: 100}} showSecond={showSecond} defaultValue={moment()} className="input-date" onChange={this.handleStartTimeChange} />
	          </div>
	          <div className="element-wrapper date">
	          	<div className="event-element">Fecha de termino</div>
	            <Datepicker selected={this.state.end_date} onChange={this.handleEndDateChange} />
	          </div>
	          <div className="element-wrapper date">
	            <div className="event-element">Hora de fin</div>
	            <TimePicker style={{width: 100}} showSecond={showSecond} defaultValue={moment()} className="input-date" onChange={this.handleEndTimeChange} />
	          </div>
	          <div className="element-wrapper">
	            <div className="event-element">Descripción</div>
	            <div className="editor-toolbar">
	              <span>
	                <i className="fa fa-bold bold-brackets"></i>
	              </span>
	              <span>
	                <i className="fa fa-italic italic-brackets"></i>
	              </span>
	              <span>
	                <i className="fa fa-quote-left quote-brackets"></i>
	              </span>
	              <span>
	                <i className="fa fa-file-image-o img-brackets"></i>
	              </span>
	            </div>
	            <input className="input-description" name="description" onChange={this.props.handleTypingChange} value={this.props.description}/>
	            <div className="preview-container"></div>
	          </div>
	          <div className="element-wrapper">
	            <div className="event-element">Invitación</div>
	            <Autosuggest 
	            id="react-autosuggest__invitees"
		          suggestions={inviteesSuggestions}
		          onSuggestionSelected={this.props.onCandidateSelected}
		          onSuggestionsFetchRequested={()=>{}}
		          onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
		          getSuggestionValue={getSuggestionValue}
		          renderSuggestion={renderSuggestion}
		          inputProps={inviteesAtcProps} 
		        />
	          </div>
	          <div className="post-event-button">Publicar evento</div>
	        </div>
	    );
	}
}

export default EventForm;
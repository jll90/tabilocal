--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_div_ones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE admin_div_ones (
    id integer NOT NULL,
    country character varying,
    code character varying,
    name character varying,
    ascii character varying,
    place_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    concat_key character varying
);


--
-- Name: admin_div_ones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admin_div_ones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_div_ones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admin_div_ones_id_seq OWNED BY admin_div_ones.id;


--
-- Name: conversations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE conversations (
    id integer NOT NULL,
    private boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: conversations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE conversations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: conversations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE conversations_id_seq OWNED BY conversations.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE countries (
    id integer NOT NULL,
    iso_alpha2 character varying,
    iso_alpha3 character varying,
    iso_numeric integer,
    fips_code character varying,
    country character varying,
    capital character varying,
    areainsqkm double precision,
    population integer,
    continent character varying,
    tld character varying,
    currency_code character varying,
    currency_name character varying,
    phone character varying,
    postal character varying,
    postalregex character varying,
    languages character varying,
    place_id integer,
    neighbours character varying,
    equivalent_fips_code character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE countries_id_seq OWNED BY countries.id;


--
-- Name: cover_pictures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cover_pictures (
    id integer NOT NULL,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: cover_pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cover_pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cover_pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cover_pictures_id_seq OWNED BY cover_pictures.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE events (
    id integer NOT NULL,
    place_id integer,
    title character varying,
    address character varying,
    description character varying,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: events_participants; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE events_participants (
    id integer NOT NULL,
    event_id integer,
    participant_id integer,
    rank character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: events_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE events_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE events_participants_id_seq OWNED BY events_participants.id;


--
-- Name: friend_requests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE friend_requests (
    id integer NOT NULL,
    user_id integer,
    friend_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: friend_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE friend_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: friend_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE friend_requests_id_seq OWNED BY friend_requests.id;


--
-- Name: friendships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE friendships (
    id integer NOT NULL,
    user_id integer,
    friend_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: friendships_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE friendships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: friendships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE friendships_id_seq OWNED BY friendships.id;


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE invitations (
    id integer NOT NULL,
    user_id integer,
    content character varying,
    email character varying,
    sender_name character varying,
    recipient_name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: invitations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE invitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE invitations_id_seq OWNED BY invitations.id;


--
-- Name: languages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE languages (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: languages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: languages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE languages_id_seq OWNED BY languages.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE messages (
    id integer NOT NULL,
    conversation_id integer,
    user_id integer,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE messages_id_seq OWNED BY messages.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE notifications (
    id integer NOT NULL,
    notificationable_id integer,
    notificationable_type character varying,
    user_id integer,
    read boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: pictures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE pictures (
    id integer NOT NULL,
    name character varying,
    caption character varying,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pictures_id_seq OWNED BY pictures.id;


--
-- Name: places; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE places (
    id integer NOT NULL,
    name character varying,
    asciiname character varying,
    alternatenames character varying,
    latitude double precision,
    longitude double precision,
    fclass character varying,
    fcode character varying,
    country character varying,
    cc2 character varying,
    admin1 character varying,
    admin2 character varying,
    admin3 character varying,
    admin4 character varying,
    population bigint,
    elevation integer,
    gtopo30 integer,
    timezone character varying,
    moddate date,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    admin_id integer,
    country_id integer,
    concat_key character varying,
    residents integer DEFAULT 0,
    natives integer DEFAULT 0,
    visiting integer DEFAULT 0,
    visited integer DEFAULT 0,
    events integer DEFAULT 0,
    threads integer DEFAULT 0
);


--
-- Name: places_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: places_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE places_id_seq OWNED BY places.id;


--
-- Name: profile_pictures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE profile_pictures (
    id integer NOT NULL,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: profile_pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE profile_pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profile_pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE profile_pictures_id_seq OWNED BY profile_pictures.id;


--
-- Name: redirects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE redirects (
    id integer NOT NULL,
    link character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: redirects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE redirects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: redirects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE redirects_id_seq OWNED BY redirects.id;


--
-- Name: replies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE replies (
    id integer NOT NULL,
    replyable_id integer,
    replyable_type character varying,
    user_id integer,
    content text,
    first boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: replies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE replies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: replies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE replies_id_seq OWNED BY replies.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: stories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stories (
    id integer NOT NULL,
    user_id integer,
    recipient_id integer,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: stories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stories_id_seq OWNED BY stories.id;


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE subscriptions (
    id integer NOT NULL,
    subscriptionable_id integer,
    subscriptionable_type character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subscriptions_id_seq OWNED BY subscriptions.id;


--
-- Name: suggestions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE suggestions (
    id integer NOT NULL,
    name character varying,
    email character varying,
    subject character varying,
    message character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status character varying DEFAULT 'P'::character varying
);


--
-- Name: suggestions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE suggestions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: suggestions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE suggestions_id_seq OWNED BY suggestions.id;


--
-- Name: taggings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE taggings (
    id integer NOT NULL,
    tag_id integer,
    taggable_id integer,
    taggable_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE taggings_id_seq OWNED BY taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying,
    category character varying(1),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: travel_threads; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE travel_threads (
    id integer NOT NULL,
    user_id integer,
    title character varying,
    language_id integer,
    place_id integer,
    replies_count integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: travel_threads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE travel_threads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: travel_threads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE travel_threads_id_seq OWNED BY travel_threads.id;


--
-- Name: user_conversations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_conversations (
    id integer NOT NULL,
    conversation_id integer,
    user_id integer,
    read boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: user_conversations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_conversations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_conversations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_conversations_id_seq OWNED BY user_conversations.id;


--
-- Name: user_languages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_languages (
    id integer NOT NULL,
    user_id integer,
    language_id integer,
    category character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: user_languages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_languages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_languages_id_seq OWNED BY user_languages.id;


--
-- Name: user_places; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_places (
    id integer NOT NULL,
    user_id integer,
    place_id integer,
    category character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: user_places_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_places_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_places_id_seq OWNED BY user_places.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    firstname character varying,
    lastname character varying,
    about_intro text,
    website character varying,
    email character varying,
    password_digest character varying,
    remember_digest character varying,
    activation_digest character varying,
    activated boolean DEFAULT false,
    activated_at timestamp without time zone,
    reset_digest character varying,
    reset_sent_at timestamp without time zone,
    dob date,
    gender character varying(1),
    gender_privacy character varying(1),
    birthday_privacy character varying(1),
    hometown_id integer,
    current_residence_id integer,
    posts_count integer,
    stories_count integer,
    friends_count integer,
    languages_count integer,
    cover_picture_id integer,
    profile_picture_id integer,
    language_preference character varying(4),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status character varying DEFAULT 'T'::character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_div_ones ALTER COLUMN id SET DEFAULT nextval('admin_div_ones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY conversations ALTER COLUMN id SET DEFAULT nextval('conversations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries ALTER COLUMN id SET DEFAULT nextval('countries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cover_pictures ALTER COLUMN id SET DEFAULT nextval('cover_pictures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_participants ALTER COLUMN id SET DEFAULT nextval('events_participants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY friend_requests ALTER COLUMN id SET DEFAULT nextval('friend_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY friendships ALTER COLUMN id SET DEFAULT nextval('friendships_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY invitations ALTER COLUMN id SET DEFAULT nextval('invitations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY languages ALTER COLUMN id SET DEFAULT nextval('languages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages ALTER COLUMN id SET DEFAULT nextval('messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pictures ALTER COLUMN id SET DEFAULT nextval('pictures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY places ALTER COLUMN id SET DEFAULT nextval('places_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY profile_pictures ALTER COLUMN id SET DEFAULT nextval('profile_pictures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY redirects ALTER COLUMN id SET DEFAULT nextval('redirects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY replies ALTER COLUMN id SET DEFAULT nextval('replies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stories ALTER COLUMN id SET DEFAULT nextval('stories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscriptions ALTER COLUMN id SET DEFAULT nextval('subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY suggestions ALTER COLUMN id SET DEFAULT nextval('suggestions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY taggings ALTER COLUMN id SET DEFAULT nextval('taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY travel_threads ALTER COLUMN id SET DEFAULT nextval('travel_threads_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_conversations ALTER COLUMN id SET DEFAULT nextval('user_conversations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_languages ALTER COLUMN id SET DEFAULT nextval('user_languages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_places ALTER COLUMN id SET DEFAULT nextval('user_places_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: admin_div_ones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_div_ones
    ADD CONSTRAINT admin_div_ones_pkey PRIMARY KEY (id);


--
-- Name: conversations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY conversations
    ADD CONSTRAINT conversations_pkey PRIMARY KEY (id);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: cover_pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cover_pictures
    ADD CONSTRAINT cover_pictures_pkey PRIMARY KEY (id);


--
-- Name: events_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_participants
    ADD CONSTRAINT events_participants_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: friend_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY friend_requests
    ADD CONSTRAINT friend_requests_pkey PRIMARY KEY (id);


--
-- Name: friendships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY friendships
    ADD CONSTRAINT friendships_pkey PRIMARY KEY (id);


--
-- Name: invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT invitations_pkey PRIMARY KEY (id);


--
-- Name: languages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- Name: places_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY places
    ADD CONSTRAINT places_pkey PRIMARY KEY (id);


--
-- Name: profile_pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY profile_pictures
    ADD CONSTRAINT profile_pictures_pkey PRIMARY KEY (id);


--
-- Name: redirects_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY redirects
    ADD CONSTRAINT redirects_pkey PRIMARY KEY (id);


--
-- Name: replies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY replies
    ADD CONSTRAINT replies_pkey PRIMARY KEY (id);


--
-- Name: stories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stories
    ADD CONSTRAINT stories_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: suggestions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY suggestions
    ADD CONSTRAINT suggestions_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: travel_threads_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY travel_threads
    ADD CONSTRAINT travel_threads_pkey PRIMARY KEY (id);


--
-- Name: user_conversations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_conversations
    ADD CONSTRAINT user_conversations_pkey PRIMARY KEY (id);


--
-- Name: user_languages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_languages
    ADD CONSTRAINT user_languages_pkey PRIMARY KEY (id);


--
-- Name: user_places_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_places
    ADD CONSTRAINT user_places_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_admin_div_ones_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admin_div_ones_on_place_id ON admin_div_ones USING btree (place_id);


--
-- Name: index_conversations_on_private; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_conversations_on_private ON conversations USING btree (private);


--
-- Name: index_countries_on_iso_alpha2; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_countries_on_iso_alpha2 ON countries USING btree (iso_alpha2);


--
-- Name: index_countries_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_countries_on_place_id ON countries USING btree (place_id);


--
-- Name: index_cover_pictures_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cover_pictures_on_user_id ON cover_pictures USING btree (user_id);


--
-- Name: index_events_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_place_id ON events USING btree (place_id);


--
-- Name: index_events_participants_on_event_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_participants_on_event_id ON events_participants USING btree (event_id);


--
-- Name: index_events_participants_on_event_id_and_participant_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_participants_on_event_id_and_participant_id ON events_participants USING btree (event_id, participant_id);


--
-- Name: index_events_participants_on_event_id_and_rank; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_participants_on_event_id_and_rank ON events_participants USING btree (event_id, rank);


--
-- Name: index_events_participants_on_participant_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_participants_on_participant_id ON events_participants USING btree (participant_id);


--
-- Name: index_events_participants_on_rank; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_participants_on_rank ON events_participants USING btree (rank);


--
-- Name: index_friend_requests_on_friend_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friend_requests_on_friend_id ON friend_requests USING btree (friend_id);


--
-- Name: index_friend_requests_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friend_requests_on_user_id ON friend_requests USING btree (user_id);


--
-- Name: index_friend_requests_on_user_id_and_friend_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_friend_requests_on_user_id_and_friend_id ON friend_requests USING btree (user_id, friend_id);


--
-- Name: index_friendships_on_friend_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendships_on_friend_id ON friendships USING btree (friend_id);


--
-- Name: index_friendships_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendships_on_user_id ON friendships USING btree (user_id);


--
-- Name: index_friendships_on_user_id_and_friend_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_friendships_on_user_id_and_friend_id ON friendships USING btree (user_id, friend_id);


--
-- Name: index_invitations_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invitations_on_user_id ON invitations USING btree (user_id);


--
-- Name: index_messages_on_conversation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_conversation_id ON messages USING btree (conversation_id);


--
-- Name: index_messages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_user_id ON messages USING btree (user_id);


--
-- Name: index_notifications_on_notificationable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_notificationable_id ON notifications USING btree (notificationable_id);


--
-- Name: index_notifications_on_notificationable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_notificationable_type ON notifications USING btree (notificationable_type);


--
-- Name: index_notifications_on_read; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_read ON notifications USING btree (read);


--
-- Name: index_notifications_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_user_id ON notifications USING btree (user_id);


--
-- Name: index_pictures_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_pictures_on_user_id ON pictures USING btree (user_id);


--
-- Name: index_places_on_admin_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_admin_id ON places USING btree (admin_id);


--
-- Name: index_places_on_country_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_country_id ON places USING btree (country_id);


--
-- Name: index_places_on_events; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_events ON places USING btree (events);


--
-- Name: index_places_on_natives; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_natives ON places USING btree (natives);


--
-- Name: index_places_on_residents; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_residents ON places USING btree (residents);


--
-- Name: index_places_on_threads; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_threads ON places USING btree (threads);


--
-- Name: index_places_on_visited; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_visited ON places USING btree (visited);


--
-- Name: index_places_on_visiting; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_places_on_visiting ON places USING btree (visiting);


--
-- Name: index_profile_pictures_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_profile_pictures_on_user_id ON profile_pictures USING btree (user_id);


--
-- Name: index_replies_on_first; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_replies_on_first ON replies USING btree (first);


--
-- Name: index_replies_on_replyable_type_and_replyable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_replies_on_replyable_type_and_replyable_id ON replies USING btree (replyable_type, replyable_id);


--
-- Name: index_replies_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_replies_on_user_id ON replies USING btree (user_id);


--
-- Name: index_stories_on_recipient_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stories_on_recipient_id ON stories USING btree (recipient_id);


--
-- Name: index_stories_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stories_on_user_id ON stories USING btree (user_id);


--
-- Name: index_stories_on_user_id_and_recipient_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_stories_on_user_id_and_recipient_id ON stories USING btree (user_id, recipient_id);


--
-- Name: index_subscriptions_on_subscriptionable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriptions_on_subscriptionable_id ON subscriptions USING btree (subscriptionable_id);


--
-- Name: index_subscriptions_on_subscriptionable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriptions_on_subscriptionable_type ON subscriptions USING btree (subscriptionable_type);


--
-- Name: index_subscriptions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriptions_on_user_id ON subscriptions USING btree (user_id);


--
-- Name: index_taggings_on_tag_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_taggings_on_tag_id ON taggings USING btree (tag_id);


--
-- Name: index_travel_threads_on_language_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_travel_threads_on_language_id ON travel_threads USING btree (language_id);


--
-- Name: index_travel_threads_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_travel_threads_on_place_id ON travel_threads USING btree (place_id);


--
-- Name: index_travel_threads_on_replies_count; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_travel_threads_on_replies_count ON travel_threads USING btree (replies_count);


--
-- Name: index_travel_threads_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_travel_threads_on_user_id ON travel_threads USING btree (user_id);


--
-- Name: index_user_conversations_on_conversation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_conversations_on_conversation_id ON user_conversations USING btree (conversation_id);


--
-- Name: index_user_conversations_on_conversation_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_conversations_on_conversation_id_and_user_id ON user_conversations USING btree (conversation_id, user_id);


--
-- Name: index_user_conversations_on_read; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_conversations_on_read ON user_conversations USING btree (read);


--
-- Name: index_user_conversations_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_conversations_on_user_id ON user_conversations USING btree (user_id);


--
-- Name: index_user_conversations_on_user_id_and_read; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_conversations_on_user_id_and_read ON user_conversations USING btree (user_id, read);


--
-- Name: index_user_languages_on_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_languages_on_category ON user_languages USING btree (category);


--
-- Name: index_user_languages_on_language_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_languages_on_language_id ON user_languages USING btree (language_id);


--
-- Name: index_user_languages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_languages_on_user_id ON user_languages USING btree (user_id);


--
-- Name: index_user_languages_on_user_id_and_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_languages_on_user_id_and_category ON user_languages USING btree (user_id, category);


--
-- Name: index_user_languages_on_user_id_and_language_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_languages_on_user_id_and_language_id ON user_languages USING btree (user_id, language_id);


--
-- Name: index_user_places_on_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_places_on_category ON user_places USING btree (category);


--
-- Name: index_user_places_on_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_places_on_place_id ON user_places USING btree (place_id);


--
-- Name: index_user_places_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_places_on_user_id ON user_places USING btree (user_id);


--
-- Name: index_user_places_on_user_id_and_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_places_on_user_id_and_category ON user_places USING btree (user_id, category);


--
-- Name: index_user_places_on_user_id_and_place_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_places_on_user_id_and_place_id ON user_places USING btree (user_id, place_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: places_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX places_name ON places USING gin (to_tsvector('simple'::regconfig, COALESCE((name)::text, ''::text)));


--
-- Name: places_trigram; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX places_trigram ON places USING gin ((COALESCE((name)::text, ''::text)) gin_trgm_ops);


--
-- Name: places_trigram_alt; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX places_trigram_alt ON places USING gin (name gin_trgm_ops);


--
-- Name: places_trigram_alt_ascii; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX places_trigram_alt_ascii ON places USING gin (asciiname gin_trgm_ops);


--
-- Name: subscriptions_dual_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subscriptions_dual_index ON subscriptions USING btree (subscriptionable_id, subscriptionable_type);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_0094729689; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY friend_requests
    ADD CONSTRAINT fk_rails_0094729689 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_0be39eaff3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_languages
    ADD CONSTRAINT fk_rails_0be39eaff3 FOREIGN KEY (language_id) REFERENCES languages(id);


--
-- Name: fk_rails_256e4b72c5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY replies
    ADD CONSTRAINT fk_rails_256e4b72c5 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_273a25a7a6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT fk_rails_273a25a7a6 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_3268570edc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY pictures
    ADD CONSTRAINT fk_rails_3268570edc FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_6f1279cfbb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_conversations
    ADD CONSTRAINT fk_rails_6f1279cfbb FOREIGN KEY (conversation_id) REFERENCES conversations(id);


--
-- Name: fk_rails_74c85693d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_places
    ADD CONSTRAINT fk_rails_74c85693d2 FOREIGN KEY (place_id) REFERENCES places(id);


--
-- Name: fk_rails_7e96fcbe95; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY travel_threads
    ADD CONSTRAINT fk_rails_7e96fcbe95 FOREIGN KEY (language_id) REFERENCES languages(id);


--
-- Name: fk_rails_7eae413fe6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY invitations
    ADD CONSTRAINT fk_rails_7eae413fe6 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_7f927086d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT fk_rails_7f927086d2 FOREIGN KEY (conversation_id) REFERENCES conversations(id);


--
-- Name: fk_rails_8071a77426; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_places
    ADD CONSTRAINT fk_rails_8071a77426 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_8bc5a13233; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY profile_pictures
    ADD CONSTRAINT fk_rails_8bc5a13233 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_933bdff476; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT fk_rails_933bdff476 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_9fcd2e236b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT fk_rails_9fcd2e236b FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: fk_rails_b080fb4855; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT fk_rails_b080fb4855 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_b2e7252813; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events
    ADD CONSTRAINT fk_rails_b2e7252813 FOREIGN KEY (place_id) REFERENCES places(id);


--
-- Name: fk_rails_baff727535; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_conversations
    ADD CONSTRAINT fk_rails_baff727535 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_beb371e47e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cover_pictures
    ADD CONSTRAINT fk_rails_beb371e47e FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_c09452deae; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY travel_threads
    ADD CONSTRAINT fk_rails_c09452deae FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_c53f5feaac; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stories
    ADD CONSTRAINT fk_rails_c53f5feaac FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_db4f7502c2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_languages
    ADD CONSTRAINT fk_rails_db4f7502c2 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_ddbf4232c8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY events_participants
    ADD CONSTRAINT fk_rails_ddbf4232c8 FOREIGN KEY (event_id) REFERENCES events(id);


--
-- Name: fk_rails_e3733b59b7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY friendships
    ADD CONSTRAINT fk_rails_e3733b59b7 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_e5c48820a7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY travel_threads
    ADD CONSTRAINT fk_rails_e5c48820a7 FOREIGN KEY (place_id) REFERENCES places(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20150609015039');

INSERT INTO schema_migrations (version) VALUES ('20150620010643');

INSERT INTO schema_migrations (version) VALUES ('20150620012438');

INSERT INTO schema_migrations (version) VALUES ('20150620143420');

INSERT INTO schema_migrations (version) VALUES ('20150621143420');

INSERT INTO schema_migrations (version) VALUES ('20150703214938');

INSERT INTO schema_migrations (version) VALUES ('20150703220028');

INSERT INTO schema_migrations (version) VALUES ('20150718131054');

INSERT INTO schema_migrations (version) VALUES ('20150718131514');

INSERT INTO schema_migrations (version) VALUES ('20150718152622');

INSERT INTO schema_migrations (version) VALUES ('20150723002148');

INSERT INTO schema_migrations (version) VALUES ('20150727202113');

INSERT INTO schema_migrations (version) VALUES ('20150811201510');

INSERT INTO schema_migrations (version) VALUES ('20150812011553');

INSERT INTO schema_migrations (version) VALUES ('20150815141601');

INSERT INTO schema_migrations (version) VALUES ('20150817225217');

INSERT INTO schema_migrations (version) VALUES ('20150821002807');

INSERT INTO schema_migrations (version) VALUES ('20151003141456');

INSERT INTO schema_migrations (version) VALUES ('20151108152710');

INSERT INTO schema_migrations (version) VALUES ('20151113160342');

INSERT INTO schema_migrations (version) VALUES ('20151216135215');

INSERT INTO schema_migrations (version) VALUES ('20160113185222');

INSERT INTO schema_migrations (version) VALUES ('20160116155551');

INSERT INTO schema_migrations (version) VALUES ('20160224123303');

INSERT INTO schema_migrations (version) VALUES ('20160401112320');

INSERT INTO schema_migrations (version) VALUES ('20160609015158');

INSERT INTO schema_migrations (version) VALUES ('20160803005045');

INSERT INTO schema_migrations (version) VALUES ('20160805134522');

INSERT INTO schema_migrations (version) VALUES ('20160818140522');

INSERT INTO schema_migrations (version) VALUES ('20161015142731');

INSERT INTO schema_migrations (version) VALUES ('20161022184850');

INSERT INTO schema_migrations (version) VALUES ('20161022190558');


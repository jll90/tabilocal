class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.references :user, index: true, foreign_key: true
      t.string :content
      t.string :email
      t.string :sender_name
      t.string :recipient_name
      t.timestamps null: false
    end
  end
end

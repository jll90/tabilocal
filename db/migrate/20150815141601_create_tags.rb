class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.string :category, limit: 1
      t.timestamps null: true
    end
  end
end

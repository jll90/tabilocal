class CreateUserPlaces < ActiveRecord::Migration
  def change
    create_table :user_places do |t|
      t.references :user, index: true, foreign_key: true
      t.references :place, index: true, foreign_key: true
      t.string :category, index: true

      t.timestamps null: false
    end

    add_index :user_places, [:user_id, :place_id]
    add_index :user_places, [:user_id, :category]
  end
end
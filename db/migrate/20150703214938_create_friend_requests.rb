class CreateFriendRequests < ActiveRecord::Migration
  def change
    create_table :friend_requests do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :friend_id, index: true, foreign_key: true, references: :users, to_column: :user_id

      t.timestamps null: false
    end

    add_index :friend_requests, [:user_id, :friend_id], unique: true
  end
end

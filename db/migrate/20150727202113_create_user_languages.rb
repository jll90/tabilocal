class CreateUserLanguages < ActiveRecord::Migration
  def change
    create_table :user_languages do |t|
    	t.references :user, index: true, foreign_key: true
    	t.references :language, index: true, foreign_key: true
    	t.string :category, index: true
    	t.timestamps null: false
    end
    
    add_index :user_languages, [:user_id, :language_id]
    add_index :user_languages, [:user_id, :category]
  end
end

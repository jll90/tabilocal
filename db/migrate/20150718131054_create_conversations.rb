class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.boolean :private, index: true
      t.timestamps null: false
    end
  end
end

class UserStatus < ActiveRecord::Migration
  def change
  	add_column :users, :status, :string, default: "T"
  end
end

class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :subscriptionable, polymorphic: true
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end

    #returned index name too long error if 
    # t.references :subscriptionable, polymorphic: true, index: true
    add_index :subscriptions, :subscriptionable_id
    add_index :subscriptions, :subscriptionable_type
    add_index :subscriptions, [:subscriptionable_id, :subscriptionable_type], name: "subscriptions_dual_index"
  end
end

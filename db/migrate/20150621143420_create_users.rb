class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.text :about_intro
      t.string :website
      t.string :email, index: true, unique: true
      t.string :password_digest
      t.string :remember_digest
      t.string :activation_digest
      t.boolean :activated, default: false
      t.datetime :activated_at
      t.string :reset_digest
      t.datetime :reset_sent_at
      t.date :dob
      t.string :gender, :limit => 1

      t.string :gender_privacy, :limit => 1
      t.string :birthday_privacy, :limit => 1

      t.integer :hometown_id, foreign_key: true, references: :places, to_column: :id
      t.integer :current_residence_id, foreign_key: true, references: :places, to_column: :id

      t.integer :posts_count
      t.integer :stories_count
      t.integer :friends_count
      t.integer :languages_count

      t.integer :cover_picture_id
      t.integer :profile_picture_id

      #just to be on the safe side
      t.string :language_preference, :limit => 4
  
      t.timestamps null: false
    end
  end
end

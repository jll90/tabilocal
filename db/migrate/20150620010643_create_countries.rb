class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :iso_alpha2, index: true
      t.string :iso_alpha3
      t.integer :iso_numeric
      t.string :fips_code
      t.string :country
      t.string :capital
      t.float :areainsqkm
      t.integer :population
      t.string :continent
      t.string :tld
      t.string :currency_code
      t.string :currency_name
      t.string :phone
      t.string :postal
      t.string :postalregex
      t.string :languages
      #not set as foreign key so places can be deleted to
      #filter only P
      t.integer :place_id, index: true
      t.string :neighbours
      t.string :equivalent_fips_code
      t.timestamps null: true
    end
  end
end

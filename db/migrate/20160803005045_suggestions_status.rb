class SuggestionsStatus < ActiveRecord::Migration
  def change
  	add_column :suggestions, :status, :string, index: true, default: "P"
  end
end

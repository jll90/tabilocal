class IndecesPlaces < ActiveRecord::Migration
  def up
    execute "create index places_name on places using gin (to_tsvector('simple', coalesce(\"places\".\"name\"::text, '')))"
    #pg_search
  	execute "create index places_trigram on places using gin ( (coalesce(\"places\".\"name\"::text, '')) gin_trgm_ops)"
  	#no gem alternative
  	execute "CREATE INDEX places_trigram_alt on places USING gin(name gin_trgm_ops)"
  end

  def down
    execute "drop index places_name"
    execute "drop index places_trigram"
  	execute "drop index places_trigram_alt"
  end
end

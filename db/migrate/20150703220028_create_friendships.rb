class CreateFriendships < ActiveRecord::Migration
  def change
    create_table :friendships do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :friend_id, index: true, foreign_key: true, references: :users, to_column: :user_id

      t.timestamps null: false
    end

    add_index :friendships, [:user_id, :friend_id], unique: true
  end
end

class CreateAdminDivOnes < ActiveRecord::Migration
  def change
    create_table :admin_div_ones do |t|
      t.string :country
      t.string :code
      t.string :name
      t.string :ascii
      #must be a foreign key
      #see countries
      t.integer :place_id, index: true
      t.timestamps null: true

      t.string :concat_key
    end
  end
end

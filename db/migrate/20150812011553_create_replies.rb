class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.references :replyable, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: true
      t.text :content
      t.boolean :first, index: true
      t.timestamps null: false
    end
  end
end

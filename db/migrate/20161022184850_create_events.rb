class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :place, index: true, foreign_key: true
      t.string :title
      t.string :address
      t.string :description
      t.timestamp :start_time
      t.timestamp :end_time

      t.timestamps null: false
    end
  end
end

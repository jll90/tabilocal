class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.string :asciiname
      t.string :alternatenames
      t.float :latitude
      t.float :longitude
      t.string :fclass
      t.string :fcode
      t.string :country
      t.string :cc2
      t.string :admin1
      t.string :admin2
      t.string :admin3
      t.string :admin4
      t.bigint :population
      t.integer :elevation
      t.integer :gtopo30
      t.string :timezone
      t.date :moddate
      t.timestamps null: true

      #tabilocal's own to use foreign keys
      t.integer :admin_id, index: true
      t.integer :country_id, index: true
      t.string :concat_key

      t.integer :residents, index: true, default: 0
      t.integer :natives, index: true, default: 0
      t.integer :visiting, index: true, default: 0
      t.integer :visited, index: true, default: 0
      t.integer :events, index: true, default: 0
      t.integer :threads, index: true, default: 0
    end
  end
end

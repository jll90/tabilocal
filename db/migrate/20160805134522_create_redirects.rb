class CreateRedirects < ActiveRecord::Migration
  def change
    create_table :redirects do |t|
      t.string :link
      t.timestamps null: false
    end
  end
end

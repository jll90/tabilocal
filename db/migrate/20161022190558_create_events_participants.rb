class CreateEventsParticipants < ActiveRecord::Migration 
  def change
    create_table :events_participants do |t|
      t.references :event, index: true, foreign_key: true
      t.integer :participant_id, index: true, foreign_key: true, references: :users, to_column: :user_id
      t.string :rank, index: true
      t.timestamps null: false
    end

    add_index :events_participants, [:event_id, :participant_id]
    add_index :events_participants, [:event_id, :rank]
  end
end

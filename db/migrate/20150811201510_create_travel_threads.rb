class CreateTravelThreads < ActiveRecord::Migration
  def change
    create_table :travel_threads do |t|
      t.references :user, index: true, foreign_key: true
      t.string :title
      t.references :language, index: true, foreign_key: true
      t.references :place, index: true, foreign_key: true
      t.integer :replies_count, index: true
      t.timestamps null: false
    end
  end
end

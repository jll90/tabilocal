class CreateCoverPictures < ActiveRecord::Migration
  def change
    create_table :cover_pictures do |t|
      t.attachment :image
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end

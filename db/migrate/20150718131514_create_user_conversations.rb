class CreateUserConversations < ActiveRecord::Migration
  def change
    create_table :user_conversations do |t|
      t.references :conversation, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.boolean :read, index: true, default: false
      t.timestamps null: false
    end

    add_index :user_conversations, [:conversation_id, :user_id]
    add_index :user_conversations, [:user_id, :read]
  end
end

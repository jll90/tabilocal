class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :recipient_id, index: true, foreign_key: true, references: :users, to_column: :user_id
      t.text :content
      t.timestamps null: false
    end

    add_index :stories, [:user_id, :recipient_id], unique: true
  end
end

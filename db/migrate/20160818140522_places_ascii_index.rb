class PlacesAsciiIndex < ActiveRecord::Migration
  def up
  	execute "CREATE INDEX places_trigram_alt_ascii on places USING gin(asciiname gin_trgm_ops)"
  end

  def down
  	execute "drop index places_trigram_alt_ascii"
  end
end

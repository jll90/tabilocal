class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :notificationable, polymorphic: true
      t.references :user, index: true, foreign_key: true
      t.boolean :read, index: true
      t.timestamps null: false
    end

    add_index :notifications, :notificationable_id
    add_index :notifications, :notificationable_type
  end
end

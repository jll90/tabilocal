Rails.application.routes.draw do



  scope "(:locale)", :locale => /#{I18n.available_locales.join("|")}/ do

    root 'static_pages#home_offline'
    
    get 'places/create'
    get 'places/index'
    get 'friends/index'
    get 'friends/destroy'
    get 'sessions/new'
    
    get 'terms' => 'static_pages#terms'
    get 'signup' => 'users#new'
    get 'objectives' => 'static_pages#objectives'
    
    get 'login' => 'static_pages#home_offline'
    post 'login' => 'sessions#create'
    
    delete 'logout' => 'sessions#destroy'
    get 'homepage' => 'static_pages#home_offline'
    get 'dashboard' => 'static_pages#dashboard'
    get 'settings' => 'static_pages#settings'
    get 'terms_of_use' => 'static_pages#terms_of_use'
    get 'contact_us' => 'static_pages#contact_us'
    get 'faq' => 'static_pages#faq'

    get 'safety' => 'static_pages#safety'
    get 'place/:place_id', :to => 'places#show'

    get 'unauthorized_access' => 'static_pages#home_offline'
    get 'not_found' => 'static_pages#home_offline'

    get 'react_test' => 'static_pages#react_test'

    get 'event_coorganizers' => 'data#event_coorganizers'

    # get 'photos' => 'photos#index'
    
    resources :users do
      resources :cover_pictures
      resources :friendships, only: [:index, :destroy]  
      resources :photos, only: [:index]    
      resources :pictures
      resources :profile_pictures
      resources :stories, only: [:index, :destroy]
      resources :replies
      resources :subscriptions
    end

    resources :notifications, only: [:index, :show, :create, :destroy]
    #updates all param
    resources :notifications do
        collection do
            put 'update_all'
            #put so is not caught in the controller correct_user method's logic
            put 'destroy_all'
        end
    end
    resources :account_activations, only: [:edit]
    resources :password_resets, only: [:new, :create, :edit, :update]
    resources :password_updates, only: [:create]
    resources :email_updates, only: [:create]
    resources :places
    resources :friend_requests
    resources :travel_threads, except: [:edit, :update, :destroy] 
    resources :conversations
    resources :messages, only: [:create]
    resources :user_languages
    resources :user_places
    resources :suggestions
    resources :stories, only: [:new, :create, :edit, :update, :destroy]
    resources :invitations, only: [:create]
    resources :stories, only: [:new]
    resources :redirects, only: [:create, :show]
    resources :events, only: [:create, :new, :edit, :update, :index, :destroy, :show]
  end

  

end
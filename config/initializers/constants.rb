BANNERS = [
	{
		name: "Lovilmi",
		owner: "Vilma Mellado",
		site_url: "http://www.lovilmi.com",
		image_url: "https://s3-us-west-2.amazonaws.com/basic-tabi/banners/VilmaMellado_Lovilmi.png"
	},
	{
		name: "Hanabi",
		owner: "Akira Uchimura",
		site_url: "http://www.hana.bi",
		image_url: "https://s3-us-west-2.amazonaws.com/basic-tabi/banners/AkiraUchimura_Hanabi.jpg"
	},
	{
		name: "Ni Hao Cassandra",
		owner: "Cassandra Armijo",
		site_url: "http://www.nihaocassandra.com",
		image_url: "https://s3-us-west-2.amazonaws.com/basic-tabi/banners/CassandraArmijo_NihaoCassandra.png"
	},
	{
		name: "EL Blog de Sam",
		owner: "Samuel Ponce",
		site_url: "http://www.elblogdesam.com",
		image_url: "https://s3-us-west-2.amazonaws.com/basic-tabi/banners/SamuelPonce_ElBlogdeSam.gif"
	},
	{
		name: "Japonismo",
		owner: "Laura Tomás Avellana",
		site_url: "http://japonismo.com",
		image_url: "https://s3-us-west-2.amazonaws.com/basic-tabi/banners/LauraTomasAvellana_Japonismo.jpg"
	}
]
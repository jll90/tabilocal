require 'test_helper'

class SubscriptionsControllerTest < ActionController::TestCase
  
  def setup
  	#lana speaks spanish and has no subscription to this thread
  	@user = users(:lana)
  	@travel_thread = travel_threads(:travel_thread_spanish)
  	@incorrect_travel_thread = travel_threads(:travel_thread_english)
  	@subscribed_user = users(:archer)
  	@archer_subscription	 = subscriptions(:archer_travel_thread_spanish)
  end

  def create_new_subscription(user, travel_thread)
  	xhr :post, :create, user_id: user.id ,subscription: {
  		subscriptionable_type: travel_thread.class.name,
  		subscriptionable_id: travel_thread.id
  	}
  end

  def destroy_existing_subscription(user, subscription)
  	xhr :delete, :destroy, user_id: user.id, id: subscription.id
  end

  test "must be logged in to subscribe" do
  	create_new_subscription(@user, @travel_thread)
  	assert_redirected_to login_url
  	log_in_as @user
	create_new_subscription(@user, @travel_thread)
  	assert_response :success
  end

  test "cannot resubscribe if subscribed" do
  	log_in_as @user
  	assert_difference "Subscription.count", 1 do
  		create_new_subscription(@user, @travel_thread)
  	end
  	assert_response :success
  	assert_no_difference "Subscription.count" do
  		create_new_subscription(@user, @travel_thread)
  	end
  	assert_response :success
  end

  test "must be logged in to unsubscribe" do
  	destroy_existing_subscription(@subscribed_user, @archer_subscription)
  	assert_redirected_to login_url
  	log_in_as @subscribed_user
	destroy_existing_subscription(@subscribed_user, @archer_subscription)
	assert_response :success
  end

  test "must speak correct language when subscribing to a thread" do
  	log_in_as @user
  	create_new_subscription(@user, @incorrect_travel_thread)
  	assert_redirected_to root_url
  end

  test "cannot destroy another user's subscription" do
  	log_in_as @user
  	destroy_existing_subscription(@user, @archer_subscription)
  	assert_redirected_to unauthorized_access_url
  end

end
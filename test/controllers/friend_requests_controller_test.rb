require 'test_helper'

class FriendRequestsControllerTest < ActionController::TestCase
 def setup
    @user = users(:michael)
    @other_user = users(:archer)
  	@friend_request = friend_requests(:john_michael)
    @other_friend_request = friend_requests(:archer_lana)
    @request.env['HTTP_REFERER'] = 'http://foo.com'   
  end
  
  #no friend request or friendship between michael and archer
  test "should redirect create if not logged in" do  	
  	xhr :post, :create, friend_request: {friend_id: @other_user.id}
  	assert_redirected_to login_url
    log_in_as @user
    xhr :post, :create, friend_request: {friend_id: @other_user.id}
    assert_response :success
  end

  test "should redirect destroy if not logged in" do
  	xhr :delete, :destroy, id: @friend_request.id
  	assert_redirected_to login_url
    log_in_as @user
    xhr :delete, :destroy, id: @friend_request.id
    assert_response :success
  end

  test "should redirect update if not logged in" do
    xhr :patch, :update, id: @friend_request.id
    assert_redirected_to login_url
    log_in_as @user
    xhr :patch, :update, id: @friend_request.id
    assert_response :success
  end

  test "should redirect destroy for wrong friend_request" do
  	log_in_as @user
  	xhr :delete, :destroy, id: @other_friend_request.id
  	assert_redirected_to unauthorized_access_url
  end

  test "should redirect update for wrong friend_request" do
  	log_in_as @user
  	xhr :patch, :update, id: @other_friend_request.id
  	assert_redirected_to unauthorized_access_url
  end


end
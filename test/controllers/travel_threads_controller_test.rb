require 'test_helper'

class TravelThreadsControllerTest < ActionController::TestCase
  def setup
    @user = users(:michael)
    @no_language_user = users(:john)
    @existing_travel_thread = travel_threads(:travel_thread_english)
    @wrong_language_thread = travel_threads(:travel_thread_spanish)
  end

  test "must log in to index threads" do
    get :index
    assert_redirected_to login_url
    log_in_as @user
    get :index
    assert_response :success
  end

  test "must log in to see a thread" do
    get :show, id: @existing_travel_thread.id
    assert_redirected_to login_url
    log_in_as @user
    get :show, id: @existing_travel_thread.id
    assert_response :success
  end

  test "show redirected if speaks the wrong language" do
    log_in_as @user
    get :show, id: @wrong_language_thread.id
    assert_redirected_to @user
  end

  test "index redirected if speaks no languages" do
    log_in_as @no_language_user
    get :index
    assert_redirected_to @no_language_user
  end

  test "must log in to access new" do 
    get :new
    assert_redirected_to login_url
    log_in_as @user
    get :new
    assert_response :success
  end

  test "must be able to create a thread" do
    get :new
    post :create, travel_thread: @new_travel_thread
    assert_redirected_to login_url  
    log_in_as @user
    get :new
    post :create, travel_thread: {
        user_id: @user.id,
        title: "standard title",
        language_id: @user.languages.first.id,
        place_id: places(:kyoto).id,
        replies_attributes:
          [{
            user_id: @user.id,
            content: "Standardized content",
            first: true
          }],
        taggings_attributes:
          [
            {tag_id: tags(:safety).id },
            {tag_id: tags(:transportation).id   }
          ],
        subscriptions_attributes: 
          [
            {user_id: @user.id}
          ]
        }
    @thread_id = TravelThread.last.id
    assert_equal "window.location='/en/travel_threads/#{@thread_id}'", response.body 
  
  end
end
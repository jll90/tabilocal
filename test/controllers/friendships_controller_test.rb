require 'test_helper'

class FriendshipsControllerTest < ActionController::TestCase
	def setup
		@user = users(:michael)
		@friendship = friendships(:michael_mallory)
		@incorrect_friendship = friendships(:archer_mallory)
		@request.env['HTTP_REFERER'] = 'http://foo.com'
	end

	test "must be logged in to index" do 
		get :index, user_id: @user.id
		assert_redirected_to login_url
		log_in_as @user
		get :index, user_id: @user.id
		assert_response :success
	end

	test "must be logged in to destroy" do
		xhr :delete, :destroy, id: @friendship.id, user_id: @user.id
		assert_redirected_to login_url
		log_in_as @user
		xhr :delete, :destroy, id: @friendship.id, user_id: @user.id
		assert_response :success
	end

	test "cannot destroy incorrect friendship" do
		log_in_as @user
		xhr :delete, :destroy, id: @incorrect_friendship.id, user_id: @user.id
		assert_redirected_to unauthorized_access_url
	end
end

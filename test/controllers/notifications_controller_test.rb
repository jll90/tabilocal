require 'test_helper'

class NotificationsControllerTest < ActionController::TestCase
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @notification = notifications(:michael_notification_reply_two_english)
    @notifications = @user.notifications
    @wrong_notification = @other_user.notifications.first
    @reply = @notification.notificationable
    @travel_thread = @notification.notificationable.replyable
    #reply
  end

  test "should redirect show if not logged in" do
  	get :show, id: @notification.id
  	assert_redirected_to login_url
  	log_in_as @user
  	get :show, id: @notification.id
    anchor = "reply-#{@reply.id}"
  	assert_redirected_to travel_thread_path(id: @travel_thread.id, anchor: anchor)
  end

  test "should redirect show if incorrect_user" do
  	log_in_as @user
  	xhr :get, :show, id: @wrong_notification.id
  	assert_redirected_to unauthorized_access_url
  end

  test "notification redirects to correct item person" do
  	log_in_as @user
  	@notifications.each do |notification|
	  	get :show, id: notification.id
	  	if notification.notificationable_type == "Reply"
        reply = notification.notificationable
        anchor = "reply-#{reply.id}"
	  		replyable = notification.notificationable.replyable
	  		assert_redirected_to travel_thread_path(replyable.id, anchor: anchor)
	    end
	    if notification.notificationable_type == "Story"
	    	assert_redirected_to user_stories_path(user_id: @user.id)
	    end
	    if notification.notificationable_type == "Friendship"
	    	friendship = notification.notificationable
	    	assert_redirected_to user_path(id: friendship.friend.id)
	    end
	 end
  end

  test "should redirect destroy if incorrect_user" do
    log_in_as @user
    xhr :delete, :destroy, id: @wrong_notification.id
    assert_redirected_to unauthorized_access_url
    xhr :delete, :destroy, id: @notification.id
    assert_response :success
  end
end
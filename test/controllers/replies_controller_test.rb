require 'test_helper'

class RepliesControllerTest < ActionController::TestCase

  def setup
  	@user = users(:michael)
  	@thread = travel_threads(:travel_thread_english)
  	@incorrect_thread = travel_threads(:travel_thread_spanish)
    @reply = replies(:reply_one_travel_thread_english)
    @incorrect_reply = replies(:reply_one_travel_thread_spanish)
  end

  test "should redirect if not logged in" do
  	get :index, user_id: @user.id
  	assert_redirected_to login_url
  	get :create, user_id: @user.id
  	assert_redirected_to login_url
  	log_in_as @user
  	get :index, user_id: @user.id
  	assert_response :success
  end

  test "redirects if doesn't speak thread language" do
  	log_in_as @user
  	xhr :post, :create, user_id: @user.id, reply: {
  		replyable_type: @thread.class.name,
  		replyable_id: @thread.id,
  		content: "This is a reply. Long reply"
  	}
  	assert_response :success
  	xhr :post, :create, user_id: @user.id, reply: {
  		replyable_type: @incorrect_thread.class.name,
  		replyable_id: @incorrect_thread.id,
  		content: "This is a reply. Long reply"
  	}
  	assert_redirected_to root_url
  end

  test "redirects if trying to edit incorrect post" do
    log_in_as @user
    get :edit, id: @incorrect_reply.id, user_id: @user.id
    assert_redirected_to unauthorized_access_url
    get :edit, id: @reply.id, user_id: @user.id
    assert_response :success
  end

  test "redirects if trying to update incorrect post" do
    log_in_as @user
    new_content = "hello" * 20
    get :edit, id: @reply.id, user_id: @user.id
    assert_response :success
    xhr :patch, :update, id: @incorrect_reply.id, user_id: @user.id, reply: {
      content:  new_content
    }
    assert_redirected_to unauthorized_access_url
    get :edit, id: @reply.id, user_id: @user.id
    xhr :patch, :update, id: @reply.id, user_id: @user.id, reply: {
      content:  new_content
    }
    assert_response :success
    @updated_reply = Reply.find(@reply.id)
    assert_equal new_content, @updated_reply.content
  end

end

require 'test_helper'

class SuggestionsControllerTest < ActionController::TestCase
  
  def setup

  end

  test "should be able to send suggestion" do
    assert_difference "Suggestion.count", 1 do
      post :create, suggestion: {
        name: "mynameis",
        email: "memail@email.com",
        subject: 'killer line',
        message: "a"*51 
      }
    end
    assert_response :success
  end
end

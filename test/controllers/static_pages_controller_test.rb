require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

	def setup 
		@user = users(:michael)
	end

  test "should get home_offline" do
    get :home_offline
    assert_response :success
  end

  test "should redirect dashboard if not logged in" do
  	get :dashboard
  	assert_response :redirect
  end

  test "should show dashboard after loggin in" do
  	get :home_offline
  	assert_response :success
  	log_in_as @user
  	get :dashboard
  	assert_response :success
  end

end

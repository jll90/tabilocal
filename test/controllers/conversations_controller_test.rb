require 'test_helper'

class ConversationsControllerTest < ActionController::TestCase
  
  def setup
  	@user = users(:michael)
  	@user_belongs_to_conversation = conversations(:one)
  	@other_conversation = conversations(:two)
  end

  test "must log in to index conversations" do
  	get :index
  	assert_redirected_to login_url
  end

  test "must complete profile before indexing conversations" do
    log_in_as @user
    get :index
    assert_response :success
  end

  test "must login to show" do
  	xhr :get, :show, id: @user_belongs_to_conversation.id
  	assert_redirected_to login_url
  	# log_in_as @user
  	# xhr :get, :show, id: @user_belongs_to_conversation.id
  	# assert_response :success
  	# throws error due to profile picture not present, should check
  end

  test "cannot show a conversation belonging to another user" do
  	log_in_as @user
  	xhr :get, :show, id: @other_conversation.id
  	assert_redirected_to unauthorized_access_url
  end

end

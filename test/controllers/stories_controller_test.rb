require 'test_helper'

class StoriesControllerTest < ActionController::TestCase
  
  def setup
  	@user = users(:archer) #archer and mallory are friends
    @other_user = users(:mallory)
    @not_friend_user = users(:michael)
    @story = stories(:archer_to_mallory)
    @incorrect_story = stories(:mallory_to_michael)
  end

  test "should redirect new if not logged in" do
    get :new
    assert_redirected_to login_url
    log_in_as @user
    get :new, recipient_id: @other_user.id
    assert_response :success
  end

  test "should redirect index if not logged in" do
  	get :index, user_id: @user.id
  	assert_redirected_to login_url
  	log_in_as @user
  	get :index, user_id: @user.id
  	assert_response :success
  end

  test "should redirect create if not logged in" do
    xhr :post, :create, user_id: @user.id, story: {
      user_id: @user.id,
      recipient_id: @other_user.id,
      content: "This is the content of the story"
    }
    assert_redirected_to login_url
    log_in_as @user
    xhr :post, :create, user_id: @user.id, story: {
      recipient_id: @other_user.id,
      content: "This is the content of the story"
    }
    assert_response :success
  end

  test "must be friends to write a story" do
    log_in_as @user
    xhr :post, :create, user_id: @user.id, story: {
      user_id: @user.id,
      recipient_id: @not_friend_user.id,
      content: "This is the story's content"
    }
    #Redirection with JS, so it shows 200
    assert_response :success
  end

  test "should redirect edit if not logged in" do
    get :edit, id: @story.id
    assert_redirected_to login_url
    log_in_as @user
    get :edit, id: @story.id, recipient_id: @story.recipient.id
    assert_response :success
  end

  test "should redirect update if not logged in" do
    patch :update, id: @story.id, story: {
      user_id: @story.user_id,
      recipient_id: @story.recipient_id,
      content: @story.content
    }
    assert_redirected_to login_url
    log_in_as  @user
    patch :update, id: @story.id, story: {
      user_id: @story.user_id,
      recipient_id: @story.recipient_id,
      content: @story.content
    }
    assert_response :success
  end

  test "cannot edit incorrect story" do
    log_in_as @user
    get :edit, id: @incorrect_story.id
    assert_redirected_to unauthorized_access_url
  end

  test "cannot update incorrect story" do
    log_in_as @user
    get :edit, id: @story.id
    xhr :patch, :update, id: @incorrect_story.id, story: {
     user_id: @story.user_id,
      recipient_id: @story.recipient_id,
      content: @story.content 
    }
    assert_redirected_to unauthorized_access_url
  end

  test "redirects to edit if story exists" do
    log_in_as @user
    get :new, recipient_id: @other_user.id, check_story_exists: true
    assert_redirected_to edit_story_path(id: @story.id)
  end
end
require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest

	def assert_profile_cover_links(user)
		assert_select ".profile-cover" do
			assert_select "a[href=?]", user_path(user), 1
			assert_select "a[href=?]", user_friendships_path(user), 1
			assert_select "a[href=?]", user_replies_path(user), 1
			assert_select "a[href=?]", user_photos_path(user), 1
			assert_select "a[href=?]", user_stories_path(user), 1
		end
	end

	def setup
		@user = users(:michael)
		@other_user = users(:archer)
		log_in_as @user
		get dashboard_path
		assert_select ".online-navbar" do
			assert_select "a[href=?]", user_path(@user), 1
		end
		get user_path(@user)
		assert_template "shared/_profile_cover"
	end

	test "self profile_cover_links" do
		assert_profile_cover_links(@user)
		assert_select ".profile-cover a[href=?]", conversations_path, 1
	end

	test "other profile_cover_links" do
		get user_path(@other_user)
		assert_profile_cover_links(@other_user)
		assert_select ".profile-cover a[href=?]", conversations_path(:message_to => @other_user.id), 1
	end

end
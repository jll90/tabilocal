require 'test_helper'

class UserLanguageFlowTest < ActionDispatch::IntegrationTest
  
	def setup
		@user = users(:michael)
		@user_language = user_languages(:michael_english)
		@user_languages = @user.user_languages
 		@other_user = users(:archer)
 		@other_user_language = user_languages(:archer_english)
 		log_in_as @user
 		get user_path(id: @user.id)
 		assert_template :show	
 		assert_select "form#add-user-language-K" do
 			assert_select "input#user_language_category[value=?]", "K", 1
 		end
 		assert_select "form#add-user-language-L" do
 			assert_select "input#user_language_category[value=?]", "L", 1
 		end
 		@user_languages.each do |user_language|
 			assert_select "a[href=?][data-method=delete]", user_language_path(id: user_language.id), 1
 		end
	end

 	test "should destroy self language pair" do
 		assert_difference "UserLanguage.count", -1 do 
 			xhr :delete, user_language_path(@user_language)
 		end
 		assert_template 'user_languages/destroy.js.erb'
 	end

 	test "should not destroy other_user's language pairs" do
 		assert_no_difference "UserLanguage.count" do
 			xhr :delete, user_language_path(@other_user_language)
 		end
 		assert_redirected_to unauthorized_access_url
 	end
end

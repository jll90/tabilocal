require 'test_helper'

class ThreadsInterfaceTest < ActionDispatch::IntegrationTest

 	def setup
 		@user = users(:michael)
 		@user_languages_count = @user.user_languages.count
 		@other_user = users(:archer)
 		@title = "this is a good title"
 		@tags_count = Tag.count
 		@countries_count = Country.count
 		@travel_thread = travel_threads(:travel_thread_english)
 		@other_travel_thread = travel_threads(:travel_thread_spanish)
 		
 		
 		log_in_as @user
 		follow_redirect!
 		assert_select "a[href=?]", travel_threads_path, 1
 		get travel_threads_path
 		assert_template 'travel_threads/index'
 		assert_select "span[queryable-category=?]", "languages", @user_languages_count
 		assert_select "span[queryable-category=?]", "tags", @tags_count
 		assert_select "span[queryable-category=?]", "places", @countries_count
 		assert_select "a[href=?]", new_travel_thread_path, 1
 	end

 	test "can create a travel thread, first reply, taggings and subscription" do
 		get new_travel_thread_path
 		assert_select "select#travel_thread_language_id" do
 			assert_select "option", @user_languages_count
 		end
 		assert_select "span.tag", @tags_count
 		assert_select "form#new_travel_thread", 1
 		assert_difference "Tagging.count", 2 do #count should be replaced by users
 			assert_difference "Reply.count", 1 do
 				assert_difference "Subscription.count", 1 do
			 		assert_difference "TravelThread.count", 1 do
			 			post travel_threads_path travel_thread: {
							title: @title,
							user_id: @user.id,
							place_id: places(:kyoto).id,
							language_id: @user.languages.last.id,
							replies_attributes: 
								[{
									user_id: @user.id,
									content: "this is the content"
								}],
							taggings_attributes: 
								[
									{tag_id: tags(:food).id}, 
									{tag_id: tags(:safety).id}
								],
							subscriptions_attributes:
							  [
							  	{user_id: @user.id}
							  ]
						  }
			 		end
			 	end
		 	end
		end
		@thread_id = TravelThread.first.id #scope is backwards
 		assert_equal "window.location='/en/travel_threads/#{@thread_id}'", response.body
 	end


 	test "thread in another language is not rendered" do
 		assert_select "a[href=?]", travel_thread_path(@other_travel_thread), 0
 	end

 	#archer is not subscribed
 	test "can subscribe and unsubscribe from thread cprrect susbscription status" do
 		assert_select "a[href=?]", travel_thread_path(@travel_thread), 1
 		get travel_thread_path(@travel_thread)
 		assert_template 'travel_threads/show'
 		#user is already subscribed
 		subscription = @user.subscriptions.where(
 			subscriptionable_type: @travel_thread.class.name,
 			subscriptionable_id: @travel_thread.id
 			).first
 		assert_template 'subscriptions/_unsubscribe'
 		assert_difference "Subscription.count", -1 do
 			xhr :delete, user_subscription_path(id: subscription.id, user_id: @user.id)
 		end
 		assert_template 'subscriptions/_subscribe'
 		assert_difference "Subscription.count", 1 do
 			xhr :post, user_subscriptions_path(user_id: @user.id), subscription: {
 				subscriptionable_id: @travel_thread.id,
 				subscriptionable_type: @travel_thread.class.name
 			}
 		end
 		assert_template 'subscriptions/_unsubscribe'
 	end

 	test "can add a reply proper rendering" do
 		get travel_thread_path(id: @travel_thread)
 		assert_template 'travel_threads/show'
 		assert_select '#replies-list' do
 			assert_select '> div', @travel_thread.replies.count
 		end
 		assert_difference "Reply.count", 1 do
 			xhr :post, user_replies_path(user_id: @user.id), reply: {
 				replyable_id: @travel_thread.id,
 				replyable_type: @travel_thread.class.name,
 				content: "here is my reply to this thread"
 			}
 		end
 	end

end
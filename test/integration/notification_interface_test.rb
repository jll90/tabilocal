require 'test_helper'

class NotificationsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @notifications = @user.notifications
    log_in_as @user
    follow_redirect!
  end

  test "the number of notification is correct" do
    assert_select "#notification-dropdown" do
      @notifications.each do |notification|
        assert_select "a[href=?]", notification_path(id: notification)
      end
    end
  end

end
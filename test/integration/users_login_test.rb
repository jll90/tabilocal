require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

	def setup
		@user = users(:michael)
	end

	test "login with invalid information" do
		get login_path
		assert_template "static_pages/home_offline"
		post login_path, session: { email: "", password: ""}
		assert_redirected_to root_url
		assert_not flash.empty?
	end

	test "login with valid information" do
		get login_path
		post login_path, session: {email: @user.email, password: "password"}
		assert is_logged_in?
		assert_redirected_to dashboard_path(:locale => @user.language_preference)
		follow_redirect!
		assert_template "static_pages/dashboard"
		assert_select "a[href=?]", login_path, count: 0
		assert_select "a[href=?]", logout_path
		assert_select "a[href=?]", user_path(@user)
		delete logout_path
		assert_not is_logged_in?
		assert_redirected_to login_url
		delete logout_path
		follow_redirect!
		assert_select "form[action=?]", login_path
		assert_select "a[href=?]", logout_path, count: 0
		assert_select "a[href=?]", user_path(@user), count: 0
	end

	test "login with remembering" do
		log_in_as(@user, remember_me: "1")
		assert_not_nil cookies["remember_token"]
	end

	test "login without remembering" do
		log_in_as(@user, remember_me: "0")
		assert_nil cookies["remember_token"]
	end



end
require 'test_helper'

class MessageInterfaceTest < ActionDispatch::IntegrationTest
  def setup
  	@user = users(:michael)
    @conversations_count = @user.user_conversations.length
  	@conversation = conversations(:one)
    @content = "message content"
  	log_in_as @user
    get dashboard_path #so it renders the navbar
  	assert_select "a[href=?]", conversations_path, 1
  	get conversations_path
  	assert_select "a[href=?]", conversation_path(@conversation)
    assert_select ".conversation-thumbnail", @conversations_count
  end

  

  test "can send a message" do
  	assert_difference "Message.count", 1 do
  		xhr :post, messages_path(message: {conversation_id: @conversation.id, 
  			content: "standard content"})
  	end
    
  end

  test "empty message will bounce" do
  	assert_no_difference "Message.count" do
  		xhr :post, messages_path(message: {conversation_id: @conversation.id, 
  			content: ""})
  	end
  end
end

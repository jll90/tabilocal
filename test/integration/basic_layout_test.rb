require 'test_helper'

class BasicLayoutTest < ActionDispatch::IntegrationTest	

	def setup
		@user = users(:michael)
	end

	test "basic layout" do
		log_in_as @user
		follow_redirect!

		assert_select ".online-navbar" do
			assert_select "a[href=?]", conversations_path, 1
			assert_select "a[href=?]", user_path(@user), 1
			assert_select "a[href=?]", travel_threads_path, 1
			assert_select "#friend-request-dropdown", 1
			assert_select "#notification-dropdown", 1
		end
	end

	test "no banner navigation" do
		get root_url
		assert_select ".banner", 0
		get new_user_path
		assert_select ".banner", 0
		log_in_as @user
		get edit_user_path @user
		assert_select ".banner", 0
	end

	test "single banner" do
		log_in_as @user
		get conversations_path
		assert_select ".banner", 1
	end

end
require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
	def setup
		@user = users(:michael)
	end

	test "unsuccessful edit" do
		log_in_as(@user)
		get edit_user_path(@user)
		assert_template 'users/edit'
		xhr :patch, user_path(@user), user: {
			firstname: "",
			lastname: "",
			email: "foo@invalid",
			gender: "",
			#invalid dob values such as string check for pending
			# dob: "1990-01-01",
			password: "foo",
			password_confirmation: "foobar"
		}
		assert_template 'errorHandler.js.erb'
	end


=begin 
	test "successful edit w/ friendly forwarding" do
		get edit_user_path(@user)
		log_in_as(@user)
		assert_redirected_to dashboard_path
		
		firstname = "FooBar"
		lastname = "BarFoo"
		email = "foo@bar.com"
		patch user_path(@user), user: {
			firstname: firstname,
			lastname: lastname,
			email: email,
			gender: "F",
			dob: "1990-01-01",
			password: "",
			password_confirmation: ""
		}
		assert_not flash.empty?
		assert_redirected_to @user
		@user.reload
		assert_equal firstname, @user.firstname
		assert_equal email, @user.email
	end
=end
end
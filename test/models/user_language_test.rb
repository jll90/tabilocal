require 'test_helper'

class UserLanguageTest < ActiveSupport::TestCase

	def setup
		@user = users(:michael)
		@category = "L"
		@user_language = @user.user_languages.build(
			language_id: languages(:spanish).id,
			category: @category)
		@existing_user_language = user_languages(:michael_english)
	end

	test "user language is valid" do
		assert @user_language.valid?
	end

	test "can save a user language pair" do
		assert_difference "UserLanguage.count", 1 do 
			@user_language.save
		end
	end

	test "cannot save the same language twice" do
		assert_difference "UserLanguage.count", 1 do
			@user_language.save
		end
		assert_no_difference "UserLanguage.count" do
			@user_language.save
		end
	end

	test "category must be L or K of length 1" do
		@user_language.category = "K"
		assert @user_language.valid?
		@user_language.category = "MM"
		assert_not @user_language.valid?
	end

	test "saving increments user languages count" do
		assert_difference "@user.user_languages.count", 1 do
			@user_language.save
		end
	end

	test "can destroy a existing_user_language" do
		assert_difference "UserLanguage.count", -1 do
			@existing_user_language.destroy
		end
	end

	test "destroying decrements user languages count" do
		
		assert_difference "@user.user_languages.count", -1 do
			@existing_user_language.destroy
		end
		
	end
end
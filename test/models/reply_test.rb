require 'test_helper'

class ReplyTest < ActiveSupport::TestCase

  def setup
	  @travel_thread = travel_threads(:travel_thread_english)
	  @user = users(:michael)
	  @reply = Reply.new(
	  	replyable_type: @travel_thread.class.name,
	  	replyable_id: @travel_thread.id,
	  	content: "a"* 20,
	  	user_id: @user.id,
      first: false
	  	)
  end

  test "reply is valid" do
  	assert @reply.valid?
  end

  test "content must be of ten characters at least" do
  	@reply.content = ""
  	assert_not @reply.valid?
  	@reply.content = "a"*10
  	assert @reply.valid?
  end

  test "replyable_type must be correct" do
  	@reply.replyable_type = "AFASDFASDF"
  	assert_not @reply.valid?
  end

  test "reply creates notifications for subscribed users who are not self" do
  	assert_difference "Reply.count", 1 do
  		assert_difference "Notification.count", 1 do
  			@reply.save #creates one notification even though there are two subscribers
  		end #becaues wont notify the user who wrote the reply (michael)
  	end
  end

  test "reply first must be either true or false" do
    #set up to false on setup method
    # @reply.first = "jfdsoapfj"
    # assert_not @reply.valid?
    # the above is a false positive
    # must implement a custom validator
    @reply.first = true
    assert @reply.valid?
  end

#the result sought for on the test shown below works though it throws a false negative, it could be due to
#the record not updating at the time of calling the method assert_equal
# test "creating a reply increments the corresponding thread count" do
#    replies = @reply.replyable.replies_count
#    @reply.save
#    #emulates a reload
#    @updated_reply = Reply.find(@reply.id)
#    replyable = @updated_reply.replyable
#    replyable.reload
#    assert_equal replyable.replies_count, (replies + 1)
#  end  

end
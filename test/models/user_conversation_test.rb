require 'test_helper'

class UserConversationTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		@new_user = users(:mallory)
		@conversation = conversations(:one)
	end

	test "cannot add a user twice to a conversation" do
		assert_no_difference "UserConversation.count" do
			@conversation.user_conversations.create(user_id: @user.id)	
		end
	end

	test "can add a new user to a conversation" do
		assert_difference "UserConversation.count", 1 do
			@conversation.user_conversations.create(user_id: @new_user.id)
		end
	end

	test "conversation (parent) must exist" do
		@uc = UserConversation.new(conversation_id: 4234, user_id: @user.id)
		assert_no_difference "UserConversation.count" do
			@uc.save
		end
	end
 

	#test "user (parent) must exist" do
		#passes test but fails with database PG foreign-keys
end

require 'test_helper'

class FriendRequestTest < ActiveSupport::TestCase

	def setup
		@user = users(:michael)
		@friend = users(:archer)

		@friend_request = @user.friend_requests.build(friend_id: @friend.id)
		@self_friend_request  = @user.friend_requests.build(friend_id: @user.id)
        @existing_friend_request = friend_requests(:john_michael)
	end

	test "basic validity test" do
		assert @friend_request.valid?
	end

    test "cannot add oneself" do
    	assert_not @self_friend_request.valid?
    end 

    test "can send a friend request but not send a second one" do
    	assert_difference "FriendRequest.count", 1 do
    		@friend_request.save
    	end
    	assert_no_difference "FriendRequest.count" do
    		@friend_request.save
    	end
    end

    test "accept friend request notifies user" do
        assert_difference "Notification.count", 1 do
            @friend_request.accept
        end
    end



end

require 'test_helper'

class StoryTest < ActiveSupport::TestCase  

  def setup
  	@user = users(:michael)
  	@other_user = users(:archer)
    #they are not friends so they need to become friends for the last test to pass
  	@story = Story.new(user_id: @user.id, recipient_id: @other_user.id, content: "a" * 1001, friendship_pre_validated: true)
  	@repeated_story = Story.new(user_id: @user.id, recipient_id: @other_user.id, content: "a" * 1001, friendship_pre_validated: true)
  	@inverse_story = Story.new(user_id: @other_user.id, recipient_id: @user.id, content: "a" * 1001, friendship_pre_validated: true)
    @expired_story = Story.new(user_id: @user.id, recipient_id: @other_user.id, content: "a" * 1001, friendship_pre_validated: true, created_at: Time.now - 25.hours, updated_at: Time.now - 25.hours)
  end

  test "story is valid" do
  	assert @story.valid?
  end

  test "story must have content" do
  	@story.content = ""
  	assert_not @story.valid?
  end

  test "story cannot be about self" do
  	@story.recipient_id = @user.id
  	assert_not @story.valid?
  end

  test "cannot send same story twice" do
  	assert_difference "Story.count", 1 do
  		@story.save
  	end
  	assert_no_difference "Story.count" do
  		@repeated_story.save
  	end
  end

  test "can receive a story from user I wrote a story to" do
  	assert_difference "Story.count", 1 do
  		@inverse_story.save
  	end
  end

  test "sending a story creates a notification" do
    assert_difference "Notification.count", 1 do
      @story.save
    end
  end

  test "must be friends to write a story" do
    @story.friendship_pre_validated = false
    assert_not @story.valid?
    @story.friendship_pre_validated = nil
    assert_not @story.valid?
    Friendship.create!(user_id: @user.id, friend_id: @other_user.id)
    assert @story.valid?
  end

  test "cannot destroys a expired story" do
    #pre validated friendship filled_in
    assert_difference "Story.count", 1 do
      @expired_story.save
    end 
    assert_no_difference "Story.count" do
      @expired_story.destroy
    end
  end

  test "can destroy a non expired story" do
    #pre validated friendship filled_in
    assert_difference "Story.count", 1 do
      @story.save
    end
    assert_difference "Story.count", -1 do
      @story.destroy
    end
  end

end
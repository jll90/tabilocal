require 'test_helper'

class UserTest < ActiveSupport::TestCase

	def setup
		@user = User.new(firstname: "Example", lastname: "User",
						email: "user2@example.com", gender: "M", 
						dob: "1990-08-05", password: "foobar",
						password_confirmation: "foobar",
						gender_privacy: "F",
						birthday_privacy: "F",
						language_preference: "en")
    @user_with_place = users(:michael)
	end

	test "should be valid" do
		assert @user.valid?

	end

	test "firstname should be present" do
		@user.firstname = "    "
		assert_not @user.valid?
	end

	test "lastname should be present" do
		@user.lastname = "    "
		assert_not @user.valid?
	end

	test "email should be present" do
		@user.email = "    "
		assert_not @user.valid?
	end

	test "gender should be present" do
		@user.gender = "    "
		assert_not @user.valid?
	end

	test "dob should be present" do
		@user.dob = "    "
		assert_not @user.valid?
	end

	test "email validation should accept valid addresses" do
	    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
	                         first.last@foo.jp alice+bob@baz.cn]
	    valid_addresses.each do |valid_address|
	      @user.email = valid_address
	      assert @user.valid?, "#{valid_address.inspect} should be valid"
	    end
    end

	test "email addresses should be unique" do
	    duplicate_user = @user.dup
	    duplicate_user.email = @user.email.upcase
	    @user.save
      @user.errors.each do |e, v|
        puts "#{e} #{v}"
      end
	    assert_not duplicate_user.valid?
  	end

  	test "gender validation accepts valid values" do
  		valid_genders = %w[M F O]
  		valid_genders.each do |valid_gender|
  			@user.gender = valid_gender
  			assert @user.valid?, "#{valid_gender.inspect} should be valid"
  		end
  	end

  	test "authenticated? should return false for a user with nil digest" do
  		assert_not @user.authenticated?(:remember, "")
  	end

  	test "birthday privacy takes valid values" do 
  		valid_birthday_privacy_values = %w[T F]
  		valid_birthday_privacy_values.each do |valid_birthday_privacy_value|
  			@user.birthday_privacy = valid_birthday_privacy_value
  			assert @user.valid?, "#{valid_birthday_privacy_value.inspect} should be valid"
  		end
  	end

  	test "gender privacy takes valid values" do 
  		valid_gender_privacy_values = %w[T F]
  		valid_gender_privacy_values.each do |valid_gender_privacy_value|
  			@user.gender_privacy = valid_gender_privacy_value
  			assert @user.valid?, "#{valid_gender_privacy_value.inspect} should be valid"
  		end
  	end

  	test "language_preference takes valid values" do 
  		valid_language_preference_values = %w[en es ja]
  		valid_language_preference_values.each do |valid_language_preference_value|
  			@user.language_preference = valid_language_preference_value
  			assert @user.valid?, "#{valid_language_preference_value.inspect} should be valid"
  		end
  	end

  	test "friends_count takes positive values" do
  		@user.friends_count = -1
  		assert_not @user.valid?
  		@user.friends_count = 1
  		assert @user.valid?
  	end

  	test "posts_count takes positive values" do
  		@user.posts_count = -1
  		assert_not @user.valid?
  		@user.posts_count = 1
  		assert @user.valid?
  	end

  	test "stories_count takes positive values" do
  		@user.stories_count = -1
  		assert_not @user.valid?
  		@user.stories_count = 1
  		assert @user.valid?
  	end

    test "languages_count takes on positive values" do
      @user.languages_count = -1
      assert_not @user.valid?
      @user.languages_count = 0
      assert @user.valid?
    end

    test "updating current residence updates both places counter" do
      @new_residence = places(:atokawachi)
      @previous_residence = places(:kyoto)
      new_residence_residents = @new_residence.residents
      previous_residence_residents = @previous_residence.residents
      
      @user_with_place.update(current_residence_id: @new_residence.id)
      
      @previous_residence.reload
      @new_residence.reload
      assert_equal @new_residence.residents, (new_residence_residents + 1)
      assert_equal @previous_residence.residents, (previous_residence_residents -1 )
    end

    test "updating hometown updates both places counter" do
      @new_hometown = places(:kyoto)
      @previous_hometown = places(:atokawachi)
      new_hometown_natives = @new_hometown.natives
      previous_hometown_natives = @previous_hometown.natives
      
      @user_with_place.update(hometown_id: @new_hometown.id)
      
      @previous_hometown.reload
      @new_hometown.reload
      assert_equal @new_hometown.natives, (new_hometown_natives + 1)
      assert_equal @previous_hometown.natives, (previous_hometown_natives -1 )
    end
end
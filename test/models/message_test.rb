require 'test_helper'

class MessageTest < ActiveSupport::TestCase
	def setup
		@user = users(:archer)
		@conversation = conversations(:one)
		@message = @user.messages.build(conversation_id: @conversation.id,
			content: "non-blank")
	end

	test "basic validation" do
		assert @message.valid?
	end

	test "conversation id must exist" do
		@message.conversation_id = nil
		assert_not @message.valid?
	end

	test "content must at least contain a character" do
		@message.content = ""
		assert_not @message.valid?
		@message.content = "a"
		assert @message.valid?
	end

	test "can save message " do 
		assert_difference "Message.count", 1 do
			@message.save
		end
	end
end

require 'test_helper'

class FriendshipTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		@friend = users(:archer)

		@friendship = @user.friendships.build(friend_id: @friend.id)
		@self_friendship  = @user.friendships.build(friend_id: @user.id)
	end

	test "can create friendship" do
		assert @friendship.valid?
	end

    test "cannot be friends with oneself" do
    	assert_not @self_friendship.valid?
    end 

    
    test "creating a friendship creates inverse relation" do
    	assert_difference "Friendship.count", 2 do
    		@friendship.save
    	end
    end

    test "destroying a friendship destroys inverse relation" do
    	@friendship.save
    	assert_difference "Friendship.count", -2 do
    		Friendship.last.destroy
    	end
    end

    test "destroying a friendship destroys corresponding stories" do
        @other_user = users(:lana)
        @friendship = Friendship.where(user_id: @user.id, friend_id: @other_user.id).first
        #there are two stories for michael and lana <=>
        assert_difference "Story.count", -2 do
            @friendship.destroy
        end
    end

end


require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		@reply = replies(:reply_one_travel_thread_english) #these three are a simulation of michaels
		@friendship = friendships(:michael_mallory) #potential notifications
		@story = stories(:mallory_to_michael)
		@notification = Notification.new(
			#simulates a reply
			user_id: @user.id,
			notificationable_id: @reply.id,
			notificationable_type: @reply.class.name
			)
	end

	test "initialization sets read as false" do
		assert_equal @notification.read, false
	end

	test "notification is valid" do
		assert @notification.valid?
	end

	test "class name must be correct (polymorphic)" do
		@notification.notificationable_type = "fdsaopfha"
		assert_not @notification.valid?
		@notification.notificationable = @friendship
		assert @notification.valid?
		@notification.notificationable = @story
		assert @notification.valid?
	end

end
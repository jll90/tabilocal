require 'test_helper'

class LanguageThreadTest < ActiveSupport::TestCase

	# def setup
	# 	@user = users(:michael)
	# 	@language_thread = @user.language_threads.build(
	# 		title: "standard title",
	# 		language_id: @user.languages.first.id,
	# 		about_language_id: @user.languages.last.id,
	# 		replies_attributes:
	# 			[{
	# 				user_id: @user.id,
	# 				content: "Standardized content"
	# 			}],
	# 		taggings_attributes:
	# 			[
	# 				{tag_id: tags(:punctuation).id},
	# 				{tag_id: tags(:grammar).id}
	# 			],
	# 		subscriptions_attributes: 
	# 			[{
	# 			    user_id: @user.id
	# 				}]
	# 		)
	# end

	# test "language thread is valid" do
	# 	assert @language_thread.valid?
	# end

	# test "user_id must be present and exist in users table" do
	# 	@language_thread.user_id = nil
	# 	assert_not @language_thread.valid?
	# end


	# test "title must be present and within the correct length" do
	# 	@language_thread.title = "a"
	# 	assert_not @language_thread.valid?
	# 	@language_thread.title = "a" * 129
	# 	assert_not @language_thread.valid?
	# 	@language_thread.title = nil
	# 	assert_not @language_thread.valid?
	# end

	# test "must have a reply w/ content" do
	# 	@language_thread.replies[0].content = ""
	# 	assert_not @language_thread.valid?
	# 	@language_thread.replies = []
	# 	assert_not @language_thread.valid?
	# end

	# test "can save a reply" do
	# 	assert_difference "Reply.count", 1 do
	# 		assert_difference "LanguageThread.count", 1 do
	# 			@language_thread.save
	# 		end
	# 	end
	# end

	# test "user must speak language(s) the topic is about/in" do
	# 	@language_thread.language_id = 5437057
	# 	assert_not @language_thread.valid?
	# 	@language_thread.language_id = @user.languages.first.id
	# 	assert @language_thread.valid?

	# 	@language_thread.about_language_id = 543587
	# 	assert_not @language_thread.valid?
	# 	@language_thread.about_language_id = @user.languages.last.id
	# 	assert @language_thread.valid?
	# end

	# test "must have a tag" do
	# 	@language_thread.taggings = []
	# 	assert_not @language_thread.valid?
	# end

	# test "can save both tags" do
	# 	assert_difference "Tagging.count", 2 do
	# 		assert_difference "LanguageThread.count", 1 do
	# 			@language_thread.save
	# 		end
	# 	end
	# end

end

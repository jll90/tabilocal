require 'test_helper'

class TravelThreadTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)

		@travel_thread = @user.travel_threads.build(
			title: "standard title",
			language_id: @user.languages.first.id,
			place_id: places(:kyoto).id,
			replies_attributes:
				[{
					user_id: @user.id,
					content: "Standardized content",
					first: true
				}],
			taggings_attributes:
				[
					{tag_id: tags(:safety).id	},
					{tag_id: tags(:transportation).id   }
				],
			subscriptions_attributes: 
				[{user_id: @user.id}]
			)

	end

	test "travel thread is valid" do
		assert @travel_thread.valid?
	end

	test "replies set to 0 upon init" do
		assert_equal @travel_thread.replies_count, 0
	end

	test "user_id must be present and exist in users table" do
		@travel_thread.user_id = nil
		assert_not @travel_thread.valid?
		@travel_thread.user_id = 21332
		assert_not @travel_thread.valid?
	end


	test "title must be present and within the correct length" do
		@travel_thread.title = "a"
		assert_not @travel_thread.valid?
		@travel_thread.title = nil
		assert_not @travel_thread.valid?
	end

	test "must have a single reply w/ content" do
		@travel_thread.replies[0].content = ""
		assert_not @travel_thread.valid?
		@travel_thread.replies = []
		assert_not @travel_thread.valid?
		@travel_thread.replies << Reply.new(content: 'abjfdsafjda', user_id: @user.id)
		@travel_thread.replies << Reply.new(content: 'abjfdsafjda', user_id: @user.id)
	end

	test "can save a reply" do
		assert_difference "Reply.count", 1 do
			assert_difference "TravelThread.count", 1 do
				@travel_thread.save
			end
		end
	end

	test "user must speak travel(s) the topic is about/in" do
		@travel_thread.language_id = 5437057
		assert_not @travel_thread.valid?
		@travel_thread.language_id = @user.languages.first.id
		assert @travel_thread.valid?
	end

	test "must have a tag" do
		@travel_thread.taggings = []
		assert_not @travel_thread.valid?
	end

	test "can save both tags" do
		assert_difference "Tagging.count", 2 do
			assert_difference "TravelThread.count", 1 do
				@travel_thread.save
			end
		end
	end

	test "must not exceed 3 tags" do 
		@travel_thread.taggings = []

		@travel_thread.taggings << Tagging.new(tag_id: tags(:acommodation).id)
		@travel_thread.taggings << Tagging.new(tag_id: tags(:music).id)
		@travel_thread.taggings << Tagging.new(tag_id: tags(:weather).id)

		assert @travel_thread.valid?
		
		@travel_thread.taggings << Tagging.new(tag_id: tags(:manners).id)
		
		assert_not @travel_thread.valid?
	end

	test "can save the subscription" do 
		assert_difference "Subscription.count", 1 do
			assert_difference "TravelThread.count", 1 do
				@travel_thread.save
			end
		end
	end

	test "reply's user_id must match thread's " do
		@travel_thread.replies = []
		@travel_thread.replies << Reply.new(content: 'abjfdsafjda', user_id: 123213)

		assert_not @travel_thread.valid?
	end

	test "subscription's user_id must match thread's " do
		@travel_thread.subscriptions = []
		@travel_thread.subscriptions << Subscription.new(user_id: 123213)

		assert_not @travel_thread.valid?
	end

	test "thread's language_id must match one of user's languages" do
		@travel_thread.language_id = 321321
		assert_not @travel_thread.valid?
	end

	test "creating a thread updates place count" do
		threads = @travel_thread.place.threads
		@travel_thread.save
		@travel_thread.reload
		assert_equal @travel_thread.place.threads, threads + 1
	end

end
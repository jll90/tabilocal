require 'test_helper'

class SubscriptionTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		@other_user = users(:archer)
		@subscription = Subscription.new(user_id: @user.id)
		@travel_thread = travel_threads(:travel_thread_english)
		@repeated_subscription = Subscription.new(
			user_id: @user.id,
			subscriptionable_id: @travel_thread.id,
			subscriptionable_type: @travel_thread.class.name
			)
	end

	test "subscription is valid" do
		assert @subscription.valid?
	end

	test "cannot subscribe to the same thing" do
		assert_no_difference "Subscription.count" do
			@repeated_subscription.save
		end
	end


	#cannot subscribe to something that doesn't exist
end
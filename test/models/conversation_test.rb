require 'test_helper'

class ConversationTest < ActiveSupport::TestCase
  
  def setup
  	@user = users(:michael)
  	@other_user = users(:archer)
    @third_user = users(:lana)
  	@conversation = Conversation.new(
  		user_conversations_attributes:
  		[{user_id: @user.id},{user_id: @other_user.id}],
  		messages_attributes: 
  		[{content: "standard content", user_id: @user.id}]
  	)

    @public_conversation = Conversation.new(
      user_conversations_attributes:
      [{user_id: @user.id},{user_id: @other_user.id}, {user_id: @third_user.id}],
      messages_attributes: 
      [{content: "standard content", user_id: @user.id}]
    )
  end

  test "basic validity test" do
  	assert @conversation.valid?
  end

  test "creates two user_conversations and a message" do
  	assert_difference "Message.count", 1 do
	  	assert_difference "UserConversation.count", 2 do
	  		@conversation.save
	  	end
	end
  end

  test "wont save if two users are identical" do
  	@conversation.user_conversations[1].user_id = @user.id
  	assert_no_difference "UserConversation.count" do
  		@conversation.save
  	end
  end

  test "user_conversations must be greater than 1" do
    @conversation.user_conversations = []
    @conversation.user_conversations << UserConversation.new(user_id: @user.id)
    assert_not @conversation.valid?
  end


  test "content must be present in message" do
  	@conversation.messages[0].content = ""
  	assert_not @conversation.valid?
  end

  test "conversation between two people saves sets privacy as true" do
    @conversation.save
    assert_equal @conversation.private, true
  end

  test "conversation between three or more people saves sets privacy as false" do
    @public_conversation.save
    assert_equal @public_conversation.private, false
  end

  test "cannot create a duplicated conversation (private)" do
    @copy = @conversation
    @conversation.save
    assert_no_difference "Conversation.count" do
      @copy.save
    end
  end


end
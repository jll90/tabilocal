require 'test_helper'

class UserPlaceTest < ActiveSupport::TestCase
 

	def setup
		@user = users(:archer)
		@category = "L"
		@other_category = "T"
		@user_place = @user.user_places.build(
			place_id: places(:kyoto).id,
			category: @category
			)
		@user_place_2 = @user.user_places.build(
			place_id: places(:kyoto).id,
			category: @other_category
			)
	end

	test "user_place is valid" do
		assert @user_place.valid?
	end

	test "needs a place_id" do
		@user_place.place_id = nil
		assert_not @user_place.valid?
	end

	test "needs category of valid value and length 1" do
		@user_place.category = "K"
		assert_not @user_place.valid?
		@user_place.category = "MM"
		assert_not @user_place.valid?
	end

	test "can save a user_place" do
		assert_difference "UserPlace.count", 1 do
			@user_place.save
		end
		puts @user_place.errors.inspect
	end

	test "cannot save the same place twice same category" do
		assert_difference "UserPlace.count", 1 do
			@user_place.save
		end
		assert_no_difference "UserPlace.count" do
			@user_place.save
		end
	end

	test "can save place again under the other category" do
		assert_difference "UserPlace.count", 1 do
			@user_place.save
		end
		assert_difference "UserPlace.count", 1 do
			@user_place_2.save
		end
	end
	
	test "adding a place updates corresponding count" do
		visited = @user_place.place.visited
		visiting = @user_place_2.place.visiting
		
		@user_place.save #visited, lived
		@user_place_2.save #traveling, visitng

		@user_place.reload
		@user_place_2.reload

		assert_equal @user_place.place.visited, (visited + 1)
		assert_equal @user_place_2.place.visiting, (visiting + 1)
	end

	test "removing a place updates corresponding count" do
		@user_place.save #visited, lived
		@user_place_2.save #traveling, visitng

		@user_place.reload
		@user_place_2.reload

		visited = @user_place.place.visited
		visiting = @user_place_2.place.visiting

		visited_place = @user_place.place
		visiting_place = @user_place_2.place
		
		@user_place.destroy
		@user_place_2.destroy

		visited_place.reload
		visiting_place.reload

		assert_equal visited_place.visited, (visited - 1)
		assert_equal visiting_place.visiting, (visiting - 1)
	end

end

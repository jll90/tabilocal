require 'test_helper'

class SuggestionTest < ActiveSupport::TestCase
  
  def setup
  	@suggestion = Suggestion.new(
  		name: "mynameis",
  		email: "validemail@google.com",
  		subject: "subject",
  		message: "message" * 40
  		)
  end

  test "suggestion is valid" do
  	assert @suggestion.valid?
  end

  test "name is required" do
  	@suggestion.name = ""
  	assert_not @suggestion.valid?
  end

  test "email is required w/ correct format" do
  	@suggestion.email = ""
  	assert_not @suggestion.valid?

  	valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
	                         first.last@foo.jp alice+bob@baz.cn]
	valid_addresses.each do |valid_address|
	    @suggestion.email = valid_address
	    assert @suggestion.valid?, "#{valid_address.inspect} should be valid"
	end
  end

  test "subject is required" do
  	@suggestion.subject = ""
  	assert_not @suggestion.valid?
  end

  test "message is required must be over 40 characters long" do
  	@suggestion.message = ""
  	assert_not @suggestion.valid?
    @suggestion.message = "a" * 39
    assert_not @suggestion.valid?
    @suggestion.message = "a" * 40
    assert @suggestion.valid?
  end

end

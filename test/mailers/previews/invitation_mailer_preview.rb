# Preview all emails at http://localhost:3000/rails/mailers/invitation_mailer
class InvitationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/invitation_mailer/invite_friend
  def invite_friend
  	email = "jesus@finvox.com"
  	recipient_name = "Jesus"
  	sender_name = "Dabao"
  	content = "Please join Tabilocal. The best site on the internet"
    InvitationMailer.invite_friend(
    	email,
    	recipient_name,
    	sender_name,
    	content
    	)
  end

end

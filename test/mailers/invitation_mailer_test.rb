require 'test_helper'

class InvitationMailerTest < ActionMailer::TestCase
  def setup
  	@friend_email = "myfriend@gmail.com"
    @message = "This is my message"
    @sender_name = "Lord Byron"
    @recipient_name = "Chun Tsu"
  end

  test "invite_friend" do
    mail = InvitationMailer.invite_friend(@friend_email, @recipient_name, @sender_name, @message)
    assert_equal "Invitation to join Tabilocal", mail.subject
    assert_equal [@friend_email], mail.to
    assert_equal ["noreply@tabilocal.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
